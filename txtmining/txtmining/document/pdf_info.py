'''
Created on Apr 22, 2015

@author: Daniel
'''
from txtmining.base.util import shell_quote
from txtmining.document.metadata_extractor import MetadataExtractor


class PDFInfo(MetadataExtractor):
    '''
    classdocs
    '''

    def __init__(self,
                 bin_path):
        '''
        Constructor
        '''
        super(PDFInfo, self).__init__(bin_path)

    def get_metadata(self, path):
        self._call = self._bin
        self._call += ' ' + shell_quote(path)

        return super(PDFInfo, self).get_metadata(path)
