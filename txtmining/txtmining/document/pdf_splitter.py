'''
Created on sep 27, 2016

@author: Alejandro Molina
'''

import os
from PyPDF2 import PdfFileWriter, PdfFileReader


class PDFPaginator():
    '''
    Multi-page pdf file to separate pdf files per page.
    '''

    def __init__(self):
        pass

    def split(self, pdf_path, output_dir):
        (pdf_dir, pdf_basename) = os.path.split(pdf_path)
        output_dir = output_dir if output_dir else './'
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        inputpdf = PdfFileReader(open(pdf_path, "rb"))

        for i in xrange(inputpdf.numPages):
            output = PdfFileWriter()
            output.addPage(inputpdf.getPage(i))
            page_bname = 'page_{}_{}'.format(i, pdf_basename)

            page_path = os.path.join(output_dir, page_bname)

            with open(page_path, "wb") as outputStream:
                output.write(outputStream)

# Usage :from txtmining.document.pdf_splitter import PDFPaginator
# p = PDFPaginator()
# p.split('/path/to/pdf/File.pdf', '/tmp/File_items')
