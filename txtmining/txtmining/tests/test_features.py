# -*- coding: utf-8 -*-


import os
import unittest
import simplejson

from txtmining.mlearning.ml_features import (alias_features)
from txtmining.tests.util import (get_test_data_folder)
import nltk


class AliasFeaturesTest(unittest.TestCase):

    def setUp(self):
        self.txt = u'Las plantas medicinales de uso más frecuente son el pirul (Schinus molle) (30 menciones), árbol del que se utilizan sus ramas y hojas en padecimientos de definición popular (aire y barridas), este se puede encontrar en la zona baja del municipio, el pericón (Tagetes lucida) (30 menciones), hierba usada principalmente para malestares estomacales, crece en las zonas perturbadas cercanas a los encinares, el espule (Pinaropappus roseus) (29 menciones), ayuda a aliviar enfermedades de la piel especialmente en los niños, crece durante la época de lluvias en las laderas de pastoreo de la zona baja y el Botón-chiguite, de la familia solanaceae con (28 menciones), que ayuda en casos de fiebre, diarrea, salpullido, viruela y sarampión, se encuentra en la zona baja del municipio, aunque no es frecuente encontrarlo (Figura 40).'
        nltk.download('punkt')

    def test_isNonEmptyDic(self):
        returned_dict = alias_features(self.txt, features_file=None)
        j = simplejson.dumps(returned_dict,
                         encoding = 'utf-8',
                         ensure_ascii = False,
                         sort_keys = True,
                         indent = 2).encode('UTF-8')
        self.assertIsNot(returned_dict, {})

if __name__ == '__main__':
    unittest.main()
