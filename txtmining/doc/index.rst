.. Text Mining - Biodiversity documentation master file, created by
   sphinx-quickstart on Tue Aug 18 09:53:21 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la documentación de Text Mining - Biodiversity
===========================================================

    :Última revisión: `Juan M. Barrios`_, 24/Agosto/2015

Guía de usuario
---------------

Aquí se encuentra un manual de como puedes empezar a usar la aplicación de
``txtmining`` via el script ``txtm.py``.

.. toctree::
   :maxdepth: 2

   user/intro
   user/install
   user/quick
   user/guia
   user/async
   user/backnotes

Documentación del módulo
------------------------

Algunas de las características más importantes del módulo ``txtmining`` se
encuentran documentadas en esta sección.

.. toctree::
   :maxdepth: 2

   api

.. .. automodule:: txtmining
   .. automodule:: txtmining.controllers.controllers
   .. autoclass:: txtmining.controllers.controllers.MetadataController

Índices y Tablas
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. _Alejandro Molina Villegas: amolina@conabio.gob.mx
.. _Juan M. Barrios: juan.barrios@conabio.gob.mx
.. _Daniel Zenteno: dzenteno@conabio.gob.mx
.. _Raul Sierra: rsierra@conabio.gob.mx
