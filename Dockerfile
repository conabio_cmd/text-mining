FROM conabio/txtmining:latest 

MAINTAINER Coordinación de Ecoinformática <ecoinformatica@conabio.gob.mx>

# Code
ENV OPENNLP_MODEL /code/txtmining/txtmining/resources/models/es-iula.bin
ENV REGEX_DIR=/code/txtmining/txtmining/resources/regexp
COPY . /code
WORKDIR /code

RUN pip install mercurial
RUN pip install -r txtmining/requirements.txt

# CHICANO FIX. REMOVE
RUN pip install -e txtmining

# Install taxonfinder
WORKDIR /code/txtmining/external
RUN npm install taxonfinder --save
# Create symlink node = nodejs
RUN ln -s /usr/bin/nodejs /usr/bin/node
WORKDIR /code

CMD ["/usr/local/bin/nosetests", "-c", "nose.cfg"]
