.. _async-exec:

Ejecutar servicio de GlobalNames de manera asíncrona
====================================================

Como se mencionó en :ref:`globalnames-r-d`, al ser un servicio web este está
sujeto variaciones en el tiempo de respuesta, es por esto que se ha implementado
la capacidad de hacer llamadas asíncronas al servicio de |gnrd|_. Esta
implementación se hizo usando `Celery`_, un
manejador de tareas con soporte de colas.

Para hacer uso de esta funcionalidad es importante entender un poco la manera
que funciona `Celery`_. La principal función es la de crear colas de tareas,
en las cuales las tareas que tienen que ser ejecutadas son registradas, la
ejecución de las tareas se lleva a cabo en los *trabajadores*. El
registro de las tareas en las colas se lleva a cabo mediante un
*intermediario* y cada uno de los resultados de las tareas es registrado en
una base de datos, que pueden ser consultados en algún punto de la ejecución
mediante el *id* de la tarea.

En nuestra implentación las tareas se registran al momento de construir el
objeto y se registran en el atributo
:attr:`~txtmining.document.gn_caller.GlobalNamesCelery.results` del objeto
:class:`txtmining.document.gn_caller.GlobalNamesCelery`. También al momento de
constuir el objeto se incluye la información relativa a la aplicaición de
`Celery`_.

A continuación presentamos un ejemplo de como se puede hacer los llamados de
manera asíncrona.

Lo primero que hay que hacer es definir la configuración que tendrá nuestra
aplicación en `Celery`_, un ejemplo es

.. literalinclude:: ../code/myceleryconf.py
   :caption: myceleryconf.py
   :name: celeryconf

Aquí se define que la aplicación usará SQLite como *negociador* y base para los
resultados, además el idioma que hablaran los *trabajadores* y el *negociador*
será ``JSON``, y por útlimo se pide que las tareas se resuelvan de manera
asíncrona con la opción ``CELERY_ALWAYS_EAGER = False``.

Después de esto tenemos que lanzar el servicio de `Celery`_ el cual será el
encargado de recibir las tareas sometidas y registrar los estados de estas. Para
esto hay que correr en una terminal la siguiente línea:

.. code-block:: console

   $ celery worker -A txtmining.document.globalnames --config=/path/to/mycelery.py/mycelery -l INFO

En este momento se tiene un *negociador* con las nuestras tareas registradas y
usando la configuración que le dimos. Por último podemos hacer uso de las
llamadas asíncronas, el siguiente *script* procesa el archivo
``my_segmented_file.txt`` e imprime el resultado de las consultas.

.. literalinclude:: ../code/asynccall.py
   :caption: asynccall.py
   :name: async

.. note::
   El archivo :ref:`celeryconf` debe de ser accesible desde el path de Python para
   que funcione :ref:`async`.


.. _Celery: http://www.celeryproject.org/
