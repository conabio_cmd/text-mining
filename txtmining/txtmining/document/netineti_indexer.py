# -*- coding: utf-8 -*-
"""
Created on July 31, 2015

@author: Alejandro Molina
"""
import codecs
from txtmining.base.util import (DEFAULT_ENCODING)
from txtmining.lib.netineti.neti_neti import NetiNeti


class SpaNeti(NetiNeti):
    """SpaNeti is a NetiNeti subclass of NetiNeti adapted for Spanish."""
    def __init__(self, netineti_model):
        NetiNeti.__init__(self, netineti_model)
        self.index = []

    def get_indexf(self, infile, abbrev=True, sciname_type='all'):
        file_lines = self.__loadf(infile)

        for (i, line) in enumerate(file_lines):
            (resolved_names, names_verbatim, offsets) = self.find_names(line)

            if names_verbatim:
                for (j, name) in enumerate(names_verbatim):

                    name_len = len(name.split(' '))
                    must_add_name = False
                    # index any name
                    if sciname_type == 'all':
                        must_add_name = True
                    # index exclusivelly one-word names
                    elif sciname_type == 'monomial' and name_len >= 1:
                        must_add_name = True
                    # index exclusivelly two-words names
                    elif sciname_type == 'binomial' and name_len >= 2:
                        must_add_name = True
                    # index exclusivelly more-than-two-words names
                    elif sciname_type == 'long' and name_len > 2:
                        must_add_name = True

                    # Verify abbreviations in name
                    if not abbrev and '.' in name:
                        must_add_name = False

                    if must_add_name:
                        self.index.append((i+1,
                                           name,
                                           offsets[j]))

        if len(self.index) > 1:
            return self.index
        else:
            return []

    def __loadf(self, filepath):
        try:
            fp = codecs.open(filepath, 'r', DEFAULT_ENCODING)
            return fp.readlines()
        except Exception, e:
            raise e
