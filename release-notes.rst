***************************************************
Módulo Txtmining - Biblioteca para la biodiversidad
***************************************************

Release 1.0
===========

- Extracción de texto plano a partir de archivos pdf usando Apache PDFBox,
  Apache Tika y Poppler pdftotex.
- Extracción de metadatos de archivos pdf usando Apache Tika y Poppler pdfinfo.
- Segmentación de oraciones basada en aprendizaje supervisado y heurísticas
  (precisión de 92%),
- Detección de entidades nombradas de interés en biodiversidad (nombres de
  taxones, nombres comunes, localidades, conceptos de usos tradicionales).
- Integración con el servicio Global Names Recognition and Discovery.
- Extracción de información de usos tradicionales de especies mediante recursos
  léxicos.
- Interfaz de línea de comandos para usar la bibioteca (txtm.py).
- Documentación (directorio htmldoc).
