from cStringIO import StringIO
import json
import time
from celery import Celery
from celery.utils.log import get_task_logger
import requests

logger = get_task_logger(__name__)
GN_URL = "http://gnrd.globalnames.org/name_finder.json"
MAX_TRIES = 20


app = Celery('txtmining.document.globalnames')


@app.task(bind=True, max_retries=MAX_TRIES)
def submit_text(self, text):
    st = StringIO(str(text))
    try:
        r = requests.get(GN_URL,
                         files={'file': ('text.txt', st, 'text/plain')},
                         params={'all_data_sources': 'true',
                                 'verbatim': 'true'},
                         headers={"charset": "UTF-8"},
                         timeout=30)

    except requests.exceptions.ConnectionError:
        self.retry(args=self.request.args,
                   countdown=max(2 ** self.request.retries, 2),
                   exc=requests.exceptions.ConnectionError)
    return r.url


@app.task(bind=True)
def check_status(self, url):
    for i in xrange(0, MAX_TRIES):
        r = requests.get(url, timeout=30)
        if r.content:
            d = json.loads(r.content)
            if d['status'] != 303:
                break
        time.sleep(2 * i)
    return r.content
