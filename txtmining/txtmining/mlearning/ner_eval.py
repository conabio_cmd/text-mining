"""
Named Entity Recognition Evaluation

@author: Alejandro Molina.

Created on April 2016.
"""

from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix

from txtmining.converters.brat import BratConv
from txtmining.converters.brat import apply_rule


class NEReval():
    """docstring for NEReval"""
    def __init__(self,
                 text_file,
                 reference_ann_file,
                 predicted_ann_file,
                 heuristic_rule=False):

        self.txt = text_file
        self.ref_ann = reference_ann_file
        self.pred_ann = predicted_ann_file

        # get list of word classes and relations from reference
        (self.ref_word_classes,
         self.ref_rels) = BratConv.brat2classes(self.txt,
                                                self.ref_ann)

        # get list of word classes and relations from prediction
        (self.pred_word_classes,
         self.pred_rels) = BratConv.brat2classes(self.txt,
                                                 self.pred_ann)
        # heuristic for word class correction
        if heuristic_rule:
            self.pred_word_classes = apply_rule(self.pred_word_classes)

    def word_classification(self, labels):

        y_true = [classs for (word, classs) in self.ref_word_classes]
        y_pred = [classs for (word, classs) in self.pred_word_classes]
        l = list(set(y_true))
        # evaluation
        confu_matrix = confusion_matrix(y_true, y_pred, labels)
        report = (classification_report(y_true, y_pred, labels=l))
        return (confu_matrix, report)

    def relation_detection(self, ref_rels, pred_rels, label):
        # TODO: Use distances as criteria for relation evaluation

        # filter by label
        label_ref = filter(lambda x: x[2] == label, ref_rels)
        label_pred = filter(lambda x: x[2] == label, pred_rels)

        # remove ids since they could be different
        ref = self.remove_ids(label_ref)
        pred = self.remove_ids(label_pred)
        card_found = float(len(pred))
        card_ref = float(len(ref))
        # define sets
        U = [set(ref), set(pred)]
        intersection = set.intersection(*U)
        card_intersection = float(len(intersection))
        # assez metrics
        precision = card_intersection / card_found
        recall = card_intersection / card_ref
        return (precision, recall)

    def remove_ids(self, rels):

        noids = [((a1_label, a1start, a1end, a1name, a1canon),
                  rel_label,
                  (a2_label, a2start, a2end, a2name, a2canon))
                 for
                 (rel_id,
                  (a1id, a1_label, a1start, a1end, a1name, a1canon),
                  rel_label,
                  (a2id, a2_label, a2start, a2end, a2name, a2canon))
                 in rels]

        return noids
