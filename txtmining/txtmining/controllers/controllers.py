# -*- coding: utf-8 -*-
"""
Easy-to-use controllers to apply common Text Mining preprocessing tasks.

Tasks:
    - OCR parsing of PDF file,
    - extraction of plain UTF-8 txt characters,
    - parsing correction (abreviations and dash errors) of OCR/PDF parsing,
    - sentence segmentation (frontiers detection),
    - index extraction,
    - global names service call,
    - regular expression extraction,
    - Machine learning classification.

Classes:
    :class:`AbstractController` (abstract) The abstract
    base class.

    :class:`MetadataController` Parse files to extract
    metdata.

    :class:`PDFparserController` Parse PDF files (and
    some other formats).

    :class:`PreprocessingController` Correct some
    errors produced during PDF parsing.

    :class:`IndexController` Produce a list of mentions of
    entities in a file.

    :class:`ReIndexController` Extract an index of entities
    and return a list.

    :class:`GnIndexController` A wrapper to call Global
    Names service.

    :class:`FragmentController` A lexical Information extractor

    :class:`TrainController` A machine learning generic classifier trainer

Functions:
    All controllers implement ``apply(*args, **kwargs)``.

    Depending on the controller, optional arguments could be provided.

Exceptions:
    IOError: infile must be string or unicode.

    IOError: Path to infile does not exist.

    IOError: Expected file name, directory name given.

    ValueError: metadata_type must be string or unicode.

"""

from StringIO import StringIO
import abc
import inspect
import mimetypes
import os
import re
import sys
import warnings
import codecs
import pickle
import nltk
import ConfigParser
import imp
import datetime

from txtmining.base.util import DEFAULT_ENCODING
from txtmining.document.fragment_extractor import (FragmentExtractor,
                                                   PlantProjectReLoader)
from txtmining.document.pdf_box import PDFBox
from txtmining.document.pdf_info import PDFInfo
from txtmining.document.pdf_to_text import PDFtoText
from txtmining.document.text_corrector import (OnelineCorrector,
                                               DashCorrector,
                                               AbreviationCorrector,
                                               SentenceCorrector)
from txtmining.document.text_indexer import (RegexIndexBuilder)
from txtmining.document.tika import Tika
from txtmining.document.taxonfinder import Taxonfinder
from txtmining.document.netineti_indexer import (SpaNeti)
from txtmining.base.util import get_spaneti_model, get_netineti_model
# from txtmining.mlearning.ml_features import alias_features
from txtmining.mlearning.ml_bayes import BayesClassifier
from txtmining.API.BioAPI import AliasFinder
from txtmining.converters.brat import BratConv


def get_controllers_name():
    modes = list()
    current_module = sys.modules[__name__]
    members = inspect.getmembers(current_module)
    for _, obj in members:
        if inspect.isclass(obj):
            module = inspect.getmodule(obj)
            if hasattr(module, 'AbstractController'):
                obj_str = str(obj)
                obj_str = obj_str.replace('<class', '')
                obj_str = obj_str.replace('>', '')
                obj_str = obj_str.replace("'", '')
                obj_str = os.path.splitext(obj_str)[1]
                obj_str = obj_str.replace('.', '')
                if obj_str != 'AbstractController':
                    modes.append(obj_str)

    return modes


class AbstractController(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, *args, **kwargs):
        self.default_encoding = DEFAULT_ENCODING

    @abc.abstractmethod
    def apply(self, infile, *args, **kwargs):
        pass

    def verif_path(self, path, name):
        if not path or not os.path.exists(path):
            raise IOError("Path to {} does not exist: {}, \"{}\"".format(
                name, type(path), path))

    def verif_params(self, infile):
        if not isinstance(infile, basestring):
            raise IOError(
                "infile must be string or unicode: {} found.".format(
                    type(infile)))
        if not os.path.exists(infile):
            raise IOError("Path to infile does not exist.")
        if os.path.isdir(infile):
            raise IOError("Expected file name, directory name given.")

    def verif_isPDF(self, infile):
        # gives like ('application/pdf', None)
        return mimetypes.guess_type(infile,
                                    strict=True)[0] == 'application/pdf'

    def verif_isTXT(self, infile):
        # gives like ('text/plain', None)
        return mimetypes.guess_type(infile, strict=True)[0] == 'text/plain'

    def verif_dataset(self, data):
        e_msg = 'Expected list of pairs. '\
                'Each pair must have two unicode text strings'
        try:
            is_list = isinstance(data, list)
            pairs = all(len(x) >= 2 for x in data)
            first_str = all(isinstance(x[0], basestring) for x in data)
            second_str = all(isinstance(x[1], basestring) for x in data)
        except Exception:
            raise ValueError(e_msg)
        finally:
            if not (is_list and pairs and first_str and second_str):
                raise ValueError(e_msg)


class MetadataController(AbstractController):

    """MetadataController."""

    def __init__(self, tika_bin, pdfinfo_bin):
        super(MetadataController, self).__init__()
        super(MetadataController, self).verif_path(tika_bin, 'tika')
        super(MetadataController, self).verif_path(pdfinfo_bin, 'pdfinfo')
        self.tika = Tika(tika_bin, None)
        self.pdfinfo = PDFInfo(pdfinfo_bin)

    def apply(self, infile, metadata_type='all'):
        """
        Extract metadata from infile and return a dictionary.

        :param infile: the file to process (tentatively a PDF)

        :param metadata_type: any of: [``'tika'``, ``'pdfinfo'``, ``'all'``].
                              ``'all'`` is used as the default value if not
                              valid or not specified.

        :return: :class:`dict` Each metadata parser are returned as dictionary
                 key:
                 ::

                 {'pdfinfo_meta': {...}, 'tika_meta': {...}}

        :rtype: dict
        """
        super(MetadataController, self).verif_params(infile)
        retvalue = dict()
        # metadata_type is any of: ['tika'|'pdfinfo'|'all']
        # defaults to 'all' if unknow metadata_type
        if metadata_type not in ['tika', 'pdfinfo', 'all']:
            warnings.warn('Unknow metadata_type: Using all')
            metadata_type = 'all'

        # defaults to 'tika' if not a pdf
        is_pdf = super(MetadataController, self).verif_isPDF(infile)
        if not is_pdf:
            warnings.warn('Input file is not a pdf: Using only tika',
                          Warning)
            metadata_type = 'tika'

        # use metadata_type to create instance(s)
        if metadata_type in ['tika', 'all']:
            tika_meta = self.tika.extract_meta_data(infile)
            retvalue['tika_meta'] = tika_meta
        if metadata_type in ['pdfinfo', 'all']:
            pdfinfo_meta = self.pdfinfo.get_metadata(infile)
            retvalue['pdfinfo_meta'] = pdfinfo_meta
        return retvalue


class PDFparserController(AbstractController):

    """PDFparserController."""

    def __init__(self, out_dir, tika_bin, pdfbox_bin, pdftotext_bin):
        super(PDFparserController, self).__init__()
        super(PDFparserController, self).verif_path(tika_bin, 'tika')
        super(PDFparserController, self).verif_path(pdfbox_bin, 'pdfbox')
        super(PDFparserController, self).verif_path(pdftotext_bin, 'pdftotext')

        tikadir = os.path.join(out_dir, 'tika')
        try:
            os.makedirs(tikadir)
        except Exception:
            pass
        self.tika = Tika(tika_bin, tikadir)

        pdfboxdir = os.path.join(out_dir, 'pdfbox')
        try:
            os.makedirs(pdfboxdir)
        except Exception:
            pass
        self.pdfbox = PDFBox(pdfbox_bin, pdfboxdir)

        pdftotextdir = os.path.join(out_dir, 'pdftotext')
        try:
            os.makedirs(pdftotextdir)
        except Exception:
            pass
        self.pdftotext = PDFtoText(pdftotext_bin, pdftotextdir)

    def apply(self, infile, parser_type='all'):
        """
        Extract text from infile and return a unicode string.

        This method is file type independent. If OCR is needed
        it would be applied to the file, which could be a PDF, Word, JPG,
        etcetera. However, only tika type paser would process almost any
        file type. The other parser types only would work with PDFs.

        :param infile: the file to process (tentatively a PDF)

        :param parser_type: any of: [``'tika'``, ``'pdfbox'``, ``'pdftotext'``,
                            ``'all'``]. ``'all'`` is used as default value if
                            not valid or not specified.

        :param outdir: directory where raw text files are out. Current directory
                       is used as default value.

        :return: A :class:`dict` with path to each generated text file:
                 ::

                     {'tika_out': 'outdir/tika/infile.pdf.txt',
                      'pdfbox_out': 'outdir/pdfbox/infile.pdf.txt',
                      'pdftotext_out': 'outdir/pdftotext/infile.pdf.txt',
                     }

                 If directories ``tika``, ``pdfbox``, ``pdftotext`` does not
                 exist they will be created.

        :rtype: dict
        """
        super(PDFparserController, self).verif_params(infile)
        retvalue = dict()
        # metadata_type is any of: ['tika', 'pdfbox', 'pdftotext', 'all']
        # defaults to 'all' if unknow metadata_type
        if parser_type not in ['tika', 'pdfbox', 'pdftotext', 'all']:
            warnings.warn('Unknow parser_type: Using all')
            parser_type = 'all'

        # defaults to 'tika' if not a pdf
        is_pdf = super(PDFparserController, self).verif_isPDF(infile)
        if not is_pdf:
            warnings.warn('Input file is not a pdf: Using only tika')
            parser_type = 'tika'

        # use parser_type to create dirs and instance(s)
        if parser_type in ['tika', 'all']:
            retvalue['tika_out'] = self.tika.extract(infile)

        if parser_type in ['pdfbox', 'all']:
            retvalue['pdfbox_out'] = self.pdfbox.extract(infile)

        if parser_type in ['pdftotext', 'all']:
            retvalue['pdftotext_out'] = self.pdftotext.extract(infile)

        return retvalue


class PreprocessingController(AbstractController):

    """PreprocessingController."""

    def __init__(self, opennlp_bin, opennlp_mod):
        super(PreprocessingController, self).__init__()
        super(PreprocessingController, self).verif_path(opennlp_bin, 'opennlp')
        super(PreprocessingController, self).verif_path(opennlp_mod,
                                                        'opennlp model')
        self.sentcorrector = SentenceCorrector(opennlp_bin, opennlp_mod)
        """
        :param opennlp_bin: Path to opennlp
        :param opennlp_model: Path to opennlp model if other than default
        """
    def apply(self, infile, correction_type=['all']):
        """
        Apply some OCR corrections to infile and return a string.

        :param infile: the file to process (txt)

        :param correction_type: any of: [``'dash'``, ``'abrv'``, ``'onel'``,
                                ``'sent'``, ``'all'``]. ``'all'`` is used as
                                the default value if not valid or not specified.
        """
        super(PreprocessingController, self).verif_params(infile)
        is_txt = super(PreprocessingController, self).verif_isTXT(infile)
        if not is_txt:
            warnings.warn('Input file was not recognized as a text file or'
                          ' is not UTF-8 encoded.', UserWarning)
        # if not is_txt:
        #    raise IOError('Input file is not a text file or'
        #                  ' is not UTF-8 encoded.')

        # correction_type is any of: ['dash', 'abrv', 'onel', 'sent', 'all']
        # defaults to 'all' if unknow correction_type
        if not frozenset(correction_type).issubset(['dash', 'abrv', 'onel',
                                                    'sent', 'all']):
            print('Unknow correction_type: Using all')
            correction_type = ['all']

        strio = StringIO(file(infile).read())

        if 'all' in correction_type:

            strio = DashCorrector.correct(strio)
            strio = OnelineCorrector.correct(strio)
            strio = self.sentcorrector.correct(strio)
            strio = AbreviationCorrector.correct(strio)

            return strio.getvalue()

        for correction in correction_type:

            # use correction_type to create instance(s)
            if correction == 'dash':
                strio = DashCorrector.correct(strio)

            if correction == 'abrv':
                strio = AbreviationCorrector.correct(strio)

            if correction == 'onel':
                strio = OnelineCorrector.correct(strio)

            if correction == 'sent':
                # Dash and abreviations not corrected.
                strio = OnelineCorrector.correct(strio)
                strio = self.sentcorrector.correct(strio)

        return strio.getvalue()


class ReIndexController(AbstractController):

    """ReIndexController."""

    def __init__(self, regexp_file):
        r"""
        regexp -- a compiled regular expression.

           The use of groups are higly recommended to capture labelled indexes.
           For instance, a regular expression like this one:
           (?P<scientificNames>
                (?P<genera>\bAbies\b|...|Zyzyxia\b)\s
                (?P<species>\bidiopoda\b|...|\bzolleriana\b|Spp\.)
           )
        """
        if not os.path.isfile(regexp_file):
            raise ValueError(
                "regexp_file must be a valid file")

        rexf = codecs.open(regexp_file, 'r', DEFAULT_ENCODING)
        regex_contents = rexf.read()
        rexf.close()
        pattern = regex_contents.replace('\t', '').replace('\n', '')
        regexp = re.compile(pattern)

        super(ReIndexController, self).__init__()
        if not regexp or not isinstance(regexp, re._pattern_type):
            raise ValueError(
                "regexp must be a valid, non-empty regular expression")
        self.regexp = regexp

    def apply(self, infile):
        """
        Extract an index of entities from infile and return a list.

        Entities could be scientific names, common names, locations or
        any other group(s) captured in a regular expression.
        However, special interest is given to species names.

        :param infile: the file to process (txt)

        :return: a generated indexes :class:`list`:
                 ::

                     [(204, 'bAbies bidiopoda', 62, 69),
                     (204, 'Zyzyxia Spp.', 83, 90), ...]

        :rtype: list
        """
        super(ReIndexController, self).verif_params(infile)
        is_txt = super(ReIndexController, self).verif_isTXT(infile)
        if not is_txt:
            # raise IOError('Input file is not a text file or'
            #               ' is not UTF-8 encoded.')
            warnings.warn('Input file was not recognized as a text file or'
                          ' is not UTF-8 encoded.', UserWarning)
        return RegexIndexBuilder(infile, self.regexp).get_index()


class NetinetiController(AbstractController):
    """
    :param language: any of: ['es', 'en']
            ::
                'es' Spanish model will be used.
                'en' English model will be used.
    """

    def __init__(self, language="es"):
        if language == 'es':
            netineti_mod = get_spaneti_model()
        elif language == 'en':
            netineti_mod = get_netineti_model()
        else:
            raise Exception("Please specify either 'es' or 'en' "
                            "in language parameter")
        self.spaneti = SpaNeti(netineti_mod)

    def apply(self, infile, abbrev=True, sciname_type='all'):
        """
        Extract scientific names of species from a file and return a list.

        Scientific names recognition is based on NetiNeti projetc.
        See https://github.com/mbl-cli/NetiNeti

        :param infile: the file to process (txt)

        :abbrev:  abbreviations must be included or not.
                  For instance sp., spp., cf. or var.

        :sciname_type: any of: ['monomial', 'binomial', 'long', 'all']
                 ::
                     'monomial' will return exclusivelly one-word names;
                     'binomial' will retur exclusivelly two-word names;
                     'long' will return names with more than two words;
                     'all' will return any lenght names,
                      it is used as the default value.

        :return: an index of tuples :class:`list`:
                 ::
                     [(204, 'bAbies bidiopoda', 62, 69),
                     (204, 'Zyzyxia Spp.', 83, 90), ...]
        :rtype: list
        """
        if not isinstance(abbrev, bool):
            abbrev = abbrev.lower() in ['true', '1', 'yes']
        super(NetinetiController, self).verif_params(infile)
        is_txt = super(NetinetiController, self).verif_isTXT(infile)
        if not is_txt:
            warnings.warn('Input file was not recognized as a text file or'
                          ' is not UTF-8 encoded.', UserWarning)
        return self.spaneti.get_indexf(infile, abbrev=True, sciname_type='all')


class FragmentController(AbstractController):

    """
    FragmentController is very specific of plants project.

    An especial data type (fragment) that matches the following
    regular expressions will be created:
    ::

        self.structured_re = re.compile('.*(' + terms + ')+.*' +
                                        patterns + '+.*(' + cuewords + ')+.*',
                                        re.UNICODE | re.IGNORECASE)

        self.unestructured_re = re.compile(terms + '|' + patterns + '|' +
                                           cuewords, re.UNICODE | re.IGNORECASE).
    """

    def __init__(self, regex_dir):
        super(FragmentController, self).__init__()
        if not regex_dir or not isinstance(regex_dir, basestring):
            raise IOError('regex_dir is not set or is not string type.')
        if not os.path.exists(regex_dir):
            raise IOError("Path regex_dir does not exist.")
        (nombre_o_parte, verbo, uso) = PlantProjectReLoader.load(regex_dir)
        self.fragextractor = FragmentExtractor(nombre_o_parte, verbo, uso)

    def apply(self, infile, nlines_before=4, nlines_after=2):
        """
        Extract information about plants uses and return a list of dicts.

        An especial data type (fragment) that matches the following
        regular expression:
        ::

            'name_part | verb | cueword'

        where name_part represents scientific or common names of species
        or parts of them; verb is a functional pattern; and cueword is a
        lexicon of words related with plants uses.

        :param infile: the file to process (txt).

        :param nlines_before: number of lines before matching line to be
                              considered as context.

        :param nlines_before: number of lines after matching line to be
                        considered as context.

        :return: a list of dictionary with fragments.
        """
        super(FragmentController, self).verif_params(infile)
        is_txt = super(FragmentController, self).verif_isTXT(infile)
        if not is_txt:
            raise IOError('Input file is not a text file or is'
                          ' not UTF-8 encoded.')

        if not (nlines_before and nlines_after) \
                or not (isinstance(nlines_before, int) and
                        isinstance(nlines_after, int)) \
                or not (nlines_before + nlines_after > 1):
            warnings.warn(
                'nlines_before and nlines_after must be positive integers. \
                Using default values: nlines_before = 4, nlines_after = 2')
            nlines_before = 4
            nlines_after = 2

        return self.fragextractor.extract(infile, nlines_before, nlines_after)


class TaxonfinderController(AbstractController):

    """
    A Wrapper to execute Taxonfinder.

    A file must be passed in constructor.

    apply() method will execute Taxonfinder.

    """

    def __init__(self):
        super(TaxonfinderController, self).__init__()
        self.taxonfinder = Taxonfinder()

    def apply(self, infile):
        """
        Extract an index of scientific names from infile and return a
        :class:`list`.

        :return: a generated indexes :class:`list`:
                 ::

                     [(204, 'bAbies bidiopoda', 62, 69),
                     (204, 'Zyzyxia Spp.', 83, 90), ...]

        :rtype: list
        """
        super(TaxonfinderController, self).verif_params(infile)
        if not super(TaxonfinderController, self).verif_isTXT(infile):
            raise IOError('Input file is not a text file or '
                          'is not UTF-8 encoded.')
        return self.taxonfinder.get_index(infile)


class TrainController(AbstractController):
    """
    A machine learning generic classifier trainer.

    :param type: any of: ['nbayes', 'maxent']
            ::
                'nbayes' A Naive Bayes classifier.
                'maxent' A Maximum entropy classifier.

    :param train_set: a :class:`list' of pairs for training.
                      Each pair have two unicode text strings
                      the text for training and a label.
                 ::

                     [(u'I like bees', u'POS'),
                     (u'I hate cats', u'NEG'), ...]

    :param config: Configuration file.
                   Specific python config file for parameter values
                   depending on classifier type.

    :param out_modeldir: the path to store resulting model.

    """
    def __init__(self, classifier_type, config, train_set, out_modeldir):
        super(TrainController, self).__init__()
        # verif out dir, use local if not
        try:
            super(TrainController, self).verif_path(out_modeldir,
                                                    'out_modeldir')
            self.out_modeldir = out_modeldir
        except Exception:
            self.out_modeldir = os.getcwd()
            warnings.warn('Using current directory for output: {}'.format(out_modeldir))

        # verif config file, wont continue if incorrect
        try:
            super(TrainController, self).verif_params(config)
            # read config
            self.config = ConfigParser.ConfigParser()
            self.config.read(config)
        except Exception, e:
            print('Invalid config file.')
            raise e

        # verif train set dataformat, wont continue if incorrect
        try:
            super(TrainController, self).verif_dataset(train_set)
        except Exception, e:
            print('Invalid train set format.')
            raise e

        # if args OK assign members
        self.train_set = list(train_set)
        self.classifier_type = classifier_type

    def apply(self):
        retdict = dict()
        # classifier type is any of: ['nbayes'|'maxent'|'all']
        # defaults to 'all' if unknow metadata_type
        if self.classifier_type not in ['nbayes', 'maxent', 'all']:
            warnings.warn('Unknow classifier type: Using all', UserWarning)
            self.classifier_type = 'all'

            # Naïve Bayes classifier
        if self.classifier_type in ['all', 'nbayes']:
            label = 'nbayes'
            retdict[label] = self.get_bayes(label)

        # created model paths
        return retdict

    def get_bayes(self, label):
            # load module dinamically
            module_name = self.config.get(label, 'MODULE')
            module_path = self.config.get(label, 'FEATFUNCTION_PATH')
            featfunction_name = self.config.get(label, 'FEATFUNCTION_NAME')
            features_file = self.config.get(label, 'FEATFILE')
            ml = imp.load_source(module_name, module_path)
            my_function = getattr(ml, featfunction_name)

            # get trained model
            nb_classifier = BayesClassifier(my_function)
            nb_model = nb_classifier.train(self.train_set, features_file)

            # create a name with label and datestamp
            pickle_path = self.model_name(label)
            # write model into a file
            try:
                pickle.dump(nb_model, open(pickle_path, 'wb'))
                return pickle_path
            except Exception, e:
                raise e

    def model_name(self, label):
        datestamp = datetime.datetime.now().strftime("%d-%m-%Y_%Hh%Mm%Ss")
        pickle_name = '{}_{}.bin'.format(label, datestamp)
        pickle_path = os.path.join(self.out_modeldir, pickle_name)
        return pickle_path


class AliasController(AbstractController):

    """
    TODO
    """

    def __init__(self,
                 arg1_label='Nombre_comun',
                 arg2_label='Nombre_cientifico',
                 rel_label='alias',
                 taxon_indexer='Taxonfinder',
                 sciname_type='all'):
        super(AliasController, self).__init__()
        self.arg1_label = arg1_label
        self.arg2_label = arg2_label
        self.rel_label = rel_label
        self.taxon_indexer = taxon_indexer
        self.sciname_type = sciname_type
        self.aliasfinder = AliasFinder(arg1_label,
                                       arg2_label,
                                       rel_label,
                                       taxon_indexer,
                                       sciname_type)

    def apply(self, infile, out_dir):
        """
        TODO
        """
        super(AliasController, self).verif_params(infile)
        if not super(AliasController, self).verif_isTXT(infile):
            raise IOError('Input file is not a text file or '
                          'is not UTF-8 encoded.')
        bname = self.__basename(infile)

        # get brat dict then ann string
        brat_dict = self.aliasfinder.get_rels(infile)
        ann_str = BratConv.dict2ann(brat_dict)

        if self.taxon_indexer in ['all', 'Taxonfinder']:
            tfpath = self.__write_ann(ann_str, bname, out_dir, 'Taxonfinder')

        return {'Taxonfinder': tfpath}

    def __basename(self, path):
        bname = (path.split('/')).pop()
        bname = (bname.split('.')[:-1]).pop(0)
        return bname

    def __write_ann(self, ann_str, bname, out_dir, label):
        try:
            tfdir = os.path.join(out_dir, 'Taxonfinder')
            if not os.path.exists(tfdir):
                os.makedirs(tfdir)
            f_path = os.path.join(tfdir, bname+'.ann')
            f = codecs.open(f_path, mode='wb')
            f.write(ann_str)
            f.close()
        except Exception:
            raise

        return f_path
