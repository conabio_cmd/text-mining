#!/usr/bin/env python
import txtmining
from setuptools import setup, find_packages


setup(
    name='txtmining',
    version=txtmining.__version__,
    packages=find_packages(exclude=['scripts', ]),
    scripts=['scripts/txtm.py'],
    description=('Spatial feature learning project'),
    author='Ecoinformatica-CONABIO',
    author_email="ecoinformatica@conabio.gob.mx",
    license="GNU V2",
    url=txtmining.__url__,
)
