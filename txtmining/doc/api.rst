Documentación de desarrollo
===========================

.. module:: txtmining

Esta sección de la documentación cubre los aspectos de implementación del
módulo.

Controllers
-----------

.. automodule:: txtmining.controllers.controllers
    :members:

GlobalNamesCelery
-----------------

.. automodule:: txtmining.document.gn_caller
    :members:
    :undoc-members:
