# -*- coding: utf-8 -*-

"""
Created on Nov. 30, 2015.

@author: Alejandro Molina.

NLP pre-procesing tasks

Alejandro Molina
2015

"""

import string
import nltk
import re
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer


class NLPpreprocessor(object):
    """
    processor handles common Natural Language Processing tasks:

    -remove punctuation;
    -remove stopwords;
    -stemmize;
    """

    def __init__(self, language):
        self.language = language
        self.defaultstplist = [u'a', u'aca', u'ahi', u'ajena', u'ajenas', u'ajeno', u'ajenos', u'al', u'algo', u'algun', u'alguna', u'algunas', u'alguno', u'algunos', u'alla', u'alli', u'ante', u'antes', u'aquel', u'aquella', u'aquellas', u'aquello', u'aquellos', u'aqui', u'bajo', u'cabe', u'cada', u'cierta', u'ciertas', u'cierto', u'ciertos', u'como', u'con', u'conmigo', u'consigo', u'contigo', u'contra', u'cual', u'cualquier', u'cualquiera', u'cualquieras', u'cuan', u'cuando', u'cuanta', u'cuantas', u'cuanto', u'cuantos', u'de', u'dejar', u'del', u'demas', u'demasiada', u'demasiadas', u'demasiados', u'desde', u'donde', u'durante', u'e', u'el', u'ella', u'ellas', u'ellos', u'en', u'entre', u'era', u'erais', u'eran', u'eras', u'eres', u'es', u'esa', u'esas', u'ese', u'eso', u'esos', u'esta', u'estaba', u'estabais', u'estaban', u'estabas', u'estad', u'estada', u'estadas', u'estado', u'estados', u'estamos', u'estando', u'estar', u'estaremos', u'estará', u'estarán', u'estarás', u'estaré', u'estaréis', u'estaría', u'estaríais', u'estaríamos', u'estarían', u'estarías', u'estas', u'este', u'estemos', u'esto', u'estos', u'estoy', u'estuve', u'estuviera', u'estuvierais', u'estuvieran', u'estuvieras', u'estuvieron', u'estuviese', u'estuvieseis', u'estuviesen', u'estuvieses', u'estuvimos', u'estuviste', u'estuvisteis', u'estuviéramos', u'estuviésemos', u'estuvo', u'está', u'estábamos', u'estáis', u'están', u'estás', u'esté', u'estéis', u'estén', u'estés', u'etc', u'etcetera', u'fue', u'fuera', u'fuerais', u'fueran', u'fueras', u'fueron', u'fuese', u'fueseis', u'fuesen', u'fueses', u'fui', u'fuimos', u'fuiste', u'fuisteis', u'fuéramos', u'fuésemos', u'ha', u'habida', u'habidas', u'habido', u'habidos', u'habiendo', u'habremos', u'habrá', u'habrán', u'habrás', u'habré', u'habréis', u'habría', u'habríais', u'habríamos', u'habrían', u'habrías', u'habéis', u'había', u'habíais', u'habíamos', u'habían', u'habías', u'hacer', u'hacia', u'han', u'has', u'hasta', u'hay', u'haya', u'hayamos', u'hayan', u'hayas', u'hayáis', u'he', u'hemos', u'hube', u'hubiera', u'hubierais', u'hubieran', u'hubieras', u'hubieron', u'hubiese', u'hubieseis', u'hubiesen', u'hubieses', u'hubimos', u'hubiste', u'hubisteis', u'hubiéramos', u'hubiésemos', u'hubo', u'jamas', u'junto', u'juntos', u'la', u'las', u'le', u'les', u'lo', u'los', u'mas', u'me', u'mediante', u'menos', u'mi', u'mia', u'mias', u'mientras', u'mio', u'mios', u'mis', u'misma', u'mismas', u'mismo', u'mismos', u'mucha', u'muchas', u'muchisima', u'muchisimas', u'muchisimo', u'muchisimos', u'mucho', u'muchos', u'muy', u'más', u'mí', u'mía', u'mías', u'mío', u'míos', u'nada', u'ni', u'ninguna', u'ningunas', u'ninguno', u'ningunos', u'no', u'nos', u'nosotras', u'nosotros', u'nuestra', u'nuestras', u'nuestro', u'nuestros', u'nunca', u'o', u'os', u'otra', u'otras', u'otro', u'otros', u'para', u'parecer', u'pero', u'poca', u'pocas', u'poco', u'pocos', u'por', u'porque', u'que', u'querer', u'quien', u'quienes', u'quienesquiera', u'quienquiera', u'qué', u'se', u'sea', u'seamos', u'sean', u'seas', u'segun', u'sentid', u'sentida', u'sentidas', u'sentido', u'sentidos', u'ser', u'seremos', u'será', u'serán', u'serás', u'seré', u'seréis', u'sería', u'seríais', u'seríamos', u'serían', u'serías', u'seáis', u'si', u'siempre', u'siente', u'sin', u'sintiendo', u'so', u'sobre', u'sois', u'somos', u'son', u'soy', u'sr', u'sra', u'sres', u'sta', u'su', u'sus', u'suya', u'suyas', u'suyo', u'suyos', u'sí', u'tal', u'tales', u'también', u'tan', u'tanta', u'tantas', u'tanto', u'tantos', u'te', u'tendremos', u'tendrá', u'tendrán', u'tendrás', u'tendré', u'tendréis', u'tendría', u'tendríais', u'tendríamos', u'tendrían', u'tendrías', u'tened', u'tenemos', u'tenga', u'tengamos', u'tengan', u'tengas', u'tengo', u'tengáis', u'tenida', u'tenidas', u'tenido', u'tenidos', u'teniendo', u'tenéis', u'tenía', u'teníais', u'teníamos', u'tenían', u'tenías', u'ti', u'tiene', u'tienen', u'tienes', u'toda', u'todas', u'todo', u'todos', u'tomar', u'tras', u'tu', u'tus', u'tuve', u'tuviera', u'tuvierais', u'tuvieran', u'tuvieras', u'tuvieron', u'tuviese', u'tuvieseis', u'tuviesen', u'tuvieses', u'tuvimos', u'tuviste', u'tuvisteis', u'tuviéramos', u'tuviésemos', u'tuvo', u'tuya', u'tuyas', u'tuyo', u'tuyos', u'tú', u'u', u'un', u'una', u'unas', u'uno', u'unos', u'usted', u'ustedes', u'varias', u'varios', u'via', u'vosostras', u'vosostros', u'vosotras', u'vosotros', u'vuestra', u'vuestras', u'vuestro', u'vuestros', u'y', u'ya', u'yo', u'él', u'éramos']

    def tokenize(self, text):
        return nltk.word_tokenize(text)

    def offset_tokenize(self, text):
        tail = text
        accum = 0
        tokens = self.tokenize(text)
        info_tokens = []
        for tok in tokens:
            scaped_tok = re.escape(tok)
            m = re.search(scaped_tok, tail)
            start, end = m.span()
            # global offsets
            gs = accum + start
            ge = accum + end
            accum += end
            # keep searching in the rest
            tail = tail[end:]
            info_tokens.append((tok, (gs, ge)))
        return info_tokens

    def remove_punctuation(self, text):
        """remove these chars from text: !"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~

            - if text is a str returns a tokenized str without punctuation chars;
            - if text is a list returns the list without punctuation elements
        """
        punctuation = list(string.punctuation)

        if type(text) == unicode or type(text) == str:
            return ' '.join(word for word in nltk.word_tokenize(text)
                            if word not in punctuation)

        elif type(text) == list:
            return [token for token in text if token not in punctuation]

        else:
            raise TypeError("{} unsupported operand type: use unicode str or list".format(type(text)))

    def remove_numbers(self, text):
        """remove numbers from text

            - if text is a str returns a tokenized str without numeric words;
            - if text is a list returns the list without numeric elements
        """

        if type(text) == unicode or type(text) == str:
            return ' '.join(token for token in nltk.word_tokenize(text)
                            if not self.is_number(token))

        elif type(text) == list:
            return [token for token in text if not self.is_number(token)]

        else:
            raise TypeError("unsupported operand type: use str or list")

    def remove_stopwords(self, text, stoplist=None):
        """remove stopwords from text

            - if text is a str returns a str without stopwords;
            - if text is a list returns the list without stopwords elements
        """
        if stoplist is None:
            stopwords_lang = self.defaultstplist
        else:
            stopwords_lang = stoplist
            # stopwords_lang = stopwords.words(self.language)

        if type(text) == unicode or type(text) == str:
            return ' '.join(word for word in nltk.word_tokenize(text)
                            if word.lower() not in stopwords_lang)

        elif type(text) == list:
            return [token for token in text
                    if token.lower() not in stopwords_lang]

        else:
            raise TypeError("unsupported operand type: use str or list")

    def stemmize(self, text):
        """A processing interface for removing morphological affixes from words.
        This process is known as stemming.
        A caller of Snowball Stemmer where the following languages are supported: Danish, Dutch, English, Finnish, French, German, Hungarian, Italian, Norwegian, Portuguese, Romanian, Russian, Spanish and Swedish. The algorithm for English is documented here: Porter, M. “An algorithm for suffix stripping.” Program 14.3 (1980): 130-137.
        See also http://snowball.tartarus.org/algorithms/spanish/stemmer.html

            - if text is a str returns a stemmed str;
            - if text is a list returns the list stemmed words
        """
        lang_stemmer = SnowballStemmer(self.language)

        if type(text) == unicode or type(text) == str:
            # return ' '.join(lang_stemmer.stem(token.decode('utf-8'))
            return ' '.join(lang_stemmer.stem(token)
                            for token in self.tokenize(text))

        elif type(text) == list:
            return [lang_stemmer.stem(token)
                    for token in text]

        else:
            raise TypeError("unsupported operand type: use str or list")

    def is_number(self, token):
        # see answer from http://stackoverflow.com/questions/379906/parse-string-to-float-or-int
        try:
            # doesnt work with comma separated : 5,447
            float(token)
            return True
        except ValueError:
            return False

    # def get_bagofwords(self, text,
    #                    remove_punctuation=True,
    #                    remove_numbers=True,
    #                    remove_stopwords=True,
    #                    use_stemmer=True,
    #                    stoplist=None):
    def get_bagofwords(self, text, params):

        tokens_list = self.tokenize(text)
        # tokens_str = str.join(' ', tokens_list).decode('UTF-8')
        tokens_str = str.join(' ', tokens_list).lower()
        if params['remove_punctuation']:
            tokens_str = self.remove_punctuation(tokens_str)
        if params['remove_numbers']:
            tokens_str = self.remove_numbers(tokens_str)
        if params['remove_stopwords']:
            tokens_str = self.remove_stopwords(tokens_str, stoplist=None)
        if params['use_stemmer']:
            tokens_str = self.stemmize(tokens_str)

        return tokens_str
