# -*- coding: utf-8 -*-

"""
Created on Jan. 26, 2016.

@author: Alejandro Molina.

Script to eval naive bayes models
using cross-validation

Alejandro Molina, Jan. 2016
"""


import codecs
import ast
import random
import numpy as np
from sklearn.metrics import classification_report
from txtmining.mlearning.ml_features import set_bow
from txtmining.mlearning.ml_features import label_examples
from txtmining.mlearning.ml_bayes import BayesClassifier
from txtmining.mlearning.ml_features import alias_features


def run_experiment(corpus, feat_dim_size, out_alias, ratio, folds, test_percent, glosses_test):

    set_bow(corpus=alias_corpus,
            dim_size=feat_dim_size,
            stoplist=None,
            out_file=out_alias)

    # prepare data
    pos = label_examples(alias_pos, 'pos_alias')
    neg = label_examples(alias_neg, 'neg_alias')

    random.shuffle(neg)
    n_examples = int(ratio * len(pos))
    neg = neg[0:n_examples]

    # Split train test data
    dev_data = pos + neg
    random.shuffle(dev_data)
    # % fot test
    n = int(len(dev_data) * test_percent)
    train = dev_data[n:]
    test =  dev_data[:n]

    # print
    (eg_pos, eg_postag) = pos[0]
    (eg_neg, eg_negtag) = neg[0]


    print '{} features'.format(feat_dim_size)
    print '{} ration'.format(ratio)
    print '{} {}'.format( len(pos),  eg_postag)
    print '{} {}'.format( len(neg),  eg_negtag)
    print '{} total'.format(len(dev_data))
    #print '{} for train, {} for test ({}%)'.format( len(train), len(test), (test_percent*100))

    bayesian = BayesClassifier(alias_features)
    nb_classifier = bayesian.train(dev_data, out_alias)

    y_true = []
    y_pred = []
    errors = []
    target_names = ['neg_alias', 'pos_alias']
    for (text, tag) in glosses_test:
        guess = nb_classifier.classify(alias_features(text, out_alias))

        y_true.append(target_names.index(tag))
        y_pred.append(target_names.index(guess))

        if guess != tag:
            errors.append( (tag, guess, text) )

    print(classification_report(y_true, y_pred, target_names=target_names))
    # cross validation
    med_ac = bayesian.cross_validation(n_folds=folds, shuffle=False)
    print 'mean accuracy using {} folds: {}'.format(folds, med_ac)

    pass

if __name__ == '__main__':
    feat_dim_size = 10

    # raw text examples
    alias_pos = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/train/pos_alias_all.txt'
    alias_neg = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/train/neg_loc.txt'
    glosas_eval = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/evaluation/alias_glosas.txt'
    
    # global files
    alias_corpus = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/train/corpus.txt'
    # out_alias_json = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/features/feat_alias.JSON'

    filenames = [alias_pos, alias_neg]
    with open(alias_corpus, 'w') as outfile:
        for fname in filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)

    # set_bow(corpus=alias_corpus,
    #               dim_size=feat_dim_size,
    #               stoplist=None,
    #               out_file=out_alias_json)

    with codecs.open(glosas_eval, 'r', encoding='UTF-8') as ge:
        raw_str = ge.read()
        glosses_test =  ast.literal_eval(raw_str)

    # Balance the number of examples for each class
    ratio = 0.8
    # number of folds in cross validation
    folds = 3
    # % fot test
    test_percent = 0.0
    n_experiments = 7
    # for i in range(1, n_experiments):
    for i in np.arange(0.1, 2.5, 0.15):
        ratio = i
        for j in range(1, n_experiments):
            feat_dim_size = n_experiments**j
            out_alias = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/features/exp/alias.JSON'.format(feat_dim_size)
            run_experiment(corpus=alias_corpus, feat_dim_size=feat_dim_size, out_alias=out_alias, ratio=ratio, folds=folds, test_percent=test_percent, glosses_test=glosses_test)
