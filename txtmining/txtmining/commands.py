# -*- coding: utf-8 -*-

from argparse import ArgumentParser
from ast import literal_eval
import inspect
import os
import sys

from joblib.parallel import Parallel, delayed
from slugify.slugify import slugify

from base.config import Config
from controllers import controllers
from controllers.controllers import get_controllers_name
import simplejson as json


def generic_controller(cmd, class_name, argv):

    # get arguments from constructor and apply function
    init_args = inspect.getargspec(class_name.__init__)[0]
    apply_args = inspect.getargspec(class_name.apply)[0]
    # remove self
    init_args.pop(init_args.index('self'))
    apply_args.pop(apply_args.index('self'))

    args = init_args + apply_args

    prog = os.path.basename(sys.argv[0])
    prog += ' '
    prog += cmd

    parser = ArgumentParser(prog=prog)

    for arg in args:
        if cmd == 'PreprocessingController' and \
           arg == 'correction_type':
            parser.add_argument('--' + arg, required=True,
                                nargs='+')
        else:
            parser.add_argument('--' + arg, required=True)

    usr_args = parser.parse_args(argv)

    init_kwargs = __get_kwargs(init_args, usr_args)
    apply_kwargs = __get_kwargs(apply_args, usr_args)

    instance = class_name(**init_kwargs)
    output = instance.apply(**apply_kwargs)

    # if output is a dictionary, print it in json

    if isinstance(output, list) or isinstance(output, dict):
        print(json.dumps(output))
    elif isinstance(output, basestring):
        print output


def __get_kwargs(args, obj):

    output_dict = dict()
    for arg in args:
        if isinstance(obj, dict):
            usr_val = obj.get(arg)
        else:
            usr_val = getattr(obj, arg)
        if isinstance(usr_val, basestring):
            usr_val = int(usr_val) if usr_val.isdigit() else usr_val
        output_dict.setdefault(arg, usr_val)

    return output_dict


def get_modes():
    modes = get_controllers_name()

    return modes


def run_pipeline(config_file):
    configuration = Config(config_file)
    # find all controllers that expect a pdf as input_extension
    extractor_controllers = dict()
    for key, item in configuration.parameters.items():
        if int(item['step']) == 0:
            extractor_controllers.setdefault(key,
                                             configuration.parameters.pop(key))

    # find all controllers that expect a txt as input and txt as output
    intermediate_controllers = dict()
    for key, item in configuration.parameters.items():
        if int(item['step']) == 1:
            intermediate_controllers.setdefault(key,
                                                configuration.parameters.
                                                pop(key))

    # find all controllers that expect a json as output
    index_controllers = dict()
    for key, item in configuration.parameters.items():
        if int(item['step']) == 2:
            index_controllers.setdefault(key,
                                         configuration.parameters.pop(key))

    # execute extractor controllers
    execute_controllers(extractor_controllers)

    # execute preprocessing controllers
    execute_controllers(intermediate_controllers)

    # execute indexes
    execute_controllers(index_controllers)


def get_files_to_process(directory, extension):
    file_list = list()
    for path, _, names in os.walk(directory):
        for name in names:
            name_extension = os.path.splitext(name)[1]
            if name_extension.endswith(extension):
                complete_file = os.path.join(path, name)
                file_list.append(complete_file)

    return file_list


def parallel_execute_controller(key, item, file_name, init_args, apply_args,
                                class_obj):

    current_params = dict(item)

    current_params.setdefault('infile', file_name)

    if key == 'PreprocessingController':
            correction_type = current_params['correction_type']
            correction_type = literal_eval(correction_type)
            current_params['correction_type'] = correction_type

    init_kwargs = __get_kwargs(init_args, current_params)
    apply_kwargs = __get_kwargs(apply_args, current_params)

    instance = class_obj(**init_kwargs)
    output = instance.apply(**apply_kwargs)

    # remove input directory from string
    file_name = file_name.replace(current_params['in_dir'], '')

    output_file = os.path.splitext(file_name)[0]
    output_file = ''.join(slugify(output_file))
    output_file = os.path.join(current_params['out_dir'], output_file)
    output_file += '.{}'.format(current_params['output_extension'])

    with open(output_file, 'w') as o:
        if isinstance(output, list) or isinstance(output, dict):
            o.write(json.dumps(output))
        else:
            o.write(output)

    instance = None
    output = None


def execute_controllers(extractor_controllers):
    for key, item in extractor_controllers.items():
        class_obj = getattr(controllers, key)
        directory = item['in_dir']
        input_extension = item['input_extension']
        file_list = get_files_to_process(directory, input_extension)
        # get method parameters
        init_args = inspect.getargspec(class_obj.__init__)[0]
        apply_args = inspect.getargspec(class_obj.apply)[0]
        init_args.pop(init_args.index('self'))
        apply_args.pop(apply_args.index('self'))

        # check all parameters were set

        for init_arg in init_args:
            if init_arg == 'infile':
                continue
            if init_arg not in item:
                raise Exception('You must set {} option for {} section'.
                                format(init_arg, key))

        for apply_arg in apply_args:
            if apply_arg == 'infile':
                continue
            if apply_arg not in item:
                raise Exception('You must set {} option for {} section'.
                                format(apply_arg, key))

        # create output directory if it doesnt exist

        if not os.path.isdir(item['out_dir']):
            os.makedirs(item['out_dir'])

        # execute

        Parallel(n_jobs=int(item['n_jobs'])
                 )(delayed(parallel_execute_controller)(key,
                                                        item,
                                                        f,
                                                        init_args,
                                                        apply_args,
                                                        class_obj)
                   for f in file_list)
