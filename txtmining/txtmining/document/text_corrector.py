# -*- coding: utf-8 -*-
'''
Created on Apr 20, 2015

@author: Alejandro Molina
@rev: JMB
'''

# The usage will be something like:
# f = codecs.open(pathfile, "rU",  encoding='utf-8')
# buffer_in.write(f.read())
# undasher.correct(buffer_in)

from StringIO import StringIO
import abc
import re
from subprocess import Popen, PIPE


class AbstractCorrector(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def correct(self, stream_in):
        pass


class OnelineCorrector(AbstractCorrector):
    @staticmethod
    def correct(stream_in):
        # StringIO in StringIO out
        try:
            return StringIO(re.sub(" +", " ",
                            stream_in.getvalue().replace("\n", " ")))
        except Exception, e:
            print(u' Exception at OnelineCorrector.correct: {}'.format(e))


class DashCorrector(AbstractCorrector):

    @staticmethod
    def correct(stream_in):
        # StringIO in StringIO out
        try:
            contents = stream_in.getvalue()

            tofind = r'(?P<avant>[a-z])([-—–]\n)(?P<apres>[a-z])'
            toreplace = r'\g<avant>\g<apres>'
            dash_corrected = re.sub(tofind, toreplace, contents, re.UNICODE)

            return StringIO(dash_corrected)
        except Exception, e:
            print(u' Exception at DashCorrector.correct: {}'.format(e))


class AbreviationCorrector(AbstractCorrector):

    @staticmethod
    def correct(stream_in):
        # StringIO in StringIO out
        try:
            contents_bylines = [line for
                                line in stream_in.readlines()]
            # adding bizarre pattern in order to split after by looking it
            bizarre = '__€__'
            joined_lines = bizarre.join(contents_bylines)
            re.sub(bizarre + '+', bizarre, joined_lines)
            # This one is very good alternative :
            tofind = r'(?P<lastletter>.)(\.(\n)+' + \
                     bizarre + ')(?P<firstletter>[^(A-Z|\n)])'
            toreplace = r'\g<lastletter>. \g<firstletter>'
            (replaced, _) = re.subn(tofind,
                                    toreplace,
                                    joined_lines,
                                    re.UNICODE)
            # print(number_of_subs_made)
            return StringIO(replaced.replace(bizarre, ''))
        except Exception, e:
            print(u' Exception at AbreviationCorrector.correct: {}'.format(e))


class SentenceCorrector():
    def __init__(self, opennlp_bin, opennlp_mod):
        if not (opennlp_bin and opennlp_mod):
            raise ValueError('Environment variables OPENNLP_BIN, '
                             'OPENNLP_MODEL are not correct')
        self.openNLP_bin = opennlp_bin
        self.openNLP_model = opennlp_mod
        self.openNLP_command = "SentenceDetector"

    def correct(self, stream_in):
        # StringIO in StringIO out
        try:
            # see
            # https://docs.python.org/2/library/subprocess.html#subprocess.Popen
            p = Popen([self.openNLP_bin, self.openNLP_command,
                       self.openNLP_model],
                      stdout=PIPE, stdin=PIPE, stderr=None)
            # important to encode buffer to avoid unicode error
            # return StringIO(p.communicate(
            #                 input=stream_in.getvalue().encode('UTF-8'))[0].decode('UTF-8'))
            return StringIO(p.communicate(
                            input=stream_in.getvalue())[0])
        except Exception, e:
            raise e
