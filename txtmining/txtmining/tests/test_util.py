# -*- coding: utf-8 -*-

import unittest
import re
import os
from txtmining.base.util import (ResourceLoader)
from txtmining.tests.util import (get_ner_folder)

# TODO inlcude loader class
# class TestLoader(unittest.TestCase):
#     """Test resources files Load
#     """
#     def setUp(self):
#         pass

#     def test_none(self):
#         # Applies the same for empty string
#         nonetype = None
#         self.assertRaises(IOError, ResourceLoader.load_regexp, nonetype)

#     def test_nonstrtype(self):
#         nonstrtype = ['a', 120]
#         self.assertRaises(IOError, ResourceLoader.load_regexp, nonstrtype)

#     def test_inexistentpath(self):
#         inexistentpath = './inexistent'
#         self.assertRaises(IOError, ResourceLoader.load_regexp, inexistentpath)

#     def test_subdir(self):
#         subdir = 'test'
#         self.assertRaises(IOError, ResourceLoader.load_regexp, subdir)

#     def test_load_re(self):
#         r = ResourceLoader.load_regexp(os.path.join(get_ner_folder(),
#                                                     'lexpatterns_synonymy.re'))
#         self.assertIsInstance(r, re._pattern_type)

#     def test_load_dict(self):
#         d = ResourceLoader.load_dict(os.path.join(get_ner_folder(),
#                                                   'dicnames.json'))
#         self.assertIsInstance(d, dict)


if __name__ == '__main__':
    unittest.main()
