"""
Created on Jan 8, 2016.

@author: Alejandro Molina
"""


import os
import sys
import simplejson
import codecs
from txtmining.mlearning.ml_extract import data_from_annot


if __name__ == '__main__':

    usage = """
 Usage: generate_dataset.py dir/annotations dir/out label

 Read annotations format to get POSITIVE and NEGATIVE examples given a label:
 """

    if len(sys.argv) < 4:
        print(usage)
        sys.exit(1)

    anndir = sys.argv[1]
    outdir = sys.argv[2]
    label = sys.argv[3]

    if not (os.path.isdir(anndir) and
            os.path.isdir(outdir)):
        print("Invalid directory name in arguments.")
        sys.exit(0)

    positives = []
    negatives = []
    for afile in os.listdir(anndir):
        if afile.endswith(".JSON"):

            basename = afile.rsplit('.', 1)[0]

            # JSON annotation file
            json_file = os.path.join(anndir, basename+".JSON")
            print 'json_file', json_file
            with open(json_file, 'rb') as jfp:
                j = simplejson.load(jfp)

            # collect examples of each
            (p, n) = data_from_annot(j, label)

            positives.extend(p)
            negatives.extend(n)

    # once gathered all examples write a files
    pos_filename = 'pos_{}.txt'.format(label)
    pos_path = os.path.join(outdir, pos_filename)
    neg_filename = 'neg_{}.txt'.format(label)
    neg_path = os.path.join(outdir, neg_filename)

    with codecs.open(pos_path, 'w') as pf:
        for (text, label) in positives:
            text = text.replace('\r', '').replace('\n', '')
            if text:
                line = '{}\n'.format(text.encode('UTF-8'))
                pf.write(line)

    with codecs.open(neg_path, 'w') as nf:
        for (text, label) in negatives:
            text = text.replace('\r', '').replace('\n', '')
            if text:
                line = '{}\n'.format(text.encode('UTF-8'))
                nf.write(line)

