BROKER_URL = ('sqla+sqlite:///<absolute_path>'
              'celerydb.sqlite')
CELERY_RESULT_BACKEND = ('db+sqlite:///<absolute_path>'
                         'resultsdb.sqlite')
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ALWAYS_EAGER = False
