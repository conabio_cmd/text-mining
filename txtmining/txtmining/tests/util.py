# -*- coding: utf-8 -*-
'''
Created on Apr 22, 2015

@author: Daniel
'''

import os
import tempfile

import simplejson as json


def get_pdf_folder():
    current_path = os.path.realpath(__file__)
    current_path = os.path.dirname(current_path)
    return os.path.join(current_path, 'data/pdf')


def get_test_data_folder():
    current_path = os.path.realpath(__file__)
    current_path = os.path.dirname(current_path)
    return os.path.join(current_path, 'data')


def create_tmpfile_withnlines(no_lines):
    line = u'Lórem ipsum dolor sit amet, volumus comprehénsam eá víx, meá ' \
           u'prima mutat labore ád, cibo elítr signiferumque ea meá ' \
           u'principes.'
    text = ['{}\n'.format(line.encode('utf8')) for _ in xrange(no_lines)]

    _, temp_path = tempfile.mkstemp(text=True)
    with open(temp_path, 'w') as stream:
        stream.writelines(text)

    return temp_path


def flatten_list(l):
    return [item for sublist in l for item in sublist]


def get_regex_folder():
    current_path = os.path.realpath(__file__)
    current_path = os.path.dirname(current_path)
    return os.path.join(current_path, '../resources/regexp')


def get_ner_folder():
    current_path = os.path.realpath(__file__)
    current_path = os.path.dirname(current_path)
    return os.path.join(current_path, '../resources/ner')


def get_script_path():
    current_path = os.path.realpath(__file__)
    current_path = os.path.dirname(current_path)
    return os.path.join(current_path, '../../scripts/txtm.py')


def get_opennlp_model():
    current_path = os.path.realpath(__file__)
    current_path = os.path.dirname(current_path)
    # high precision
    return os.path.join(current_path, '../resources/models/es-iula.bin')


def get_ml_module():
    current_path = os.path.realpath(__file__)
    current_path = os.path.dirname(current_path)
    return os.path.join(current_path, '../mlearning/ml_features.py')


def is_json(json_strg):
    try:
        json.loads(json_strg)
    except ValueError:
        return False

    return True
