'''
Created on Sep 29, 2015

@author: Daniel
'''
import os
import txtmining
from string import Template
from tempfile import mkdtemp
from Naked.toolshed.shell import muterun_js
from simplejson import loads
from shutil import rmtree
import codecs
import re


class Taxonfinder():
    '''
    Class to detect scientific names using node-taxonfinder.
    Given a string, it will scan through the contents and
    use a dictionary-based approach to identifying which
    words and strings are latin scientific organism names.
    Taxonfinder will detect names at all ranks including
    Kingdom, Phylum, Class, Order, Family, Genus, Species,
    Subspecies and lots more.

    :param file_in: path to file to be processed
    '''

    def __init__(self):
        '''
        Constructor
        '''

    def get_index(self, file_in):
        '''
        :param file_in: path to file to be processed
        '''
        # Read template
        template_path = os.path.join(txtmining.__path__[0], '../',
                                     'external', 'node_modules',
                                     'taxonfinder_template.js')
        with open(template_path, 'r') as f:
            template_content = f.read()
        # Replace vars
        taxonfinder_path = os.path.dirname(template_path)
        template = Template(template_content)
        to_write = template.substitute(taxonfinder_path=taxonfinder_path,
                                       file=file_in)
        # Create script
        tmp_dir = mkdtemp()
        self.script_path = os.path.join(tmp_dir, 'taxonfinder.js')
        with open(self.script_path, 'w') as f:
            f.write(to_write)
        # run script
        response = muterun_js(self.script_path)

        # Delete tmpdir
        rmtree(tmp_dir)
        if response.exitcode == 0:
            stdout = response.stdout
            stdout = stdout.split('\n')
            # The first two lines are comments.
            # Information is in third row
            return loads(stdout[2])
        else:
            return None

    def tag_scienames(self, text, tag='SCIENAME'):
        # force unicode
        if not isinstance(text, unicode):
            text = u''+text.decode('UTF-8')

        # write index
        tmp_dir = mkdtemp()
        temp = os.path.join(tmp_dir, 'text_temp')
        with codecs.open(temp, 'w', encoding='UTF-8') as f:
            f.write(text)
        scienames_index = self.get_index(temp)
        # print scienames_index

        # put tags
        lines = text.split('\n')
        tagged_lines = lines
        for (line, name, canonical, (start, end)) in scienames_index:
            tagged = '{}'.format(tag)
            replacement = re.sub(name, tagged, tagged_lines[line-1])
            tagged_lines[line-1] = replacement

        rmtree(tmp_dir)

        return '\n'.join(tagged_lines)
