'''
Created on Apr 20, 2015

@author: Daniel
'''
import subprocess

from txtmining.base.util import DEFAULT_ENCODING, shell_quote
from txtmining.document.text_extractor import TextExtractor


class PDFBox(TextExtractor):
    '''
    classdocs
    '''

    def __init__(self,
                 bin_path,
                 output_dir):
        '''
        Constructor
        '''
        super(PDFBox, self).__init__(bin_path,
                                     output_dir)
        self._set_call()

    def extract(self, path_file):
        self._output_file = self._get_created_file_path(path_file)
        self._call += ' ' + shell_quote(path_file)
        self._call += ' ' + self._output_file
        try:
            super(PDFBox, self).extract()
            return super(PDFBox, self)._check_output()
        except subprocess.CalledProcessError as e:
            if "Document is encrypted" in e.output:
                print 'Document is encrypted'
            return None
        except Exception as e:
            return None

    def _set_call(self):
        self._call = "java -jar -Djava.awt.headless=true "
        self._call += self._bin
        self._call += ' ExtractText -force -debug -encoding '
        self._call += DEFAULT_ENCODING
