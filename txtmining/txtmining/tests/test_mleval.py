"""
Created on August 4, 2015.

@author: Alejandro Molina.
"""

import os
import unittest
from numbers import Number

from txtmining.base.util import (get_netineti_model,
                                 get_spaneti_model,
                                 get_eval_file,
                                 get_annot_csv)
from txtmining.tests.util import (get_test_data_folder)
from txtmining.document.netineti_indexer import SpaNeti
from txtmining.mlearning.ml_eval import (ScinamesEvaluator,
                                         index2classes,
                                         tagged_from_classes)
from txtmining.controllers.controllers import (TaxonfinderController,
                                               NetinetiController)


class ScinamesEvalTest(unittest.TestCase):

    def setUp(self):
        # same criteria
        self.consider_abbrev = True
        self.scitype = 'all'
        self.beta = 1
        self.simcutoff = 0.8
        # print self.simcutoff,'\t',self.beta,'\t',self.scitype,'\t',self.consider_abbrev

        # get SpaNeti scinames
        model = get_netineti_model()
        # model = get_spaneti_model()
        self.spaneti = SpaNeti(model)
        self.eval_file = get_eval_file()

        # load annotations reference
        annot_csv = get_annot_csv()
        self.scie = ScinamesEvaluator(annot_csv)

        # allow to compare long strings
        self.maxDiff = None

    # def test_boundary_precision(self):
    #     scinames = self.spaneti.get_indexf(self.eval_file,
    #                                        abbrev=self.consider_abbrev,
    #                                        sciname_type=self.scitype)
    #     evalresults = self.scie.eval(scinames,
    #                                  abbrev=self.consider_abbrev,
    #                                  sciname_type=self.scitype)
    #     (precision, recall, fmeasure) = evalresults
    #     self.assertTrue(precision >= 0 and precision <= 1)

    # def test_boundary_recall(self):
    #     scinames = self.spaneti.get_indexf(self.eval_file,
    #                                        abbrev=self.consider_abbrev,
    #                                        sciname_type=self.scitype)
    #     evalresults = self.scie.eval(scinames,
    #                                  abbrev=self.consider_abbrev,
    #                                  sciname_type=self.scitype)
    #     (precision, recall, fmeasure) = evalresults
    #     self.assertTrue(recall >= 0 and recall <= 1)

    # def test_boundary_fmeasure(self):
    #     scinames = self.spaneti.get_indexf(self.eval_file,
    #                                        abbrev=self.consider_abbrev,
    #                                        sciname_type=self.scitype)
    #     evalresults = self.scie.eval(scinames,
    #                                  abbrev=self.consider_abbrev,
    #                                  sciname_type=self.scitype)
    #     (precision, recall, fmeasure) = evalresults
    #     self.assertTrue(fmeasure >= 0 and fmeasure <= 1)


class EvalTest(unittest.TestCase):

    def setUp(self):
        self.testfile = os.path.join(get_test_data_folder(),
                                     'nombres_y_usos_en_25_lineas.txt')
        # get index
        netineti = NetinetiController()
        self.index = netineti.apply(self.testfile)
        # allow to compare long strings
        self.maxDiff = None

    def test_index2classes(self):
        c = index2classes(self.testfile, self.index)
        self.assertIsInstance(c, list)

    def test_tagged_from_classes(self):
        labels = ['WORD', 'SCIENAME']
        clases = index2classes(self.testfile, self.index)
        s = tagged_from_classes(self.testfile, clases, labels)
        self.assertIsInstance(s, unicode)

if __name__ == '__main__':
    unittest.main()
