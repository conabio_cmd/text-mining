#########################################################
# Config file example for Text Mining preprocessing tasks.
#
# tasks:
#
#     - Metadata extraction
#     - OCR/parsing of PDF file,
#     - extraction of plain UTF-8 txt characters,
#     - parsing correction (abreviations and dash errors) of OCR/PDF parsing,
#     - sentence segmentation (frontiers detection),
#     - index extraction,
#     - global names efficent service call,
#     - regular expression extraction.
#
# Created on Jun 3, 2015
# @author: Alejandro Molina



[DEFAULT]
# The directory where input files will be read if you do not precise at each processing step.
IN_DIR : ./txtmining/tests/data/pdf
# The directory where output files will be send if you do not precise at one processing step.
OUT_DIR : ./txtmining/tests/data/fromOCR
# Number of processor to use.
# If you have a multicore CPU with N cores, a good option is N-1.
n_jobs : 1
# Where the apache tika jar file is located.
TIKA_BIN : ../utils/tika-app-1.10.jar
# Where Poppler pdfinfo binary file is located.
PDFINFO_BIN : /usr/local/bin/pdfinfo
# Where Apache PDFBox jar file is located.
PDFBOX_BIN : ../utils/pdfbox-app-1.8.9.jar
# Where Poppler pdftotext
PDFTOTEXT_BIN : /usr/local/bin/pdftotext



[MetadataController]
# Whether pdf metadata should be extracted or not.
APPLY : True
# At which step this process must be applied.
STEP : 0
# Which type of parser to apply, valid options are: ['tika', 'pdfinfo', 'all']
METADATA_TYPE : all
# Expected input extension for this process.
INPUT_EXTENSION : pdf
# Expected extension after this process.
OUTPUT_EXTENSION : meta



[PDFparserController]
# Whether raw text should be extracted or not.
APPLY : True
# At which step this process must be applied.
STEP : 0
# Which type of PDF parser to apply, valid options are: ['tika', 'pdfbox', 'pdftotext', 'all']
PARSER_TYPE : all
# Expected input extension for this process.
INPUT_EXTENSION : pdf
# Expected extension after this process.
OUTPUT_EXTENSION : parsed.txt



[PreprocessingController]
# Whether text should be corrected or not.
APPLY : True
# At which step this process must be applied.
STEP : 1
# Where Apache opennlp binary file is located.
OPENNLP_BIN : ../utils/apache-opennlp-1.5.3/bin/opennlp
# Where the sentence detection model is located.
OPENNLP_MOD : ./txtmining/resources/models/es-iula.bin
# Which type of correction to apply, 'all' is highly recommended unless you are testing another correction model. Anyway, valid options are: ['dash', 'abrv', 'onel', 'sent', 'all']
CORRECTION_TYPE : ['all']
# The directory where input files will be read for this process.
# This overwrites the value from [DEFAULT SECTION].
IN_DIR : ./txtmining/tests/data/fromOCR
# The directory where output files will be send after this process.
# This overwrites the value from [DEFAULT SECTION].
OUT_DIR : ./txtmining/tests/data/segmented
# Expected input extension for this process.
INPUT_EXTENSION : txt
# Expected extension after this process.
OUTPUT_EXTENSION : txt




[ReIndexController]
# Whether scientific names should be seek using a regular expression file or not.
APPLY : True
# At which step this process must be applied.
STEP : 2
# Where regular expression file is located.
REGEXP_FILE : ./txtmining/resources/regexp/regexp_NombreCientifico.re
# The directory where input files will be read for this process.
# This overwrites the value from [DEFAULT SECTION].
IN_DIR : ./txtmining/tests/data/segmented
# The directory where output files will be send after this process.
# This overwrites the value from [DEFAULT SECTION].
OUT_DIR : ./txtmining/tests/data/indexes
# Expected input extension for this process.
INPUT_EXTENSION : txt
# Expected extension after this process.
OUTPUT_EXTENSION : re.json


[GnIndexController]
# Whether scientific names should be seek using global names resolver or not.
APPLY : True
# At which step this process must be applied.
STEP : 2
# Expected input extension for this process.
INPUT_EXTENSION : txt
# Expected extension after this process.
OUTPUT_EXTENSION : gn.json
# The directory where input files will be read for this process.
# This overwrites the value from [DEFAULT SECTION].
IN_DIR : ./txtmining/tests/data/segmented
# The directory where output files will be send after this process.
# This overwrites the value from [DEFAULT SECTION].
OUT_DIR : ./txtmining/tests/data/indexes



[FragmentController]
# Whether to extract traditional use fragments or not.
APPLY : True
# At which step this process must be applied.
STEP : 2
# Where regular expression directory is located.
REGEX_DIR : ./txtmining/resources/regexp/
# How many lines before the main matched line to include.
NLINES_BEFORE : 4
# How many lines after the main matched line to include.
NLINES_AFTER : 2
# The directory where input files will be read for this process.
# This overwrites the value from [DEFAULT SECTION].
IN_DIR : ./txtmining/tests/data/segmented
# The directory where output files will be send after this process.
# This overwrites the value from [DEFAULT SECTION].
OUT_DIR : ./txtmining/tests/data/fragments
# Expected input extension for this process.
INPUT_EXTENSION : txt
# Expected extension after this process.
OUTPUT_EXTENSION : frgmt.json