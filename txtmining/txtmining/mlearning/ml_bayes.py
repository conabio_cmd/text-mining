# -*- coding: utf-8 -*-

"""
Created on Nov. 30, 2015.

@author: Alejandro Molina.

Classifiers for NLP tasks

Alejandro Molina, Dec. 2015
"""

import nltk
from sklearn import cross_validation
from txtmining.mlearning.ml_features import alias_features


class BayesClassifier(nltk.NaiveBayesClassifier):
    """docstring for BayesClassifier"""
    def __init__(self, feat_function):
        self.feat_extractor = feat_function
        self.featuresets = None

    def train(self, data, feat_file):
        self.featuresets = [(self.feat_extractor(txt, feat_file), tag) for (txt, tag) in data]
        classifier = nltk.NaiveBayesClassifier.train(self.featuresets)
        return classifier

    def cross_validation(self, n_folds=3, shuffle=False, random_state=None):
        """
        n : int
            Total number of elements.

        n_folds : int, default=3
            Number of folds. Must be at least 2.

        shuffle : boolean, optional
            Whether to shuffle the data before splitting into batches.

        random_state : None, int or RandomState
            When shuffle=True, pseudo-random number generator state used for shuffling. If None, use default numpy RNG for shuffling.
        """
        n = len(self.featuresets)
        cv = cross_validation.KFold(n, n_folds=n_folds, shuffle=shuffle, random_state=random_state)
        # 
        acc_array = []
        for traincv, testcv in cv:
            classifier = nltk.NaiveBayesClassifier.train(self.featuresets[traincv[0]:traincv[len(traincv)-1]])
            accuracy = nltk.classify.util.accuracy(classifier, self.featuresets[testcv[0]:testcv[len(testcv)-1]])
            # print 'accuracy:', accuracy
            acc_array.append(accuracy)
        # return mean accuracy
        return sum(acc_array) / float(len(acc_array))


if __name__ == '__main__':
    bc = BayesClassifier(alias_features)
    good_data = [(u'Oui', 'POS'), (u'Non', 'NEG')]
    bc.train(good_data)
