#!/bin/bash

DIR="$(pwd)"

export OPENNLP_MODEL=$DIR"/txtmining/txtmining/resources/models/es-iula.bin"
export REGEX_DIR=$DIR"/txtmining/txtmining/resources/regexp/"

if [ -z $OPENNLP_BIN ]; then
    curl http://apache.webxcreen.org//opennlp/opennlp-1.5.3/apache-opennlp-1.5.3-bin.tar.gz | tar zxvf -
    export OPENNLP_BIN=$DIR"/apache-opennlp-1.5.3/bin/opennlp"
fi

if [ -z $TIKA_BIN ]; then
    curl -O http://apache.webxcreen.org/tika/tika-app-1.10.jar
    export TIKA_BIN=$DIR"/tika-app-1.10.jar"
fi

if [ -z $PDFBOX_BIN ]; then
    curl -O http://apache.webxcreen.org/pdfbox/1.8.9/pdfbox-app-1.8.9.jar
    export PDFBOX_BIN=$DIR"/pdfbox-app-1.8.9.jar"
fi
