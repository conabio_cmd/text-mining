'''
Created on Apr 21, 2015

@author: Daniel
'''
import subprocess

from txtmining.base.util import shell_quote
from txtmining.document.text_extractor import TextExtractor


class PDFtoText(TextExtractor):
    '''
    classdocs
    '''

    def __init__(self,
                 bin_path,
                 output_dir):
        '''
        Constructor
        '''
        super(PDFtoText, self).__init__(bin_path,
                                        output_dir)
        self._set_call()

    def extract(self, path_file):
        self._output_file = self._get_created_file_path(path_file)
        self._call += ' ' + shell_quote(path_file)
        self._call += ' ' + self._output_file
        try:
            super(PDFtoText, self).extract()
            return super(PDFtoText, self)._check_output()
        except subprocess.CalledProcessError:
            return None

    def _set_call(self):
        self._call = self._bin
