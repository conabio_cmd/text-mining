# -*- coding: utf-8 -*-
"""
Created on Apr 20, 2015

@author: Alejandro Molina
"""
import abc
import codecs
import os
import re
from txtmining.base.util import DEFAULT_ENCODING

# NOTES
# re.LOCALE Make \w, \W, \b, \B, \s and \S dependent on the current locale.
# re.UNICODE Make \w, \W, \b, \B, \d, \D, \s and \S dependent on the Unicode


class AbstractIndexBuilder(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def get_index(self):
        pass


class RegexIndexBuilder(AbstractIndexBuilder):
    def __init__(self, file_in, compiled_regex,):
        # Initialize from compiled regex
        self.FILE = codecs.open(file_in, 'r', 'UTF-8')
        self.index = dict()
        self.index["file"] = os.path.basename(file_in)
        self.RE = compiled_regex

    def get_index(self):
        for i, line in enumerate(self.FILE):
            for m in self.RE.finditer(line, re.UNICODE):
                for group, value in m.groupdict().iteritems():
                    if group in self.index.keys():
                        self.index[group].append({u"lineNumber": i+1,
                                                  u"instance": value,
                                                  u"offsetStart":
                                                  m.start(group),
                                                  u"offsetEnd": m.end(group)})
                    else:
                        self.index[group] = [({u"lineNumber": i + 1,
                                               u"instance": value,
                                               u"offsetStart": m.start(group),
                                               u"offsetEnd": m.end(group)})]
        if len(self.index.keys()) > 1:
            return [(d["lineNumber"], d["instance"],
                    (d["offsetStart"], d["offsetEnd"]))
                    for d in self.index["scientificNames"]]
        else:
            return []


class RegexGroup(object):
    """RegexGroup creates a regex given:
        - a list of unicodes or a file with the list
        - optional re.flags

        usage:
        print RegexGroup(['Acacia farnesiana', 'Opuntia guatemalensis'])
        will produce:
        (?P<scientificNames>
          (?P<genera>\bAcacia\b|\bOpuntia\b|\b((A\b|\bO){1}\.)){1}\s+
          (?P<species>\b'+farnesiana\b|\bguatemalensis\b|\b((s|S){1,1}pp{0,1}\.{0,1})\b){1,1})
        """
    def __init__(self, list_names, flags=0):
        if isinstance(list_names, basestring):
            if not os.path.isfile(list_names):
                raise ValueError('If list_names is string, it should'
                                 'be a file. {0} is not a file'.
                                 format(list_names))
            # Load the file
            with codecs.open(list_names, 'r', DEFAULT_ENCODING) as f:
                file_lines = f.readlines(list_names)
            list_names = [name.replace('\n', '') for name in file_lines]

        self.__verif_args(list_names)

        # create regexp
        expression = self.__create_regex(list_names)
        try:
            self.re = re.compile(expression, flags)
        except Exception, e:
            raise e

    def __verif_args(self, list_names):
        if not isinstance(list_names, list):
            raise ValueError('Argument is not a list.')

        for name in list_names:
            if not isinstance(name, basestring):
                raise ValueError('Some elements in list are not string type.')

            if len(name.split(' ')) != 2:
                raise ValueError('Some elements in list are not length=2.')

    def __create_regex(self, list_names):
        sep = r'\b|\b'
        genus = []
        speci = []
        abbrv = []
        for name in list_names:
            splited = name.split(' ')
            abbrv.append(splited[0][0].upper())
            genus.append(splited[0])
            speci.append(splited[1])

        genus_str = r''+sep.join(genus)
        speci_str = r''+sep.join(speci)
        abbrv_str = r''+sep.join(abbrv)

        expression = r'(?P<scientificNames>(?P<genera>\b'+genus_str+r'\b|\b(('+abbrv_str+r'){1}\.)){1}\s+(?P<species>\b'+speci_str+r'\b|\b((s|S){1,1}pp{0,1}\.{0,1})\b){1,1})'
        return expression

    def __str__(self):
        return self.re.pattern.replace(self.sep,
                                       self.sep+'\n').encode(DEFAULT_ENCODING)

    def __repr__(self):
        return self.re.pattern.replace(self.sep,
                                       self.sep+'\n').encode(DEFAULT_ENCODING)


# Utilities

def correct_gnoffsets(resolved_names, string):
    """ correct global names offsets to python re style.
    returns a list of tuples [(name,
                              (corrected start, corrected end))]

    - resolved_names must be obtained from global names answer 'j' this way:

    [n for n in j["names"] for resolved in j['resolved_names']
     if (resolved['is_known_name'] and
     d['identifiedName'] == resolved['supplied_name_string'])]"""

    corrected_ofssets = []
    found_set = set([name['identifiedName'] for name in resolved_names])
    for name in found_set:
        for m in re.finditer(name, string, re.UNICODE | re.IGNORECASE):
            corrected_ofssets.append((name, m.span()))
    return corrected_ofssets
