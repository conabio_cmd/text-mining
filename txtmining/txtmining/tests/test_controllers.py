# -*- coding: utf-8 -*-
"""
Created on May 20, 2015.

@author: Alejandro Molina.
"""
import os
import codecs
import unittest
import tempfile
import nltk
from tempfile import (mkdtemp)
import ConfigParser
from txtmining.controllers.controllers import (MetadataController,
                                               PDFparserController,
                                               PreprocessingController,
                                               ReIndexController,
                                               FragmentController,
                                               TaxonfinderController,
                                               NetinetiController,
                                               AliasController,
                                               TrainController
                                               )
from txtmining.tests.util import (get_test_data_folder, get_regex_folder,
                                  get_opennlp_model, get_ml_module)
from txtmining.base.util import (get_netineti_model)
import warnings


class TestBaseController():

    """basic tests for all Controllers."""

    def t_nonetype(self, controller):
        nonetype = None
        self.assertRaises(IOError, controller.apply, nonetype)

    def t_nonstrtype(self, controller):
        nonstrtype = ['a', 120]
        self.assertRaises(IOError, controller.apply, nonstrtype)

    def t_emptystr(self, controller):
        emptystr = ''
        self.assertRaises(IOError, controller.apply, emptystr)

    def itest_inexistentpath(self, controller):
        inexistentpath = os.path.join(get_test_data_folder(), '/noexiste')
        self.assertRaises(IOError, controller.apply, inexistentpath)

    def t_subdir(self, controller):
        subdir = os.path.join(get_test_data_folder(), 'pdf/subdir')
        self.assertRaises(IOError, controller.apply, subdir)


class TestMetadataController(TestBaseController, unittest.TestCase):

    """test for MetadataController."""

    def setUp(self):
        tika_bin_path = os.getenv('TIKA_BIN')
        pdf_info_bin_path = os.getenv('PDF_INFO_BIN')
        self.metac = MetadataController(tika_bin_path, pdf_info_bin_path)

        self.notpdf = os.path.join(
            get_test_data_folder(),
            'pdf/Partes_del_cuerpo_en_el_Codice_Badiano1.htm')
        self.pdf = os.path.join(get_test_data_folder(), 'pdf/Flora_urbana.pdf')

        self.maxDiff = None

    # tests from Base classe:
    def test_nonetype(self):
        super(TestMetadataController, self).t_nonetype(self.metac)

    def test_nonstrtype(self):
        super(TestMetadataController, self).t_nonstrtype(self.metac)

    def test_emptystr(self):
        super(TestMetadataController, self).t_emptystr(self.metac)

    def itest_inexistentpath(self):
        super(TestMetadataController, self).itest_inexistentpath(self.metac)

    def test_subdir(self):
        super(TestMetadataController, self).t_subdir(self.metac)

    def test_notpdf(self):
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            self.metac.apply(self.notpdf)
            self.assertEqual(len(w), 1)

    def test_pdf(self):
        d = self.metac.apply(self.pdf)
        self.assertIsInstance(d, dict)


# class TestPDFparserController(TestBaseController, unittest.TestCase):

#     """test for PDFparserController."""

#     def setUp(self):
#         tika_bin_path = os.getenv('TIKA_BIN')
#         pdfbox_path = os.getenv('PDFBOX_BIN')
#         pdftotext_path = os.getenv('PDF_TO_TEXT_BIN')
#         self.parc = PDFparserController(mkdtemp(),
#                                         tika_bin_path,
#                                         pdfbox_path,
#                                         pdftotext_path)
#         self.notpdf = os.path.join(get_test_data_folder(),
#                                    'pdf/Partes_del_cuerpo_en_el'
#                                    '_Codice_Badiano1.htm')
#         self.pdf = os.path.join(get_test_data_folder(),
#                                 'pdf/Flora_urbana.pdf')
#         self.tika_expected = """
# EN AGUASCALIENTES
# LABIODIVERSIDAD

# ESTUDIO DE ESTADO

# 7897099 000450

# ISBN 978-970-9000-45-0"""
#         self.pdfbox_expected = """EN AGUASCALIENTES
# LABIODIVERSIDAD
# ESTUDIO DE ESTADO
# 7897099 000450
# ISBN 978-970-9000-45-0"""
#         self.pdftotext_expected = """LABIODIVERSIDAD
# EN AGUASCALIENTES
# ESTUDIO DE ESTADO

# """

#     # tests from Base classe:
#     def test_nonetype(self):
#         super(TestPDFparserController,
#               self).t_nonetype(self.parc)

#     def test_nonstrtype(self):
#         super(TestPDFparserController,
#               self).t_nonstrtype(self.parc)

#     def test_emptystr(self):
#         super(TestPDFparserController,
#               self).t_emptystr(self.parc)

#     def itest_inexistentpath(self):
#         super(TestPDFparserController,
#               self).itest_inexistentpath(self.parc)

#     def test_subdir(self):
#         super(TestPDFparserController,
#               self).t_subdir(self.parc)

#     def test_notpdf(self):
#         with warnings.catch_warnings(record=True) as w:
#             warnings.simplefilter("always")
#             self.parc.apply(self.notpdf)
#             self.assertEqual(len(w), 1)

#     def test_tikapath(self):
#         control_ans = self.parc.apply(self.pdf,
#                                       parser_type='tika')
#         self.assertTrue(control_ans['tika_out'].
#                         endswith('flora-urbana.pdf.txt'))

#     def test_tikaextraction(self):
#         txt = open(self.parc.apply(self.pdf,
#                                    parser_type='tika')['tika_out']).read()
#         self.assertEqual(self.tika_expected,
#                          txt[:len(self.tika_expected)])

#     def test_pdfboxpath(self):
#         control_ans = self.parc.apply(self.pdf, parser_type='pdfbox')
#         self.assertTrue(control_ans['pdfbox_out'].
#                         endswith('flora-urbana.pdf.txt'))

#     def test_pdfboxextraction(self):
#         txt = open(self.parc.apply(self.pdf,
#                                    parser_type='pdfbox')['pdfbox_out']).read()
#         self.assertEqual(self.pdfbox_expected,
#                          txt[:len(self.pdfbox_expected)])

#     def test_pdftotextpath(self):
#         control_ans = self.parc.apply(self.pdf,
#                                       parser_type='pdftotext')
#         self.assertTrue(control_ans['pdftotext_out'].
#                         endswith('flora-urbana.pdf.txt'))

#     def test_pdftotextextraction(self):
#         f = self.parc.apply(self.pdf, parser_type='pdftotext')['pdftotext_out']
#         txt = open(f).read()
#         self.assertEqual(self.pdftotext_expected,
#                          txt[:len(self.pdftotext_expected)])


class TestPreprocessingController(TestBaseController, unittest.TestCase):

    """test for PreprocessingController."""

    def setUp(self):

        self.str = """Main activity is agriculture, either as a direct source of food (mainly traditional milpa, i.e.
corn and squash bean fields crops) or as a source of cash income (vegetables).

The most diverse families were Fabaceae (8 species), Caesalpiniaceae (5 species), Euphor-
biaceae (4 species), Arecaceae and Sapotaceae Turner et al.
, amounting to 35% of the total number of species in this vegetation type,
while the most diverse genera were Coccoloba, Caesalpinia, Croton and Lonchocarpus (2 species each) (Table 5).

The leaves of several palm species in the medium statured forest, mainly T.
radiata and S. yapa, are employed for construction of roofs and walls of palapas."""
        _, temp_path = tempfile.mkstemp(suffix='.txt', text=True)
        with open(temp_path, 'w') as stream:
            stream.write(self.str)

        self.txtfile = temp_path

        self.preproc = PreprocessingController(os.getenv('OPENNLP_BIN'),
                                               get_opennlp_model())

        self.maxDiff = None

    def test_nonetype(self):
        super(TestPreprocessingController,
              self).t_nonetype(self.preproc)

    def test_nonstrtype(self):
        super(TestPreprocessingController,
              self).t_nonstrtype(self.preproc)

    def test_emptystr(self):
        super(TestPreprocessingController,
              self).t_emptystr(self.preproc)

    def itest_inexistentpath(self):
        super(TestPreprocessingController,
              self).itest_inexistentpath(self.preproc)

    def test_subdir(self):
        super(TestPreprocessingController,
              self).t_subdir(self.preproc)

    def test_nottxt(self):
        nottxt = os.path.join(get_test_data_folder(),
                              'pdf/'
                              'Partes_del_cuerpo_en_el_Codice_Badiano1.htm')
        # self.assertRaisesRegexp(IOError,
        #                         'Input file is not a text file.',
        #                         self.preproc.apply, nottxt)
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("error")
            self.assertRaises(UserWarning,
                              self.preproc.apply,
                              nottxt)

    def test_dash(self):
        """Assert correction of hyphens.
        from :
            Euphor-
            biaceae
        to :
            Euphorbiaceae
        """

        undashed = self.preproc.apply(self.txtfile, correction_type=['dash'])
        lines = undashed.splitlines()
        expected = 'The most diverse families were Fabaceae (8 species), ' \
                   'Caesalpiniaceae (5 species), Euphorbiaceae (4 species), ' \
                   'Arecaceae and Sapotaceae Turner et al.'
        self.assertEqual(expected, lines[3])

    def test_abrv(self):
        """Assert correction of abbreviatures.
        from :
            i.e.
            corn
        to :
            i.e. corn
        """

        abrv = self.preproc.apply(self.txtfile, correction_type=['abrv'])
        lines = abrv.splitlines()
        expected = 'Main activity is agriculture, either as a direct source' \
                   ' of food (mainly traditional milpa, i.e. corn and squash' \
                   ' bean fields crops) or as a source of cash income' \
                   ' (vegetables).'
        self. assertEqual(expected, lines[0])

    def test_onel(self):
        onel = self.preproc.apply(self.txtfile, correction_type=['onel'])
        self.assertTrue(len(onel) > 0)
        self.assertFalse('\n' in onel)

    def test_sent(self):
        """Assert correction of sentence frontiers.
        from :
            mainly T.
            radiata
        to :
            mainly T. radiata
        """
        sents = self.preproc.apply(self.txtfile, correction_type=['sent'])
        lines = sents.splitlines()
        expected = 'The leaves of several palm species in the medium' \
                   ' statured forest, mainly T. radiata and S. yapa,' \
                   ' are employed for construction of roofs and walls' \
                   ' of palapas.'
        self.assertEqual(expected, lines[2])

    def test_all(self):
        sents = self.preproc.apply(self.txtfile, correction_type=['all'])
        lines = sents.splitlines()
        expected = ['Main activity is agriculture, either as a direct source'
                    ' of food (mainly traditional milpa, i.e. corn and squash'
                    ' bean fields crops) or as a source of cash income'
                    ' (vegetables).', 'The most diverse families were Fabaceae'
                    ' (8 species), Caesalpiniaceae (5 species), Euphorbiaceae'
                    ' (4 species), Arecaceae and Sapotaceae Turner et al. ,'
                    ' amounting to 35% of the total number of species in this'
                    ' vegetation type, while the most diverse genera were '
                    'Coccoloba, Caesalpinia, Croton and Lonchocarpus '
                    '(2 species each) (Table 5).', 'The leaves of several palm'
                    ' species in the medium statured forest, mainly T. radiata'
                    ' and S. yapa, are employed for construction of roofs and'
                    ' walls of palapas.', '']
        self.assertListEqual(expected, lines)


class TestReIndexController(TestBaseController, unittest.TestCase):

    """test for ReIndexController."""

    def setUp(self):
        self.regexp = os.path.join(get_regex_folder(),
                                   'regexp_NombreCientifico.re')

        self.indexer = ReIndexController(self.regexp)

        self.nottxt = os.path.join(get_test_data_folder(),
                                   'pdf/'
                                   'Partes_del_cuerpo_en_el_Codice_Badiano1'
                                   '.htm')

        self.txtfile = os.path.join(get_test_data_folder(),
                                    'fromOCR/'
                                    'docs__subdirTraditional_ecological'
                                    '_knowledge_Solferino_Quintana_Roo.pdf'
                                    '.txt')

        self.namesexpect = [(22, u'Manilkara zapota', (90, 106)),
                            (86, u'Manilkara zapota', (49, 65)),
                            (4797, u'Manilkara zapota', (39, 55)),
                            (4798, u'Dendropanax arboreus', (54, 74)),
                            (4806, u'Erythroxylum confusum', (50, 71)),
                            (12135, u'M. zapota', (52, 61)),
                            (12135, u'T. radiata', (70, 80)),
                            (12136, u'Bursera simaruba', (58, 74)),
                            (12137, u'H. campechianum', (35, 50)),
                            (12137, u'E. confusum', (59, 70)),
                            (12138, u'A. wrightii', (43, 54)),
                            (12165, u'Thrinax radiata', (33, 48)),
                            (12179, u'C. readii', (68, 77)),
                            (12180, u'T. radiata', (40, 50)),
                            (12183, u'Hampea trilobata', (44, 60)),
                            (12183, u'M. brownei', (61, 71)),
                            (12211, u'Thrinax radiata', (37, 52)),
                            (12212, u'M. zapota', (67, 76)),
                            (12243, u'Haematoxylum campechianum', (33, 58)),
                            (12244, u'H. campechianum', (64, 79)),
                            (12245, u'E. confusum', (52, 63)),
                            (12250, u'S. yapa', (45, 52)),
                            (12251, u'T. radiata', (51, 61)),
                            (12264, u'Dalbergia glabra', (40, 56)),
                            (12287, u'Sabal yapa', (45, 55)),
                            (12437, u'Pseudophoenix sargentii', (77, 100))]

        self.maxDiff = None

    # tests from Base classe:
    def test_nonetype(self):
        super(TestReIndexController,
              self).t_nonetype(self.indexer)

    def test_nonstrtype(self):
        super(TestReIndexController,
              self).t_nonstrtype(self.indexer)

    def test_emptystr(self):
        super(TestReIndexController,
              self).t_emptystr(self.indexer)

    def itest_inexistentpath(self):
        super(TestReIndexController,
              self).itest_inexistentpath(self.indexer)

    def test_subdir(self):
        super(TestReIndexController,
              self).t_subdir(self.indexer)

    def test_nottxt(self):
        # self.assertRaisesRegexp(IOError,
        #                         'Input file is not a text file.',
        #                         self.indexer.apply, self.nottxt)
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("error")
            self.assertRaises(UserWarning,
                              self.indexer.apply,
                              self.nottxt)

    def test_noregfile(self):
        self.assertRaisesRegexp(ValueError,
                                'regexp_file must be a valid file',
                                ReIndexController, '/devnull')

    def test_index(self):
        self.assertListEqual(self.namesexpect,
                             self.indexer.apply(self.txtfile))


class TestFragmentController(unittest.TestCase):

    """test for FragmentController."""

    def setUp(self):
        self.fcontroller = FragmentController(get_regex_folder())
        self.txtfile = os.path.join(get_test_data_folder(),
                                    'segmented/003186.txt')
        self.maxDiff = None

    # tests from Base classe:
    def test_nonetype(self):
        nonetype = None
        self.assertRaises(IOError, self.fcontroller.apply, nonetype, 4, 2)

    def test_nonstrtype(self):
        nonstrtype = ['a', 120]
        self.assertRaises(IOError, self.fcontroller.apply, nonstrtype, 4, 2)

    def test_emptystr(self):
        emptystr = ''
        self.assertRaises(IOError, self.fcontroller.apply, emptystr, 4, 2)

    def test_inexistentpath(self):
        inexistentpath = os.path.join(get_test_data_folder(), '/noexiste')
        self.assertRaises(IOError, self.fcontroller.apply,
                          inexistentpath, 4, 2)

    def test_subdir(self):
        subdir = os.path.join(get_test_data_folder(), 'pdf/subdir')
        self.assertRaises(IOError, self.fcontroller.apply, subdir, 4, 2)

    def test_noreg(self):
        self.assertRaises(IOError, FragmentController, None)

    def test_inexistentpathreg(self):
        inexistentpath = os.path.join(get_test_data_folder(), '/noexiste')
        self.assertRaises(IOError, FragmentController, inexistentpath)

    def test_notneededfiles(self):
        existentpath = get_test_data_folder()
        self.assertRaises(IOError, FragmentController, existentpath)

    def test_fragment(self):
        f = self.fcontroller.apply(self.txtfile)
        self.assertIsInstance(f, list)


class TestTaxonfinderController(unittest.TestCase):

    """test for TaxonfinderController."""

    def setUp(self):
        self.infile = os.path.join(get_test_data_folder(),
                                   'mini.txt')
        self.controller = TaxonfinderController()

    def test_controller(self):
        taxons_found = self.controller.apply(self.infile)
        # Just check the output is array
        self.assertIsInstance(taxons_found, list)


class TestNetiNetiController(TestBaseController, unittest.TestCase):

    """test for NetiNetiController."""

    def setUp(self):

        self.nottxt = os.path.join(get_test_data_folder(),
                                   'pdf/'
                                   '0666826_A1.pdf')

        self.txtfile = os.path.join(get_test_data_folder(),
                                    'mini.txt')

        self.lines = codecs.open(self.txtfile, 'r', 'UTF-8').readlines()

        # SpaNeti
        language = 'en'
        self.spaneti = NetinetiController(language)

        self.maxDiff = None

    # tests from Base classe:
    def test_nonetype(self):
        super(TestNetiNetiController,
              self).t_nonetype(self.spaneti)

    def test_nonstrtype(self):
        super(TestNetiNetiController,
              self).t_nonstrtype(self.spaneti)

    def test_emptystr(self):
        super(TestNetiNetiController,
              self).t_emptystr(self.spaneti)

    def itest_inexistentpath(self):
        super(TestNetiNetiController,
              self).itest_inexistentpath(self.spaneti)

    def test_subdir(self):
        super(TestNetiNetiController,
              self).t_subdir(self.spaneti)

    def test_nottxt(self):
        # with warnings.catch_warnings(record=True) as w:
        #     warnings.simplefilter("always")
        #     self.assertRaises(UnicodeDecodeError,
        #                       self.spaneti.apply,
        #                       self.nottxt)
        #     self.assertEqual(len(w), 1)
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("error")
            self.assertRaises(UserWarning,
                              self.spaneti.apply,
                              self.nottxt)

    def test_spaneti_index(self):
        # assert each entry in index, eg. (426, 'Nicotiana glauca', (131, 147))
        spaneti_index = self.spaneti.apply(self.txtfile,
                                           abbrev=True,
                                           sciname_type='all')
        for (num, name, (start, end)) in spaneti_index:
            # print num, name.encode('UTF-8'), (start, end)
            self.assertEqual(name, self.lines[num-1][start:end])
            pass
    pass


class TestTrainController(unittest.TestCase):

    """test for TaxonfinderController."""

    def setUp(self):
        self.fakedir = os.path.join(get_test_data_folder(), 'inexistentpath')
        self.wrong_data = [(1, 'x')]
        self.good_data = [(u'Oui', 'POS'), (u'Non', 'NEG')]
        self.created_nbayes_pickle = ''
        nltk.download('punkt')

        # set configuration file values for test
        conf_path = os.path.join(get_test_data_folder(), 'train.ini')
        config = ConfigParser.SafeConfigParser()
        with open(conf_path, 'r') as cf:
            config.readfp(cf)

        ml_dir = get_ml_module()
        config.set('nbayes', 'FEATFUNCTION_PATH', ml_dir)

        # create tmp output dir
        new_configuration_file = os.path.join(mkdtemp(), 'test.conf')
        with open(new_configuration_file, 'w') as c:
            config.write(c)
        # the final config to be read
        self.config = new_configuration_file

    def tearDown(self):
        if self.created_nbayes_pickle:
            os.remove(self.created_nbayes_pickle)

    def test_outdir(self):
        trainer = TrainController(classifier_type='all',
                                  config=self.config,
                                  train_set=self.good_data,
                                  out_modeldir=self.fakedir)
        self.assertTrue(os.path.isdir(trainer.out_modeldir))

    def test_dataformat(self):
        self.failUnlessRaises(ValueError,
                              TrainController,
                              classifier_type='all',
                              config=self.config,
                              train_set=self.wrong_data,
                              out_modeldir=self.fakedir)

    def test_nbayes_pickle(self):
        trainer = TrainController(classifier_type='nbayes',
                                  config=self.config,
                                  train_set=self.good_data,
                                  out_modeldir=mkdtemp())
        result = trainer.apply()
        self.created_nbayes_pickle = result['nbayes']
        self.assertTrue(os.path.exists(self.created_nbayes_pickle))


class TestAliasController(unittest.TestCase):

    """test for AliasFinder."""

    def setUp(self):
        self.infile = os.path.join(get_test_data_folder(),
                                   'mini.txt')

        self.controller = AliasController(arg1_label='Nombre_comun',
                                          arg2_label='Nombre_cientifico',
                                          rel_label='alias',
                                          taxon_indexer='Taxonfinder',
                                          sciname_type='all')

        self.expected_ann = os.path.join(get_test_data_folder(),
                                         'Taxonfinder',
                                         'mini.ann')

    def tearDown(self):
        if os.path.isfile(self.expected_ann):
            os.remove(self.expected_ann)
            os.removedirs(os.path.join(get_test_data_folder(), 'Taxonfinder'))

    def test_controller(self):
        output = self.controller.apply(self.infile,
                                       out_dir=get_test_data_folder())

        # check ann file writen
        self.assertTrue(output['Taxonfinder'] == self.expected_ann)

if __name__ == '__main__':
    unittest.main()
