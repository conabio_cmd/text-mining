# -*- coding: utf-8 -*-

"""
Created on Jan. 2016.

@author: Alejandro Molina.

Module to use the API CONABIO

MexBioAPI
Use example:

    bioapi = MexBioAPI(taxon)
    api_str = bioapi.get()
    if api_str:
        common_names = bioapi.common_names(api_str)

"""

import os
import math
import re
import string
import codecs
import simplejson
import requests
import ast
import tempfile
from difflib import SequenceMatcher as smatcher

from txtmining.base.util import global_offsets
from txtmining.document.taxonfinder import Taxonfinder
from txtmining.converters.brat import BratConv


class MexBioAPI(object):
    """MexBioAPI: interfaces mexican species API"""
    URL_BASE = 'http://enciclovida.mx/busquedas/resultados.json/'

    def __init__(self, taxon):
        self.params = {'busqueda': 'basica',
                       'nombre': taxon
                       }
        try:
            self.r = requests.get(self.URL_BASE, params=self.params)
        except Exception, e:
            raise e

    def get(self):
        # HTTP OK
        if self.r.status_code == 200:
            api_str = simplejson.loads(self.r.text, encoding='UTF-8')

            # print 'HTTP OK', api_str
            return api_str
        else:
            print 'HTTP ', self.r.status_code
            return None

    def common_names(self, api_response):
        assert isinstance(api_response, dict)

        list_of_names = []
        if 'taxa' in api_response:
            for t in api_response['taxa']:
                if 'nombres_comunes_todos' in t:
                    # print 't["nombres_comunes_todos"]', t['nombres_comunes_todos']
                    try:
                        nombres_comunes_todos = ast.literal_eval(t['nombres_comunes_todos'])
                    # ValueError because malformed string ignored
                    except:
                        continue
                    assert isinstance(nombres_comunes_todos, dict) 
                    # print  'nombres_comunes_todos', nombres_comunes_todos
                    for key, value in nombres_comunes_todos.items():
                        list_of_names.extend(value)

        set_of_names = list(set([cname.decode('UTF-8') for cname in list_of_names]))

        # print '##set_of_names#\n'
        # for n in set_of_names:
        #     print n
        return set_of_names


class AliasFinder(object):
    """AliasFinder: look for common name - scientific name relations."""
    def __init__(self,
                 arg1_label='Nombre_comun',
                 arg2_label='Nombre_cientifico',
                 rel_label='alias',
                 taxon_indexer='Taxonfinder',
                 sciname_type='all'):
        """
        Create an  instance of AliasFinder.

        :param arg1_lab: the label for arg1, by default: 'Nombre_comun'

        :param arg2_lab: the label for arg2, by default: 'Nombre_cientifico'

        :param rel_lab: the label of relation between arg1 and arg2,
                        by default 'alias'

        :param taxon_indexer: the type of taxon indexer to use. Any of 
                              'Taxonfinder', 'Netineti', 'Spaneti', 'CNN'

        :param taxon_indexer: the type of scientific names to include. Any of 
                              'all', 'monomial', 'binomial', 'long'

        About taxon indexers see:
                https://www.researchgate.net/publication/290190594_A_Text_Mining_Library_for_Biodiversity_Literature_in_Spanish
        """
        self.a1_label = arg1_label
        self.a2_label = arg2_label
        self.rel_lab = rel_label
        if taxon_indexer.lower() is 'netineti':
            pass
        elif taxon_indexer.lower() is 'spaneti':
            pass
        elif taxon_indexer.lower() is 'cnn':
            pass
        else:
            # default
            self.taxon_indexer = Taxonfinder()

        if sciname_type not in ['all', 'monomial', 'binomial', 'long']:
            self.sciname_type = 'all'
        # keep common names cache
        self.know_names = dict()
        return

    def get_rels_unicode(self, unicode_string):
        # create auxiliar file then call with former method
        f = tempfile.NamedTemporaryFile(delete=False)
        filename = f.name
        f.close()

        with codecs.open(filename, 'w+b', encoding='utf-8') as fh:
            fh.write(unicode_string)
            fh.seek(0)
            annotation = self.get_rels(filename)

        os.unlink(filename)

        return annotation

    def get_rels(self, txt_file):
        """
        Create a dictionary with common names - scientific names relations.

        :param txt_file: the file to process.

        :return: :class:`dict` Entities ('T') and relations ('R') found, e.g.:
                         ...
                         'T3': {'texts': [u'Cornus excelsa'],
                                'label': 'Nombre_cientifico',
                                'type': 'ENT',
                                'offsets': [(125, 139)]}
                        'T18': {'texts': [u'mimbre'],
                                'label': 'Nombre_comun',
                                'type': 'ENT',
                                'offsets': [(110, 116)]}
                         'R1': {'Arg1': 'T18',
                                'Arg2': 'T3',
                                'type': 'REL',
                                'label': 'alias'}
                         ...

        About Brat see:
                http://2011.bionlp-st.org/home/file-formats

        :rtype: dict
        """
        (contents, lines) = self.__readfile(txt_file)

        # get scientific names index
        scienames_index = self.taxon_indexer.get_index(txt_file)
        entities = BratConv.ann_format(txt_file,
                                       scienames_index,
                                       label=self.a2_label)
        # initialization
        num_entities = len(entities) + 1
        relcount = 0

        for (l, taxon, canonical, offset) in scienames_index:
            text = lines[l-1]
            if taxon in self.know_names:
                common_names = self.know_names[taxon]
            else:
                # Ask for common names via the API
                bioapi = MexBioAPI(taxon)
                api_str = bioapi.get()
                # print type(api_str), api_str
                if api_str is None:
                    continue
                common_names = bioapi.common_names(api_str)
                common_names = list(set([n.lower() for n in common_names]))

            if not common_names:
                continue
            # include in self.know_names
            self.know_names[taxon] = common_names

            # look for the name in the text of line
            names_found = self.__find_cnames(common_names, text)
            if names_found:
                ss = offset[0]
                se = offset[1]
                m = self.__nearest(names_found, (ss, se))
                cs = m.start('cnames')
                ce = m.end('cnames')
                common_name = text[cs:ce]
                # get global offsets of taxon and its common name
                (t_start, t_end) = global_offsets(l, contents, ss, se)
                (c_start, c_end) = global_offsets(l, contents, cs, ce)

                # look if common name entity (arg1) in dict
                arg1 = self.id_from_offsets(entities, c_start, c_end)
                # check not overlaping existing entities
                overlaping = self.overlaping_ents(entities, c_start, c_end)
                if not arg1 and not overlaping:
                    # add new ents and rels into dictionary
                    num_entities += 1
                    # add entity
                    arg1 = self.add_ent(entities,
                                        num_entities,
                                        c_start,
                                        c_end,
                                        self.a1_label,
                                        text[cs:ce])
                # arg2 should be in dict
                arg2 = self.id_from_offsets(entities, t_start, t_end)
                if arg1 and arg2:
                    # add relation
                    relcount += 1
                    self.add_rel(entities, relcount, arg1, arg2, self.rel_lab)
        return entities

    def __readfile(self, txt_file):
        try:
            contents = codecs.open(txt_file, 'r', 'UTF-8').read()
            lines = contents.splitlines(True)
        except Exception, e:
            raise e
        return (contents, lines)

    def __find_cnames(self, common_names, text):
        pattern = r'(?P<cnames>'+'|'.join(common_names)+')+'
        names_found = [x for x in re.finditer(pattern, text, re.IGNORECASE)]
        return names_found

    def __nearest(self, names_found, (ss, se)):
        scienamepos = (ss + se) / 2.0
        midpoints = [((m.start('cnames') + m.end('cnames')) / 2.0)
                     for m in names_found]
        dist = [math.fabs(scienamepos - mid) for mid in midpoints]
        i = dist.index(min(dist))
        m = names_found[i]
        return m

    def id_from_offsets(self, ents, start, end):
        for k, v in ents.items():
            if not k.startswith('T'):
                continue
            ent_start = v['offsets'][0][0]
            ent_end = v['offsets'][0][1]
            if ent_start == start and ent_end == end:
                return k
        return None

    def overlaping_ents(self, ents, start, end):
        for k, v in ents.items():
            if not k.startswith('T'):
                continue
            ent_start = v['offsets'][0][0]
            ent_end = v['offsets'][0][1]
            if (ent_start <= start <= ent_end) or (ent_start <= end <= ent_end):
                return True
        return False

    def add_ent(self, entities, i, start, end, lab, texts):
        entid = 'T{0}'.format(i)
        entities.setdefault(entid,
                            {'offsets': [(start, end)],
                             'type': 'ENT',
                             'label': lab,
                             'texts': [texts]})
        return entid

    def add_rel(self, entities, i, arg1, arg2, lab):
        relid = 'R{0}'.format(i)
        entities.setdefault('R{0}'.format(i),
                            {'Arg1': arg1,
                             'Arg2': arg2,
                             'type': 'REL',
                             'label': lab})
        return relid


# Utilities


def similarity(s1, s2):
    # return 2.0*M / T
    # A measure of the sequences similarity as a float in range [0, 1].
    # Where T is the total number of elements in both sequences,
    # and M is the number of matches,
    # this is 2.0*M / T.
    # Note that this is 1.0 if the sequences are identical,
    # and 0.0 if they have nothing in common.

    # first remove spaces and punctuation for fair comparisson
    a = format_2cmp(s1.lower())
    b = format_2cmp(s2.lower())
    return smatcher(None, a, b).ratio()


def format_2cmp(s):
    formated_s = ''.join(l for l in s if l not in string.punctuation)
    return formated_s.replace(' ', '')

