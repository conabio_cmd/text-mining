# -*- coding: utf-8 -*-
'''
Created on Apr 22, 2015

@author: Daniel
'''
import os
import unittest

from txtmining.document.pdf_info import PDFInfo
from txtmining.document.tika import Tika
from txtmining.tests.util import get_pdf_folder


class Test(unittest.TestCase):

    def setUp(self):
        self.__pdf_dir = get_pdf_folder()
        pdf_info_bin_path = os.getenv("PDF_INFO_BIN", None)
        tika_bin_path = os.getenv("TIKA_BIN", None)
        self.assertIsNotNone(tika_bin_path,
                             "You must set TIKA_BIN env var")
        self.assertIsNotNone(pdf_info_bin_path,
                             "You must set PDF_INFO_BIN env var")
        self.__pdf_info = PDFInfo(pdf_info_bin_path)
        self.__tika = Tika(tika_bin_path,
                           None)

    def test_pdf_info(self):
        pdf_file = os.path.join(self.__pdf_dir,
                                'Test_PDF.pdf')
        metadata = self.__pdf_info.get_metadata(pdf_file)
        self.assertIsInstance(metadata, dict,
                              'PDFInfo failed')
        self.assertEqual(metadata[u'Author'],
                         u'Ecoinformatica',
                         'PDFInfo failed: Author info')
        self.assertEqual(metadata[u'Title'],
                         u'Test PDF',
                         'PDFInfo failed: Title info')

    def test_tika(self):
        pdf_file = os.path.join(self.__pdf_dir,
                                'Test_PDF.pdf')
        metadata = self.__tika.extract_meta_data(pdf_file)

        self.assertIsInstance(metadata, dict,
                              'Tika failed')

        self.assertEqual(metadata[u'Author'],
                         u'Ecoinformatica',
                         'Tika failed: Author info')

        self.assertEqual(metadata[u'title'],
                         u'Test PDF',
                         'Tika failed: Title info')


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
