a) Comestibles


Plantas registradas como comestibles de mayor mención fueron: el laurel (Litsea glaucescens) (29 menciones), la anona (Annona cherimola) (29); los chepiches (Porophyllum tagetoides) (29); la nanche roja (Malphigia mexicana) (29); los chepiles (Crotalaria pumila) (27) y el orégano criollo (Lippia graveolens) (27) (Figura 39).


b) Medicinales


Las plantas con uso medicinal fueron categorizadas en 17 categorías de acuerdo a la Clasificación Internacional de las Enfermedades de la Organización Mundial para la Salud (OMS) adaptadas para este trabajo (Cuadro 21).


Las encuestas realizadas en la población indican que las plantas medicinales silvestres son usadas principalmente para curar o aliviar enfermedades del sistema digestivo (16 spp.), las enfermedades o padecimientos de definición popular (aire, barridas, mal de ojo amarrar el cuerpo, purificaciones, evitan que los niños se orinen, evita el alcoholismo y tristeza) (13 spp.), las que alivian el dolor (10), las enfermedades de la nutrición y el metabolismo (7) y aquellas que ayudan en complicaciones de embarazo, del parto y del puerperio (7) (Cuadro21).


Las plantas medicinales de uso más frecuente son el pirul (Schinus molle) (30 menciones), árbol del que se utilizan sus ramas y hojas en padecimientos de definición popular (aire y barridas), este se puede encontrar en la zona baja del municipio, el pericón (Tagetes lucida) (30 menciones), hierba usada principalmente para malestares estomacales, crece en las zonas perturbadas cercanas a los encinares, el espule (Pinaropappus roseus) (29 menciones), ayuda a aliviar enfermedades de la piel especialmente en los niños, crece durante la época de lluvias en las laderas de pastoreo de la zona baja y el Botón-chiguite, de la familia solanaceae con (28 menciones), que ayuda en casos de fiebre, diarrea, salpullido, viruela y sarampión, se encuentra en la zona baja del municipio, aunque no es frecuente encontrarlo (Figura 40).


Las especies más apreciadas que curan más de una enfermedad de acuerdo con las entrevistas son la salvia de castilla (Salvia sp.) (5) la bretónica (Salvia urica) (4), el chamizo de río (Baccharis salicifolia) (4), la cuanashana (Calea ternifolia) (4) La hierba del aire (asteraceae) (4) y el pirul (Schinus molle) (4) (Figura 41).


c) Construcción


De las especies empleadas para la construcción, los que mayor número de menciones tienen son los pinos (Pinus spp.) con (18 menciones cada una), los encinos cucharo (Quercus crassifolia) (11); y hoja delgada (Quercus laurina) (10); de ellos se obtienen horcones, tijeras, morillos, tablas y tejamanil para la construcción de casas y techumbres, todos estos árboles se encuentran en las partes altas del municipio en terrenos con distintos grados de perturbación provocada principalmente por las actividades forestales (Figura 42).