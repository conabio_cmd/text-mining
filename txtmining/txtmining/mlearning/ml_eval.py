# -*- coding: utf-8 -*-
"""
Created on August 4, 2015.

@author: Alejandro Molina.
"""

import abc
import string
import copy
import warnings
import unicodecsv
from collections import Counter
from difflib import SequenceMatcher as smatcher
import codecs
from txtmining.document.nlp import NLPpreprocessor
from txtmining.base.util import DEFAULT_ENCODING


class AbstractEvaluator(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, goldstd_annotation_csv):
        # headers:[num,sciname,comname,abbrev,location,useful_properties,text]
        self.rel_sciname = []
        try:
            with open(goldstd_annotation_csv) as f:
                csvreader = unicodecsv.reader(f, encoding=DEFAULT_ENCODING)
                headers = csvreader.next()

                for row in csvreader:

                    (num, sciname, comname,
                     abbrev, location,
                     useful_properties, text) = self.__correct_types(row)

                    if sciname:
                        self.rel_sciname.append((num, sciname))

                    # self.rel_comname = [(row['num'], row['comname'])
                    #                     for row in csvreader if row['comname']]

                    # self.rel_abbrev = [(row['num'], row['abbrev'])
                    #                    for row in csvreader if row['abbrev']]

                    # self.rel_location = [(row['num'], row['location'])
                    #                      for row in csvreader if row['location']]

                    # self.rel_use = [(row['num'], row['useful_properties'])
                    #                 for row in csvreader if row['useful_properties']]

                    # self.rel_text = [(row['num'], row['text'])
                    #                  for row in csvreader if row['text']]

        except Exception as e:
            raise e

    @abc.abstractmethod
    def eval(self, *args, **kwargs):
        pass

    def precision(self, retrieved, retrieved_and_relevants):
        intersection_len = float(len(retrieved_and_relevants))
        retrieved_len = len(retrieved)
        try:
            precision = intersection_len/retrieved_len
        except ZeroDivisionError as e:
            print 'ZeroDivisionError error in precision assessment.'
            raise e
        return precision

    def recall(self, relevants, retrieved_and_relevants):
        intersection_len = float(len(retrieved_and_relevants))
        relevants_len = len(relevants)
        try:
            recall = intersection_len/relevants_len
        except ZeroDivisionError as e:
            print 'ZeroDivisionError error in recall assessment.'
            raise e
        return recall

    def fmeasure_beta(self, P, R, beta):
        try:
            return (1 + beta**2)*(P*R/((beta**2 * P) + R))
        except ZeroDivisionError as e:
            print 'ZeroDivisionError error in fmeasure assessment.'
            raise e

    def __correct_types(self, r):
        # avoid (u'60', u'Quercus')
        return (int(r[0]), r[1], r[2], r[3], r[4], bool(r[5]), r[6])


class ScinamesEvaluator(AbstractEvaluator):
    """ScinamesEvaluator evaluation for scientific names of species."""

    def __init__(self, goldstd_annotation_csv):
        AbstractEvaluator.__init__(self, goldstd_annotation_csv)
        self.p = None
        self.r = None
        self.f = None

    def eval(self, retrieved, abbrev=True, sciname_type='all', beta=1, simcutoff=0.8):
        # avoid ZeroDivisionError because there are no names matching
        # this particular parameters combination
        if sciname_type == 'long' and not abbrev:
            warnings.warn('''No names matching this particular parameter combination in evaluation file.
Therefore, precision and recall could not be assessed: switching argument abbrev to True.''')
            abbrev = True

        # relevant names criteria depending on sciname_type arg
        if sciname_type == 'monomial':
            relevants = self.get_monomial(self.rel_sciname, 2)
            retrieved_pairs = self.get_monomial(retrieved, 3)

        elif sciname_type == 'binomial':
            relevants = self.get_binomial(self.rel_sciname, 2)
            retrieved_pairs = self.get_binomial(retrieved, 3)

        elif sciname_type == 'long':
            relevants = [n for n in self.rel_sciname if len(n[1].split(' ')) > 2]
            retrieved_pairs = [ (line, name) for (line, name, offset) in retrieved
                                if len(n[1].split(' ')) > 2]
        else:
            relevants = self.rel_sciname
            retrieved_pairs = [ (line, name) for (line, name, offset) in retrieved]

        # remove relevant names depending on abbrev arg
        if not abbrev:
            relevants = [n for n in relevants if '.' not in n[1]]
            retrieved_pairs = [ n for n in retrieved_pairs if '.' not in n[1]]

        (retrieved_and_relevants, complement) = self.similarity_intersection(retrieved_pairs,
                                                               relevants,
                                                               simcutoff)
        # useful debug
        self.__printlog(retrieved_pairs, relevants, retrieved_and_relevants, complement)

        self.p = self.precision(retrieved_pairs, retrieved_and_relevants)
        self.r = self.recall(relevants, retrieved_and_relevants)
        self.f = self.fmeasure_beta(self.p, self.r, beta)

        return (self.p, self.r, self.f)

    def similarity_intersection(self, retrieved, relevants, simcutoff):
        '''Intersection between retrieved and relevants
           considering not unique items'''        
        intersection = []
        difference = []

        retrieved_counter = Counter(retrieved)
        relevants_counter = Counter(relevants)
        # print 'retrieved_counter=', retrieved_counter
        # print 'relevants_counter=', relevants_counter

        for retrieved, freq_ret in retrieved_counter.iteritems():

            relevant = self.get_similar(retrieved, relevants_counter, simcutoff)

            if relevant:
                (j, name_in_relevants) = relevant
                freq_rel = relevants_counter[relevant]
                delta = freq_rel - freq_ret
                # if delta != 0:
                #     print 'ret', retrieved, freq_ret
                #     print 'rel', relevant, freq_rel
                #     print delta
                if delta > 0:
                    intersection.extend([retrieved for k in range(freq_ret)])
                    #difference.extend([retrieved for k in range(delta)])
                    # print 'intersection+=', len(range(freq_ret))
                    # print 'difference+=', delta
                elif delta == 0:
                    intersection.extend([retrieved for k in range(freq_ret)])
                elif delta < 0:
                    # print 'intersection+=', len(range(freq_rel))
                    # print 'difference+=', delta
                    intersection.extend([retrieved for k in range(freq_rel)])
                    difference.extend([retrieved for k in range(abs(delta))])
            else:
                difference.extend([retrieved for k in range(freq_ret)])

        return(sorted(intersection, key=lambda x: int(x[0])),
               sorted(difference, key=lambda x: int(x[0])))

    def get_similar(self, taxon_pair, taxon_counter, simcutoff):
        ret_val = None
        (l, n) = taxon_pair

        for (line, name), f in taxon_counter.iteritems():
            r = smatcher(None, format_2cmp(name), format_2cmp(n)).ratio()
            if (line == l and r > simcutoff):
                ret_val = (line, name)
                break
        return ret_val

    # TODO: merge get_monomial and get_binomial into one sinle method
    def get_monomial(self, tuples, tuple_cardinality):
        """
        Get the monomial version of names from a list of tuples.
        """
        if tuple_cardinality == 3:
            return [(line, ' '.join(name.split(' ')[:1]))
                    for (line, name, offset) in tuples
                    if len(name.split(' ')) >= 1]

        elif tuple_cardinality == 2:
            return [(line, ' '.join(name.split(' ')[:1]))
                    for (line, name) in tuples
                    if len(name.split(' ')) >= 1]

    def get_binomial(self, tuples, tuple_cardinality):
        """
        Get the binomial version of names from a list of tuples.
        """
        if tuple_cardinality == 3:
            return [(line, ' '.join(name.split(' ')[:2]))
                    for (line, name, offset) in tuples
                    if len(name.split(' ')) >= 2]

        elif tuple_cardinality == 2:
            return [(line, ' '.join(name.split(' ')[:2]))
                    for (line, name) in tuples
                    if len(name.split(' ')) >= 2]

    def __printlog(self, retrieved, relevants, intersctnames, complement):

        format_list('relevants', relevants)
        format_list('retrieved', retrieved)
        format_list('intersection retrieved and relevants', intersctnames)
        # complement = [e for e in retrieved if e not in intersctnames]
        # complement = set(retrieved) - set(intersctnames)
        format_list('complement: retrieved not in intersction retrieved and relevants', complement)
        print '{}\t{}\t{}'.format(len(relevants),
                                  len(retrieved),
                                  len(intersctnames))
    pass


class TaxonPair(object):
    def __init__(self, line=None, name=None, simcutoff=0.8):
        self.line = line
        self.name = name
        self.simcutoff = simcutoff

    def __eq__(self, other):
        same_line = self.line == other.line
        # print 'similarity('+self.name.encode('utf-8')+', '+other.name.encode('utf-8')+')=', similarity(self.name, other.name)
        same_name = similarity(self.name, other.name) > self.simcutoff
        return same_line and same_name

    def __hash__(self):
        return hash((self.line, self.name))


# Utilities


def similarity(s1, s2):
    # return 2.0*M / T
    # A measure of the sequences similarity as a float in range [0, 1].
    # Where T is the total number of elements in both sequences,
    # and M is the number of matches,
    # this is 2.0*M / T.
    # Note that this is 1.0 if the sequences are identical,
    # and 0.0 if they have nothing in common.

    # first remove spaces and punctuation for fair comparisson
    a = format_2cmp(s1)
    b = format_2cmp(s1)
    return smatcher(None, a, b).ratio()


def found_similar(e, some_l, simcutoff):
    (l1, s1) = e
    return any(similarity(s1, s2) > simcutoff for (l2, s2) in some_l)


def format_list(listname, l):
    print '\n'+listname+':\n'
    print '['
    for x in l:
        print '({}, \'{}\'),'.format(x[0], x[1].encode(DEFAULT_ENCODING))
    print ']'


def format_2cmp(s):
    formated_s = ''.join(l for l in s if l not in string.punctuation)
    return formated_s.replace(' ', '')


def duplicates(self, l):
    return [x for x in l if l.count(x) > 1]


def index2classes(text_file, index):
    # return a list of integers.
    # Each number represents the class of each word (token) in text_file
    # Note that text must be tokenized first and that index must be
    # formated like this:
    # [(22, u'Manilkara zapota', (90, 106)),...
    #  (line number, unicode, (offset_start, offset_end)), ...

    symbol = 'TAG'
    clases = []

    NLPpre = NLPpreprocessor('spanish')

    with codecs.open(text_file, 'r', encoding='UTF-8') as f:
        lines = f.readlines()

    (l, taxon, (start, end)) = index.pop(0)
    for (lnum, ltext) in enumerate(lines):
        ltoks = NLPpre.tokenize(ltext)
        # no tokens line
        if not ltoks:
            continue

        # No names in lines until lnum
        if lnum < l-1:
            zeros = [0] * len(ltoks)
            clases.extend(zeros)

        # replace name with symbol for each found name
        # then merge all lists of tokens
        d = []
        while l-1 == lnum:
            # print 'lnum:', lnum, 'TAXON:', taxon
            # nwords = len(ltext[start:end].split(' '))
            taxon_tokens = NLPpre.tokenize(taxon)
            nwords = len(taxon_tokens)
            s = ' '.join([symbol] * nwords)
            tagged_text = ltext[:start]+s+ltext[end:]
            tagged_toks = NLPpre.tokenize(tagged_text)
            # print tagged_text
            # print tagged_toks
            d.append(tagged_toks)

            # next name to look for
            if index:
                (l, taxon, (start, end)) = index.pop(0)
            else:
                break

        # merge TAGS in single list
        if d:
            nlists = len(d)
            ntoks = len(tagged_toks)
            taggclass = []
            for i in range(ntoks):
                val = 1 if any(d[j][i] == symbol for j in range(nlists)) else 0
                taggclass.append(val)

            # print len(taggclass), taggclass
            clases.extend(taggclass)

    return clases


def tagged_from_classes(fpath, classes, labels):
    # labels = ['WORD', 'SCIENAME']
    with codecs.open(fpath, 'r', encoding='UTF-8') as f:
        lines = f.readlines()
        text = ''.join(lines)

    NLPpre = NLPpreprocessor('spanish')
    toks = NLPpre.tokenize(text)
    len_classes = len(classes)
    if not (len(toks) == len_classes):
        raise ValueError('number of classes and tokens are not equal.')

    s = u''
    for i in range(len_classes):
        s += toks[i]+' '+labels[classes[i]]+'\n'
    return s
