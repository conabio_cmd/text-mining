# -*- coding: utf-8 -*-
"""
Created by Alejandro Molina.
Nov. 2015
"""

import os
import nltk
import codecs
import simplejson
from txtmining.document.nlp import NLPpreprocessor
from txtmining.document.taxonfinder import Taxonfinder


# defalut resources paths

current_path = os.path.realpath(__file__)
current_path = os.path.dirname(current_path)
parent_path = os.path.dirname(current_path)

# alias_corpus = os.path.join(parent_path, 'resources/train/untag_scienames_paragraphs.txt')
# out_alias = os.path.join(parent_path, 'resources/features/untag_alias.JSON')
alias_corpus = os.path.join(parent_path, 'resources/train/scienames_paragraphs.txt')
out_alias = os.path.join(parent_path, 'resources/features/alias.JSON')
default_alias_features = os.path.join(parent_path, 'resources/features/alias.JSON')
default_cname_features = 'resources/features/feat_cname.JSON'
# loc_feat = '/Users/amolina/geo/loc_features.JSON'

alias_featsize = 3000

bag_of_words_params = {'remove_punctuation': False,
                       'remove_numbers': True,
                       'remove_stopwords': True,
                       'use_stemmer': False,
                       'stoplist': None}


def casecode(word):
    lc_diacritics = [ord(u) for u in u'áéíóúüñ']
    uc_diacritics = [ord(u) for u in u'ÁÉÍÓÚÜÑ']
    code = ''
    for char in word:
        o = ord(char)
        if o in range(65, 91) or o in uc_diacritics:
            code += 'X'
        elif o in range(97, 123) or o in lc_diacritics:
            code += 'x'
        else:
            code += '.'
    return code


def cname_bow(text, features_file):
    features = {}

    NLPpre = NLPpreprocessor('spanish')
    tok_text = NLPpre.tokenize(text)
    lower_toks = [w.lower() for w in tok_text]

    bow_read = read_bow(features_file) if features_file else read_bow(default_cname_features)
    for word in bow_read:
        features['contains({})'.format(word.encode('UTF-8'))] = (word in lower_toks)
    return features


def taxon_distance(text, (start, end)):
    taxonfinder = Taxonfinder()
    tagged_text = taxonfinder.tag_scienames(text)
    # print tagged_text
    # tokens counting
    NLPpre = NLPpreprocessor('spanish')
    toks = NLPpre.tokenize(tagged_text)

    tok_i = toks.index(text[start:end])
    # split list to count distances
    back_toks = toks[tok_i-1::-1]
    forward_toks = toks[tok_i+1::]

    try:
        dback = back_toks.index('SCIENAME')+1
    except ValueError:
        # return number-comparable value if not in list
        dback = float('inf')
    try:
        dforward = forward_toks.index('SCIENAME')+1
    except ValueError:
        # return number-comparable value if not in list
        dforward = float('inf')

    return (dback, dforward)


def common_name_features(text, (start, end)):
    feats = cname_bow(text, features_file=default_cname_features)
    feats['casecode'] = casecode(text[start:end])
    (feats['back_taxon_distance'], feats['forward_taxon_distance']) = taxon_distance(text, (start, end))
    return feats


def alias_features(txt, features_file):
    features = {}
    NLPpre = NLPpreprocessor('spanish')
    sent_bow = NLPpre.get_bagofwords(txt, bag_of_words_params)
    stems = set(sent_bow)
    bow_read = read_bow(bow_file=features_file) if features_file else read_bow(bow_file=default_alias_features)
    for stem in bow_read:
        #if not stem.isalpha() or len(stem) < 2:
        features['contains({})'.format(stem.encode('UTF-8'))] = (stem in stems)
    return features


def loc_features(txt):
    features = {}
    NLPpre = NLPpreprocessor('spanish')
    sent_bow = NLPpre.get_bagofwords(txt,
                                     remove_punctuation=True,
                                     remove_numbers=True,
                                     remove_stopwords=True,
                                     use_stemmer=True,
                                     stoplist=None)
    stems = set(sent_bow)
    bow_read = read_bow(bow_file=loc_feat)
    for stem in bow_read:
        features['has({})'.format(stem.encode('UTF-8'))] = (stem in stems)
    return features


def alias_features_taxon(txt, features_file):
    features = {}
    NLPpre = NLPpreprocessor('spanish')
    taxonfinder = Taxonfinder()
    tagged_text = taxonfinder.tag_scienames(txt)
    sent_bow = NLPpre.get_bagofwords(tagged_text, bag_of_words_params)

    stems = set(sent_bow)
    bow_read = read_bow(bow_file=features_file)
    for stem in bow_read:
        # if not stem.isalpha() or len(stem) < 2:
        features['contains({})'.format(stem.encode('UTF-8'))] = (stem in stems)
    return features


def set_bow(corpus=alias_corpus,
            dim_size=alias_featsize,
            stoplist=None,
            out_file=out_alias):
    # taxonfinder = Taxonfinder()
    # get rawcontents
    try:
        text = codecs.open(corpus, encoding='UTF-8').read()
        # utext = text.decode('UTF-8')
        # tagged_text = taxonfinder.tag_scienames(utext)
    except Exception, e:
        raise e

    # bag of words
    NLPpre = NLPpreprocessor('spanish')

    corpus_bow = NLPpre.get_bagofwords(text, bag_of_words_params)

    # first n=dim_size most frequent words from left by bow
    words_by_freq = nltk.FreqDist(w.lower() for w in corpus_bow.split())
    domain_features = []
    for word, frequency in words_by_freq.most_common(dim_size):
        # print('%s\t%d' % (word, frequency)).encode('utf-8')
        domain_features.append(word.encode('utf-8'))

    # return domain_features
    try:
        with open(out_file, 'w') as f:
            simplejson.dump(domain_features, f, encoding='UTF-8')
        return out_file
    except Exception, e:
        raise e


def read_bow(bow_file=out_alias):
    try:
        with open(bow_file, 'rb') as f:
            j = simplejson.load(f)
        return j
    except Exception, e:
        raise e


def label_examples(filename, label):
    return_list = []
    try:
        with open(filename, 'r') as f:
            for line in f.readlines():
                unicode_line = line.decode('UTF-8')
                return_list.append((unicode_line, label))
    except Exception, e:
        print e
    finally:
        return return_list
