'''
Created on Apr 10, 2015

@author: Daniel
'''

import os
import sys

from txtmining import commands
from txtmining.controllers import controllers


if __name__ == '__main__':

    usage = """
 Usage: txtm [subcommand] || [configuration_file]

 Available subcommands:
    """

    usage += "\n    ".join(commands.get_modes())

    if len(sys.argv) < 2:
        print(usage)
        sys.exit(1)

    cmd = sys.argv[1]

    if(os.path.isfile(cmd)):

        commands.run_pipeline(cmd)

    else:

        try:
            main = getattr(commands, cmd)
            is_function = True
        except AttributeError:
            is_function = False
        try:
            class_name = getattr(controllers, cmd)
            is_controller = True
        except AttributeError:
            is_controller = False

        if not is_controller and not is_function:
            print(usage)
            sys.exit(1)
        elif is_function:
            main(sys.argv[2:])
        elif is_controller:
            commands.generic_controller(cmd, class_name, sys.argv[2:])

    pass
