# -*- coding: utf-8 -*-

"""
Created on May 2016.
@author: Alejandro Molina.

Script to evaluate the Named Entity Recognition (NER)
methods included in the repository.
"""

from txtmining.mlearning.ner_eval import NEReval

human_text_file = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/evaluation/h.txt'
human_ann_file = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/evaluation/h.ann'

# machine annotation
# Taxon Finder
tf_text_file = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/evaluation/tf.txt'
tf_ann_file = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/evaluation/tf.ann'

# Neural Network
nn_text_file = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/evaluation/nn.txt'
nn_ann_file = '/Users/amolina/text-mining/text-mining/txtmining/txtmining/resources/evaluation/nn.ann'


print "Human"
h_eval = NEReval(human_text_file, human_ann_file, human_ann_file)
(confu_matrix, class_report) = h_eval.word_classification(["WORD", "Nombre_cientifico"])
print confu_matrix
print class_report

print "Taxon finder"
t_eval = NEReval(human_text_file, human_ann_file, tf_ann_file)
(confu_matrix, class_report) = t_eval.word_classification(["WORD", "Nombre_cientifico"])
print confu_matrix
print class_report

print "Neural Network"
n_eval = NEReval(human_text_file, human_ann_file, nn_ann_file)
(confu_matrix, class_report) = n_eval.word_classification(["WORD", "Nombre_cientifico"])
print confu_matrix
print class_report

# Neural Network + rule
rn_eval = NEReval(human_text_file, human_ann_file, nn_ann_file, heuristic_rule=True)
(confu_matrix, class_report) = rn_eval.word_classification(["WORD", "Nombre_cientifico"])
print "Neural Network + rule"
print confu_matrix
print class_report
