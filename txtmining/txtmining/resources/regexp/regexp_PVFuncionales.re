((?P<verbo_usar>
\bhubiésemos usado\b|\b
hubiéremos usado\b|\b
hubiéramos usado\b|\b
habríamos usado\b|\b
hubisteis usado\b|\b
hubieseis usado\b|\b
hubiereis usado\b|\b
hubierais usado\b|\b
habíamos usado\b|\b
habríais usado\b|\b
hubieses usado\b|\b
hubiesen usado\b|\b
hubieron usado\b|\b
hubieres usado\b|\b
hubieren usado\b|\b
hubieras usado\b|\b
hubieran usado\b|\b
habíais usado\b|\b
habrías usado\b|\b
habrían usado\b|\b
habréis usado\b|\b
habremos usado\b|\b
habiendo usado\b|\b
hubiste usado\b|\b
hubimos usado\b|\b
hubiese usado\b|\b
hubiere usado\b|\b
hubiera usado\b|\b
hayáis usado\b|\b
hayamos usado\b|\b
habías usado\b|\b
habían usado\b|\b
habéis usado\b|\b
habría usado\b|\b
habrás usado\b|\b
habrán usado\b|\b
había usado\b|\b
habré usado\b|\b
habrá usado\b|\b
hemos usado\b|\b
hayas usado\b|\b
hayan usado\b|\b
haber usado\b|\b
usaríamos\b|\b
usadamente\b|\b
hubo usado\b|\b
hube usado\b|\b
haya usado\b|\b
usásemos\b|\b
usáremos\b|\b
usáramos\b|\b
usábamos\b|\b
usaríais\b|\b
has usado\b|\b
han usado\b|\b
usasteis\b|\b
usarías\b|\b
usarían\b|\b
usaréis\b|\b
usaremos\b|\b
usación\b|\b
he usado\b|\b
ha usado\b|\b
usaseis\b|\b
usaría\b|\b
usarás\b|\b
usarán\b|\b
usareis\b|\b
usarais\b|\b
usabais\b|\b
uséis\b|\b
usáis\b|\b
usemos\b|\b
usaste\b|\b
usases\b|\b
usasen\b|\b
usaré\b|\b
usará\b|\b
usarse\b|\b
usaron\b|\b
usares\b|\b
usaren\b|\b
usaras\b|\b
usaran\b|\b
usanza\b|\b
usante\b|\b
usando\b|\b
usamos\b|\b
usador\b|\b
usable\b|\b
usabas\b|\b
usaban\b|\b
se usa\b|\b
usase\b|\b
usare\b|\b
usara\b|\b
usaje\b|\b
usad.\b|\b
usaba\b|\b
usó\b|\b
usé\b|\b
uses\b|\b
usen\b|\b
usas\b|\b
usar\b|\b
usan\b|\b
usad\b|\b
uso\b|\b
use\b|\b
usa\b)|
(?P<verbo_utilizar>
\bhubiésemos utilizado\b|\b
hubiéremos utilizado\b|\b
hubiéramos utilizado\b|\b
habríamos utilizado\b|\b
hubisteis utilizado\b|\b
hubieseis utilizado\b|\b
hubiereis utilizado\b|\b
hubierais utilizado\b|\b
habíamos utilizado\b|\b
habríais utilizado\b|\b
hubieses utilizado\b|\b
hubiesen utilizado\b|\b
hubieron utilizado\b|\b
hubieres utilizado\b|\b
hubieren utilizado\b|\b
hubieras utilizado\b|\b
hubieran utilizado\b|\b
habíais utilizado\b|\b
habrías utilizado\b|\b
habrían utilizado\b|\b
habréis utilizado\b|\b
habremos utilizado\b|\b
habiendo utilizado\b|\b
hubiste utilizado\b|\b
hubimos utilizado\b|\b
hubiese utilizado\b|\b
hubiere utilizado\b|\b
hubiera utilizado\b|\b
hayáis utilizado\b|\b
hayamos utilizado\b|\b
habías utilizado\b|\b
habían utilizado\b|\b
habéis utilizado\b|\b
habría utilizado\b|\b
habrás utilizado\b|\b
habrán utilizado\b|\b
había utilizado\b|\b
habré utilizado\b|\b
habrá utilizado\b|\b
hemos utilizado\b|\b
hayas utilizado\b|\b
hayan utilizado\b|\b
haber utilizado\b|\b
utilizaríamos\b|\b
hubo utilizado\b|\b
hube utilizado\b|\b
haya utilizado\b|\b
utilizásemos\b|\b
utilizáremos\b|\b
utilizáramos\b|\b
utilizábamos\b|\b
utilizaríais\b|\b
has utilizado\b|\b
han utilizado\b|\b
utilizasteis\b|\b
utilizarías\b|\b
utilizarían\b|\b
utilizaréis\b|\b
utilizaremos\b|\b
he utilizado\b|\b
ha utilizado\b|\b
utilizaseis\b|\b
utilizaría\b|\b
utilizarás\b|\b
utilizarán\b|\b
utilizareis\b|\b
utilizarais\b|\b
utilizabais\b|\b
utilizáis\b|\b
utilizaste\b|\b
utilizases\b|\b
utilizasen\b|\b
utilizaré\b|\b
utilizará\b|\b
utilizarse\b|\b
utilizaron\b|\b
utilizares\b|\b
utilizaren\b|\b
utilizaras\b|\b
utilizaran\b|\b
utilizando\b|\b
utilizamos\b|\b
utilizable\b|\b
utilizabas\b|\b
utilizaban\b|\b
utilicéis\b|\b
utilicemos\b|\b
se utiliza\b|\b
utilizase\b|\b
utilizare\b|\b
utilizara\b|\b
utilizad.\b|\b
utilizaba\b|\b
utilizó\b|\b
utilizas\b|\b
utilizar\b|\b
utilizan\b|\b
utilizad\b|\b
utilicé\b|\b
utilices\b|\b
utilicen\b|\b
utilizo\b|\b
utiliza\b|\b
utilice\b)|
(?P<verbo_servir>
\bhubiésemos servido\b|\b
hubiéremos servido\b|\b
hubiéramos servido\b|\b
habríamos servido\b|\b
hubisteis servido\b|\b
hubieseis servido\b|\b
hubiereis servido\b|\b
hubierais servido\b|\b
habíamos servido\b|\b
habríais servido\b|\b
hubieses servido\b|\b
hubiesen servido\b|\b
hubieron servido\b|\b
hubieres servido\b|\b
hubieren servido\b|\b
hubieras servido\b|\b
hubieran servido\b|\b
habíais servido\b|\b
habrías servido\b|\b
habrían servido\b|\b
habréis servido\b|\b
habremos servido\b|\b
habiendo servido\b|\b
hubiste servido\b|\b
hubimos servido\b|\b
hubiese servido\b|\b
hubiere servido\b|\b
hubiera servido\b|\b
hayáis servido\b|\b
hayamos servido\b|\b
habías servido\b|\b
habían servido\b|\b
habéis servido\b|\b
habría servido\b|\b
habrás servido\b|\b
habrán servido\b|\b
había servido\b|\b
habré servido\b|\b
habrá servido\b|\b
hemos servido\b|\b
hayas servido\b|\b
hayan servido\b|\b
haber servido\b|\b
sirviésemos\b|\b
sirviéremos\b|\b
sirviéramos\b|\b
serviríamos\b|\b
hubo servido\b|\b
hube servido\b|\b
haya servido\b|\b
serviríais\b|\b
has servido\b|\b
han servido\b|\b
utilicemos\b|\b
sirvieseis\b|\b
sirviereis\b|\b
sirvierais\b|\b
servíamos\b|\b
servisteis\b|\b
servirías\b|\b
servirían\b|\b
serviréis\b|\b
serviremos\b|\b
he servido\b|\b
ha servido\b|\b
sirvieses\b|\b
sirviesen\b|\b
sirvieron\b|\b
sirvieres\b|\b
sirvieren\b|\b
sirvieras\b|\b
sirvieran\b|\b
sirviendo\b|\b
servíais\b|\b
serviría\b|\b
servirás\b|\b
servirán\b|\b
sirváis\b|\b
sirviese\b|\b
sirviere\b|\b
sirviera\b|\b
sirvamos\b|\b
servías\b|\b
servían\b|\b
serviste\b|\b
serviré\b|\b
servirá\b|\b
servirse\b|\b
servimos\b|\b
servible\b|\b
se sirve\b|\b
sirvió\b|\b
sirvise\b|\b
servís\b|\b
servía\b|\b
servid.\b|\b
sirves\b|\b
sirven\b|\b
sirvas\b|\b
sirvan\b|\b
serví\b|\b
servir\b|\b
servid\b|\b
sirvo\b|\b
sirve\b|\b
sirva\b)|
(?P<verbo_ocupar>
\bhubiésemos ocupado\b|\b
hubiéremos ocupado\b|\b
hubiéramos ocupado\b|\b
habríamos ocupado\b|\b
hubisteis ocupado\b|\b
hubieseis ocupado\b|\b
hubiereis ocupado\b|\b
hubierais ocupado\b|\b
habíamos ocupado\b|\b
habríais ocupado\b|\b
hubieses ocupado\b|\b
hubiesen ocupado\b|\b
hubieron ocupado\b|\b
hubieres ocupado\b|\b
hubieren ocupado\b|\b
hubieras ocupado\b|\b
hubieran ocupado\b|\b
habíais ocupado\b|\b
habrías ocupado\b|\b
habrían ocupado\b|\b
habréis ocupado\b|\b
habremos ocupado\b|\b
habiendo ocupado\b|\b
hubiste ocupado\b|\b
hubimos ocupado\b|\b
hubiese ocupado\b|\b
hubiere ocupado\b|\b
hubiera ocupado\b|\b
hayáis ocupado\b|\b
hayamos ocupado\b|\b
habías ocupado\b|\b
habían ocupado\b|\b
habéis ocupado\b|\b
habría ocupado\b|\b
habrás ocupado\b|\b
habrán ocupado\b|\b
había ocupado\b|\b
habré ocupado\b|\b
habrá ocupado\b|\b
hemos ocupado\b|\b
hayas ocupado\b|\b
hayan ocupado\b|\b
haber ocupado\b|\b
ocuparíamos\b|\b
hubo ocupado\b|\b
hube ocupado\b|\b
haya ocupado\b|\b
ocupásemos\b|\b
ocupáremos\b|\b
ocupáramos\b|\b
ocupábamos\b|\b
ocuparíais\b|\b
has ocupado\b|\b
han ocupado\b|\b
ocupasteis\b|\b
ocuparías\b|\b
ocuparían\b|\b
ocuparéis\b|\b
ocuparemos\b|\b
he ocupado\b|\b
ha ocupado\b|\b
ocupaseis\b|\b
ocuparía\b|\b
ocuparás\b|\b
ocuparán\b|\b
ocupareis\b|\b
ocuparais\b|\b
ocupabais\b|\b
se ocupa\b|\b
ocupéis\b|\b
ocupáis\b|\b
ocupemos\b|\b
ocupaste\b|\b
ocupases\b|\b
ocupasen\b|\b
ocuparé\b|\b
ocupará\b|\b
ocuparse\b|\b
ocuparon\b|\b
ocupares\b|\b
ocuparen\b|\b
ocuparas\b|\b
ocuparan\b|\b
ocupando\b|\b
ocupamos\b|\b
ocupabas\b|\b
ocupaban\b|\b
ocupase\b|\b
ocupare\b|\b
ocupara\b|\b
ocupad.\b|\b
ocupaba\b|\b
ocupó\b|\b
ocupé\b|\b
ocupes\b|\b
ocupen\b|\b
ocupas\b|\b
ocupar\b|\b
ocupan\b|\b
ocupad\b|\b
ocupo\b|\b
ocupe\b|\b
ocupa\b)|
(?P<verbo_emplear>
\bhubiésemos empleado\b|\b
hubiéremos empleado\b|\b
hubiéramos empleado\b|\b
habríamos empleado\b|\b
hubisteis empleado\b|\b
hubieseis empleado\b|\b
hubiereis empleado\b|\b
hubierais empleado\b|\b
habíamos empleado\b|\b
habríais empleado\b|\b
hubieses empleado\b|\b
hubiesen empleado\b|\b
hubieron empleado\b|\b
hubieres empleado\b|\b
hubieren empleado\b|\b
hubieras empleado\b|\b
hubieran empleado\b|\b
habíais empleado\b|\b
habrías empleado\b|\b
habrían empleado\b|\b
habréis empleado\b|\b
habremos empleado\b|\b
habiendo empleado\b|\b
hubiste empleado\b|\b
hubimos empleado\b|\b
hubiese empleado\b|\b
hubiere empleado\b|\b
hubiera empleado\b|\b
hayáis empleado\b|\b
hayamos empleado\b|\b
habías empleado\b|\b
habían empleado\b|\b
habéis empleado\b|\b
habría empleado\b|\b
habrás empleado\b|\b
habrán empleado\b|\b
había empleado\b|\b
habré empleado\b|\b
habrá empleado\b|\b
hemos empleado\b|\b
hayas empleado\b|\b
hayan empleado\b|\b
haber empleado\b|\b
hubo empleado\b|\b
hube empleado\b|\b
haya empleado\b|\b
emplearíamos\b|\b
has empleado\b|\b
han empleado\b|\b
empleásemos\b|\b
empleáremos\b|\b
empleáramos\b|\b
empleábamos\b|\b
emplearíais\b|\b
he empleado\b|\b
ha empleado\b|\b
empleasteis\b|\b
emplearías\b|\b
emplearían\b|\b
emplearéis\b|\b
emplearemos\b|\b
empleaseis\b|\b
emplearía\b|\b
emplearás\b|\b
emplearán\b|\b
empleareis\b|\b
emplearais\b|\b
empleabais\b|\b
se emplea\b|\b
empleéis\b|\b
empleáis\b|\b
empleemos\b|\b
empleaste\b|\b
empleases\b|\b
empleasen\b|\b
emplearé\b|\b
empleará\b|\b
emplearse\b|\b
emplearon\b|\b
empleares\b|\b
emplearen\b|\b
emplearas\b|\b
emplearan\b|\b
empleando\b|\b
empleamos\b|\b
empleabas\b|\b
empleaban\b|\b
emplease\b|\b
empleare\b|\b
empleara\b|\b
emplead.\b|\b
empleaba\b|\b
empleó\b|\b
empleé\b|\b
emplees\b|\b
empleen\b|\b
empleas\b|\b
emplear\b|\b
emplean\b|\b
emplead\b|\b
empleo\b|\b
emplee\b|\b
emplea\b)|
(?P<verbo_aprovechar>
\bhubiésemos aprovechado\b|\b
hubiéremos aprovechado\b|\b
hubiéramos aprovechado\b|\b
habríamos aprovechado\b|\b
hubisteis aprovechado\b|\b
hubieseis aprovechado\b|\b
hubiereis aprovechado\b|\b
hubierais aprovechado\b|\b
habíamos aprovechado\b|\b
habríais aprovechado\b|\b
hubieses aprovechado\b|\b
hubiesen aprovechado\b|\b
hubieron aprovechado\b|\b
hubieres aprovechado\b|\b
hubieren aprovechado\b|\b
hubieras aprovechado\b|\b
hubieran aprovechado\b|\b
habíais aprovechado\b|\b
habrías aprovechado\b|\b
habrían aprovechado\b|\b
habréis aprovechado\b|\b
habremos aprovechado\b|\b
habiendo aprovechado\b|\b
hubiste aprovechado\b|\b
hubimos aprovechado\b|\b
hubiese aprovechado\b|\b
hubiere aprovechado\b|\b
hubiera aprovechado\b|\b
hayáis aprovechado\b|\b
hayamos aprovechado\b|\b
habías aprovechado\b|\b
habían aprovechado\b|\b
habéis aprovechado\b|\b
habría aprovechado\b|\b
habrás aprovechado\b|\b
habrán aprovechado\b|\b
había aprovechado\b|\b
habré aprovechado\b|\b
habrá aprovechado\b|\b
hemos aprovechado\b|\b
hayas aprovechado\b|\b
hayan aprovechado\b|\b
haber aprovechado\b|\b
hubo aprovechado\b|\b
hube aprovechado\b|\b
haya aprovechado\b|\b
aprovecharíamos\b|\b
has aprovechado\b|\b
han aprovechado\b|\b
aprovechásemos\b|\b
aprovecháremos\b|\b
aprovecháramos\b|\b
aprovechábamos\b|\b
aprovecharíais\b|\b
aprovechamiento\b|\b
he aprovechado\b|\b
ha aprovechado\b|\b
aprovechasteis\b|\b
aprovecharías\b|\b
aprovecharían\b|\b
aprovecharéis\b|\b
aprovecharemos\b|\b
aprovechaseis\b|\b
aprovecharía\b|\b
aprovecharás\b|\b
aprovecharán\b|\b
aprovechareis\b|\b
aprovecharais\b|\b
aprovechabais\b|\b
se aprovecha\b|\b
aprovechéis\b|\b
aprovecháis\b|\b
aprovechemos\b|\b
aprovechaste\b|\b
aprovechases\b|\b
aprovechasen\b|\b
aprovecharé\b|\b
aprovechará\b|\b
aprovecharse\b|\b
aprovecharon\b|\b
aprovechares\b|\b
aprovecharen\b|\b
aprovecharas\b|\b
aprovecharan\b|\b
aprovechando\b|\b
aprovechamos\b|\b
aprovechabas\b|\b
aprovechaban\b|\b
aprovechase\b|\b
aprovechare\b|\b
aprovechara\b|\b
aprovechad.\b|\b
aprovechaba\b|\b
aprovechó\b|\b
aproveché\b|\b
aproveches\b|\b
aprovechen\b|\b
aprovechas\b|\b
aprovechar\b|\b
aprovechan\b|\b
aprovechad\b|\b
aprovecho\b|\b
aproveche\b|\b
aprovecha\b))