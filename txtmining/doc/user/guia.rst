Extracción de metadatos - MetadataController
============================================


En ocasiones los archivos pdf contienen información acerca del documento como el título, el autor, las palabras clave, la fecha de creación o los permisos.
Aunque estos atributos no forman parte del contenido textual, pueden ser extraídos de igual manera si se considera de interés.

El comando ``MetadataController`` sirve para extraer los metadatos de un archivo en formato pdf. La extracción se realiza por medio de Apache Tika [apache]_ y Poppler pdfinfo [poppler]_.

Sinopsis:

.. code-block:: console

   $ python txtm.py MetadataController [-h]  --tika_bin /ruta/tika --pdfinfo_bin /ruta/pdfinfo --infile /ruta/archivo/f.pdf ---metadata_type tipo

Opciones:

.. program:: txtm.py MetadataController

.. option:: -h

    Despliega el mensaje de ayuda.

.. option:: --tika_bin <path_file>

   Especifica la ruta hacia el ejecutable de tika. Si se configuro la variable
   :envvar:`TIKA_BIN` esta puede ser usada.

.. option:: --pdfinfo_bin <path_file>

   Especifica la ruta hacia el ejecutable de pdfinfo. Si se configuro la variable
   :envvar:`PDF_INFO_BIN` esta puede ser usada.

.. option:: --infile <path_file_pdf>

   Especifica la ruta hacia el archivo pdf.

.. option:: --parser_type [tika,pdfinfo,all]

   Indica el programa qué herramienta debe utilizarse para extraer los metadatos. Las opciones posibles son: ``tika``, ``pdfinfo`` y ``all`` (default).

Se muestra a continuación un ejemplo considerando que las variables de entorno están bien configuradas y que el archivo de entrada es /tmp/f.pdf.

.. code-block:: console

   $ python txtm.py MetadataController --tika_bin  $TIKA_BIN --pdfinfo_bin $PDF_INFO_BIN --infile /tmp/f.pdf --metadata_type all

La salida del comando despliega un diccionario con los metadatos extraídos para cada uno de los parsers:

.. code-block:: json

    {"pdfinfo_meta": {"Producer": "pdfTeX-1.40.15", "Author": "A. Molina", "Encrypted": "no", "Creator": "LaTeX with hyperref package", "File size": "227871 bytes", "Title": "Manual", "PDF version": "1.5", "ModDate": "Fri Jul  3 10:46:28 2015", "Keywords": "text mining, biodiversity", "Page size": "612 x 792 pts (letter)", "CreationDate": "Fri Jul  3 10:46:28 2015", "Pages": "7"}, "tika_meta": {"access_permission:can_modify": "true", "Content-Length": "227871", "producer": "pdfTeX-1.40.15", "Last-Modified": "2015-07-03T15:46:28Z", "xmp:CreatorTool": "LaTeX with hyperref package", "dcterms:created": "2015-07-03T15:46:28Z", "pdf:PDFVersion": "1.5", "pdf:encrypted": "false", "Content-Type": "application/pdf"}}



Conversión de pdf a texto plano y OCR - PDFparserController
===============================================================

Algunos archivos en formato pdf no contienen la capa de texto asociada para que pueda ser leída por los programas, por lo que es necesario aplicar reconocimiento óptico de caracteres (OCR) para obtener texto plano.
También puede ocurrir que los archivos presentan la capa de texto pero no tienen los permisos necesarios para extraerla de manera automática.
Por esto, la biblioteca usa tres herramientas distintas para la obtención del texto plano: Apache PDFBox [apache]_, Apache Tika [apache]_ y Poppler pdftotext [poppler]_.

El comando ``PDFparserController`` sirve para extraer el texto de un archivo en formato pdf y crear un archivo de texto plano.

Sinopsis:

.. code-block:: console

   $ python txtm.py PDFparserController [-h] --out_dir /directorio/salida --tika_bin /ruta/tika --pdfbox_bin /ruta/pdfbox --pdftotext_bin /ruta/pdftotext --infile /ruta/archivo/f.pdf --parser_type tipo

Opciones:

.. program:: txtm.py PDFparserController

.. option:: -h

    Despliega el mensaje de ayuda.

.. option:: --out_dir <path_file>

   Especifica el directorio de salida de los archivos de texto generados.

.. option:: --tika_bin <path_file>

   Especifica la ruta hacia el ejecutable de tika. Si se configuro la variable
   :envvar:`TIKA_BIN` esta puede ser usada.

.. option:: --pdfbox_bin <path_file>

   Especifica la ruta hacia el ejecutable de pdfbox. Si se configuro la variable
   :envvar:`PDFBOX_BIN` esta puede ser usada.

.. option:: --pdftotext_bin <path_file>

   Especifica la ruta hacia el ejecutable de pdftotext. Si se configuro la variable
   :envvar:`PDF_TO_TEXT_BIN` esta puede ser usada.

.. option:: --infile <path_file_pdf>

   Especifica la ruta hacia el archivo pdf.

.. option:: --parser_type [tika,pdfbox,pdftotext,all]

   Indica el programa que debe utilizarse para extraer el texto del archivo pdf. Las opciones posibles son: ``tika``, ``pdfbox``, ``pdftotext``, ``all`` (default).

Se muestra a continuación un ejemplo considerando que las variables de entorno están bien configuradas, que el archivo de entrada es ``/tmp/f.pdf`` y que el directorio de salida es ``/tmp/out``.

.. code-block:: console

   $ python txtm.py PDFparserController --out_dir /tmp/out --tika_bin $TIKA_BIN --pdfbox_bin $PDFBOX_BIN --pdftotext_bin $PDF_TO_TEXT_BIN --infile /tmp/f.pdf --parser_type all

La salida del comando indica la ruta del archivo de texto generado para cada uno de los parsers:

.. code-block:: json

    {"pdftotext_out": "/tmp/out/pdftotext/tmp-f.pdf.txt",
     "pdfbox_out":    "/tmp/out/pdfbox/tmp-f.pdf.txt",
     "tika_out":      "/tmp/out/tika/tmp-f.pdf.txt"}

El :ref:`ejemplo-1` muestra un fragmento del texto de salida extraído a partir de un archivo pdf.
Las líneas han sido numeradas para hacer evidentes los saltos de línea.

.. code-block:: text
   :caption: Ejemplo 1
   :name: ejemplo-1
   :linenos:

   Estudio de la vegetacion del municipio de San Pablo Etla, Oaxaca.

   Se analizó la vegetación del municipio de San Pablo Etla, Oaxaca, México desde
   el punto de vista ecológico y etnobotánico.
   La Calera se encuentra en Oaxaca a una altitud de 2900 msnm. y su lade-
   ra esta orientada hacia el suroeste, sus pendientes son pronunciadas,
   y el suelo se encuentra cubierto de una capa superficial de hojas de pino
   (Pinus pseudostrobus) y encino (Quercus magnolifolia).
   Del Pino se extraen tablas y horcones, es maderable, la resina se usa en ca-
   taplasmas, la madera como leña, se saca ocote y carbón, y sus hojas como fes-
   tones y ramos de flores, es el principal producto de venta.
   Estudios previos realizados por Padilla-Gómez (2005) indican que es además un
   sitio importante como receptor y paradero de especies migratorias de aves, así
   como un sitio importante de alimentación y refugio de especies residentes per-
   manentes y temporales encontrándose algunas dentro de la norma oficial mexicana
   059-2001, además en un muestreo de murciélagos realizado en esta zona
   se encontraron 5 especies de murciélagos (Artibeus intermedius, Sturnira ludo-
   vici, Sturnira lilium, Leptonycteris curasoae y Myotis californica) una de ellas
   con categoría de amenazada en la norma y varias de reptiles entre las que destaca
   el primer registro para la cordillera de tortuga de río (Kinosternum integrum)
   así como de la víbora de casabel (Crotalus basiliscus) que de confirmarse
   sería el primer registro para Oaxaca de esta especie.


Corrección de errores de OCR y detección de fronteras de las oraciones - PreprocessingController
================================================================================================

Una vez extraído el texto en ocasiones es necesario corregirlo.
Note que en ocasiones las palabras del ejemplo 1 fueron separadas silábicamente.
Esto se debe a que el flujo textual obtenido a partir de un PDF se encuentra alineado con la vista de impresión y esto no resulta conveniente para el procesamiento automático en minería de textos.

El comando ``PreprocessingController`` sirve para realizar correcciones sucesivas al texto extraído del archivo pdf hasta llegar a tener un texto plano segmentado por oraciones.
La segmentación de oraciones se realiza mediante un modelo de aprendizaje supervisado obtenido con ayuda de Apache OpenNLP [apache]_ el cual fue entrenado con 5M de ejemplos del dominio de biodiversidad del `corpus técnico del IULA
<http://www.iula.upf.edu/corpus/corpuses.htm>`_.

Sinopsis:

.. code-block:: console

   $ python txtm.py PreprocessingController [-h] --opennlp_bin /ruta/opennlp --opennlp_mod /ruta/modelo --infile /ruta/archivo/entrada --correction_type tipo

Opciones:

.. program:: txtm.py PreprocessingController

.. option:: -h

   Despliega el mensaje de ayuda.

.. option:: --opennlp <path_file>

   Especifica la ruta hacia el ejecutable de opennlp. Si se configuro la variable
   :envvar:`OPENNLP_BIN` esta puede ser usada.

.. option:: --opennlp_mod <path_file>

   Especifica la ruta hacia el archivo modelo de segmentación. Si se configuro la variable
   :envvar:`OPENNLP_MODEL` esta puede ser usada.

.. option:: --infile <path_file_txt>

   Especifica la ruta hacia el archivo txt.

.. option:: --correction_type [dash,abrv,onel,sent,all]

   Indica el tipo de corrección que se va a realizar. Las opciones posibles son: ``dash``, ``abrv``, ``onel``, ``sent``, ``all`` (default). La opción ``dash`` corrige los errores producidos cuando las palabras están separadas silábicamente; La opción ``abrv`` corrige los saltos de línea después de una abreviatura; ``onel`` concatena todo el texto en una sola línea; ``sent`` aplica el modelo de segmentación oracional. Se recomienda utilizar la opción ``all`` que realiza la corrección completa hasta la segmentación oracional a menos que se esté desarrollando un corrector independiente.

Se muestra a continuación un ejemplo de ``PreprocessingController`` considerando que las variables de entorno están bien configuradas, que el archivo de entrada es ``/tmp/t.txt``.

.. code-block:: console

   $ python txtm.py PreprocessingController  --opennlp_bin $OPENNLP_BIN --opennlp_mod $OPENNLP_MODEL --infile /tmp/t.txt --correction_type all

El :ref:`ejemplo-2` muestra la salida de ``PreprocessingController``.
Note que hay una oración por cada línea y que las palabras previamente separadas con guiones, del ejemplo 1, fueron reagrupadas correctamente.
También note que la detección de fronteras de la oración distingue el punto de una abreviatura del punto de final de oración en la línea 3.

.. code-block:: text
   :caption: Ejemplo 2
   :name: ejemplo-2
   :linenos:

   Estudio de la vegetacion del municipio de San Pablo Etla, Oaxaca.
   Se analizó la vegetación del municipio de San Pablo Etla, Oaxaca, México desde el punto de vista ecológico y etnobotánico.
   La Calera se encuentra en Oaxaca a una altitud de 2900 msnm. y su ladera esta orientada hacia el suroeste, sus pendientes son pronunciadas, y el suelo se encuentra cubierto de una capa superficial de hojas de pino (Pinus pseudostrobus) y encino (Quercus magnolifolia).
   Del Pino se extraen tablas y horcones, es maderable, la resina se usa en cataplasmas, la madera como leña, se saca ocote y carbón, y sus hojas como festones y ramos de flores, es el principal producto de venta.
   Estudios previos realizados por Padilla-Gómez (2005) indican que es además un sitio importante como receptor y paradero de especies migratorias de aves, así como un sitio importante de alimentación y refugio de especies residentes permanentes y temporales encontrándose algunas dentro de la norma oficial mexicana 059-2001, además en un muestreo de murciélagos realizado en esta zona se encontraron 5 especies de murciélagos (Artibeus intermedius, Sturnira ludovici, Sturnira lilium, Leptonycteris curasoae y Myotis californica) una de ellas con categoría de amenazada en la norma y varias de reptiles entre las que destaca el primer registro para la cordillera de tortuga de río (Kinosternum integrum) así como de la víbora de casabel (Crotalus basiliscus) que de confirmarse sería el primer registro para Oaxaca de esta especie.

Detección de nombres de especies a partir de una lista - ReIndexController
==========================================================================

La detección de nombres con ``ReIndexController`` es útil cuando ya se cuenta una lista de especies.
La distribución incluye el archivo ``regexp_NombreCientifico.re`` con la información de los nombres científicos de cerca de cuatro mil árboles endémicos de México.
Éste contiene una expresión regular con dos secciones: ``genera`` y ``species`` en las cuales se deben enlistar los nombres de los genera y los de las especies de interés.
La parte final de cada sección sirve para considerar abreviaturas como *sp.* y *spp*.

    ::

        (?P<scientificNames>
            (?P<genera>\bAbarema\b|\bAbies\b|...|([A-Z]{1}\.)){1}\s+
            (?P<species>\bidiopoda\b|\bmexicana\b|...|((s|S){1,1}pp{0,1}\.{0,1})\b){1,1}
        )

Sinopsis:

.. code-block:: console

   $ python  txtm.py ReIndexController [-h] --regexp_file /ruta/archivo/expresion/regular --infile /ruta/archivo

Opciones:

.. program:: txtm.py ReIndexController

.. option:: -h

   Despliega el mensaje de ayuda.

.. option:: --regexp_file <path_file>

   Especifica la ruta hacia el archivo que enlista los nombres científicos.

.. option:: --infile <path_file>

   Especifica la ruta hacia el archivo de texto del cual se quieren extraer los nombres de especies de la lista especificada por ``--regexp_file``.

Se muestra a continuación un ejemplo de ``ReIndexController`` utilizando el archivo con nombres de árboles, suponiendo que la variable de entorno :envvar:`REGEX_DIR` está bien configurada y que el archivo de entrada es ``/tmp/t.txt`` con el texto del ejemplo 2.

.. code-block:: console

   $ python txtm.py ReIndexController --regexp_file $REGEX_DIR/regexp_NombreCientifico.re --infile /tmp/f.txt

La salida con estos parámetros es una lista con los nombres encontrados a partir del archivo indicado por  ``--regexp_file``.

El formato para cada elemento de la lista es ``[NUMERO DE LÍNEA, NOMBRE DEL TAXÓN, [POSICIÓN INICIAL, POSICIÓN FINAL]]``.
Donde ``POSICIÓN INICIAL`` y ``POSICIÓN FINAL`` son relativas a la línea donde se encontró el nombre.

    ::

        [[3, "Pinus pseudostrobus", [215, 234]], ...]

.. _globalnames-r-d:

Detección de nombres de especies mediante servicios web especializados (Global Names) - GnIndexController
=========================================================================================================


|gnrd|_ es un servicio web para reconocer nombres científicos basado en TaxonFinder [taxfinder]_ y NetiNeti [neti]_.
Dado que se trata de un servicio Web su uso intensivo con archivos grandes puede ocasionar tiempos largos de espera.
Para subsanar esto, la biblioteca incluye un comando para llamar a |gnrd|_ que primero divide el texto en lotes, de manera que cada lote envía una petición al servicio Web.
La ventaja principal de esta estrategia es que si una petición falla, solamente una parte se perdería mientras que el resto podría recuperarse.

En esta versión, se incluye también la posibilidad de incluir :ref:`un administrador de tareas asíncronas <async-exec>`.

Sinopsis:

.. code-block:: console

   $ python txtm.py GnIndexController [-h] --infile /ruta/archivo

Opciones:

.. program:: txtm.py GnIndexController

.. option:: -h

   Despliega el mensaje de ayuda.

.. option:: --infile <path_file>

   Especifica la ruta hacia el archivo de texto del cual se quieren extraer los nombres de especies con Global Names.

Se muestra a continuación un ejemplo de ``GnIndexController`` suponiendo que el archivo de entrada es ``/tmp/t.txt`` con el texto del :ref:`ejemplo-2`.

.. code-block:: console

   $ python txtm.py GnIndexController --infile /tmp/f.txt


La salida con estos parámetros es una lista con la ubicación de los nombres.
El formato para cada elemento de la lista es ``[NUMERO DE LÍNEA, NOMBRE, [POSICIÓN INICIAL, POSICIÓN FINAL]]``.
Donde ``POSICIÓN INICIAL`` y ``POSICIÓN FINAL`` son relativas a la línea donde se encontró el nombre.

.. code-block:: json

        [[3, "Calera", [3, 9]],
        [3, "Pinus pseudostrobus", [215, 234]],
        [3, "Quercus", [246, 253]],
        [5, "Artibeus intermedius", [426, 446]],
        [5, "Crotalus basiliscus", [737, 756]],
        [5, "Kinosternum integrum", [681, 701]],
        [5, "Leptonycteris curasoae", [484, 506]],
        [5, "Myotis californica", [509, 527]],
        [5, "Sturnira lilium", [467, 482]],
        [5, "Sturnira ludovici", [448, 465]]]

Note que a diferencia de ``ReIndexController`` el comando encontró los nombres de otras especies además de los árboles.
Sin embargo, la respuesta del servicio Web puede variar de una llamada a otra o tener comportamiento inestable por lo que se recomienda guardar los resultados como se indica en la sección de uso de un archivo de configuración.

Extracción de información de usos tradicionales de especies
===========================================================

Con la ayuda de patrones sintácticos, es posible detectar fragmentos textuales que potencialmente hablan del uso de las especies.
La idea general consiste en identificar los fragmentos que mencionan el nombre de alguna especie y una predicación verbal de tipo funcional [laweb]_.
En esta versión, se incluye un extractor que identifica fragmentos textuales que hablan de usos tradicionales de especies mediante patrones sintácticos.

Sinopsis:

.. code-block:: console

   $ python txtm.py [-h] --regex_dir /ruta/regex/DIR/ --infile /ruta/archivo --nlines_before i --nlines_after j

Opciones:

.. program:: txtm.py

.. option:: -h

   Despliega el mensaje de ayuda.

.. option:: --regex_dir <path>

   Especifica la ruta hacia el directorio con los archivos de expresiones regulares. Si se configuro la variable
   :envvar:`REGEX_DIR` esta puede ser usada.

.. option:: --infile <path_file>

   Especifica la ruta hacia el archivo de texto que se quiere analizar.

.. option:: --nlines_before <n>

   Indica cuántas líneas de contexto, previas al match, deben incluirse en la estructura de datos de salida.

.. option:: --nlines_after <n>

   Indica cuántas líneas de contexto, posteriores al match, deben incluirse en la estructura de datos de salida.

Se muestra a continuación un ejemplo de ``FragmentController`` suponiendo que el archivo de entrada es ``/tmp/t.txt`` con el texto del ejemplo 2 y que queremos incluir ``1`` línea de contexto antes y ``1`` línea después.

.. code-block:: console

   $ python txtm.py FragmentController  --regex_dir $REGEX_DIR --infile /tmp/t.txt --nlines_before 1 --nlines_after 1

La salida muestra *Pino* como parte de los nombres comunes y la predicación verbal *se usa* así como varias menciones de objetos de uso tradicional entre las que se encuentran *leña* y *carbón* (combustible).
Note que además de los nombres comunes, los nombres científicos y las predicaciones verbales funcionales; existen otros elementos de interés, tales como las partes de la planta, nombres de objetos de uso doméstico, de instrumentos de caza y pesca, nombres de padecimiento en comunidades indígenas, nombres de prácticas terapéuticas, entre otros.
Todos estos elementos forman parte del lexicón resultante de la curación manual de proyectos anteriores desarrollados en CONABIO.

.. literalinclude:: ../code/fragment.json
   :language: json

Si bien, el uso de patrones sintácticos resulta un método muy intuitivo e incluso ventajoso en cobertura pues captura mucha diversidad en los resultados; éste presenta algunos inconvenientes.
El primero de ellos es que no cubre todos los casos: existen fragmentos que hablan del uso pero que no presentan predicación verbal funcional y existen otros tantos que presentan los patrones sintácticos pero que no hablan del uso de plantas (falsos positivos).
En versiones futuras, la extracción de este tipo de fragmentos se hará mediante aprendizaje de máquina pero estas estructuras pueden ser el punto de partida de la anotación manual.

.. _variables-de-entorno:

Variables de entorno
====================

Se pueden definir las siguientes variables de entorno que son comunes para
algunas de las tareas de extracción.

.. envvar:: TIKA_BIN

   Especifica la ruta hacia el ejecutable de tika.

.. envvar:: PDFBOX_BIN

   Especifica la ruta hacia el ejecutable de pdfbox

.. envvar:: PDF_INFO_BIN

   Especifica la ruta hacia el ejecutable de pdfinfo

.. envvar:: PDF_TO_TEXT_BIN

   Especifica la ruta hacia el ejecutable de pdftotext.

.. envvar:: OPENNLP_BIN

   Especifica la ruta hacia el ejecutable de opennlp.

.. envvar:: OPENNLP_MODEL

   Especifica la ruta hacia el archivo modelo de segmentación

.. envvar:: REGEX_DIR

   Especifica la ruta hacia el directorio con los archivos de expresiones regulares.


Uso de un archivo de configuración
==================================

Cuando se van a procesar varios archivos se recomienda el uso de un archivo de configuración.
La biblioteca incluye un ejemplo (``example.conf``) que puede utilizarse como punto de inicio.

El archivo de configuración está dividido en secciones.
Cada nombre de sección se distingue por un encabezado encerrado entre corchetes, e.g. ``[PDFparserController]``.

Las líneas que comienzan por ``#`` son comentarios ignorados por los programas.

En la primera sección ``[DEFAULT]`` hemos declarado algunos parámetros comunes a varias de las etapas.

.. code-block:: cfg

   [DEFAULT]
   # input directory
   IN_DIR : ./txtmining/tests/data/pdf
   OUT_DIR : ./txtmining/tests/data/fromOCR
   n_jobs : 1
   ...

Indicamos el directorio desde el cual deben leerse los archivos pdf, e.g. ``./txtmining/tests/data/pdf``.

Y el directorio al cual deben enviarse los archivos de texto una vez que se extraiga el contenido, e.g. ``./txtmining/tests/data/fromOCR``.

El parámetro ``n_jobs`` permite que las tareas se realicen en paralelo, aprovechando al máximo los procesadores, e.g. si la computadora en la que se va a ejecutar cuenta con *n* núcleos, una buena opción es ``n_jobs :`` *n*-1

También, se indica la ruta de los recursos tika, pdfinfo, pdfbox y pdftotext.


Las siguientes dos secciones ``[MetadataController]`` y ``[PDFparserController]`` configuran la extracción de metadatos y texto plano respectivamente.

.. code-block:: cfg

   [MetadataController]
   APPLY: True
   STEP: 0
   METADATA_TYPE: all
   INPUT_EXTENSION : pdf
   OUTPUT_EXTENSION : meta


   [PDFparserController]
   APPLY: True
   STEP: 0
   PARSER_TYPE: all
   INPUT_EXTENSION : pdf
   OUTPUT_EXTENSION : extractor


La opción ``APPLY`` indica si se debe realizar el comando o no, e.g. ``APPLY: False`` en la sección ``[MetadataController]`` causaría que no se realice la extracción de metadatos para ningún documento en el directorio indicado por ``IN_DIR``.

La opción ``STEP`` indica en qué etapa se debe realizar el comando, e.g. ``STEP: 0`` en la sección ``[PDFparserController]`` causaría que se realice la extracción de texto plano para cada documento en el directorio indicado por ``IN_DIR`` y que se realice esta operación antes de realizar cualquier otro proceso.

La opción ``INPUT_EXTENSION`` permite indicar el tipo de archivos que esperamos tener a la entrada del comando, e.g. ``INPUT_EXTENSION : pdf`` considera todos los archivos dentro de ``OUT_DIR`` cuya extensión es pdf.
De manera análoga, ``OUTPUT_EXTENSION : extractor`` indica que los archivos de salida deben contener el sufijo ``extractor``.

La sección ``[PreprocessingController]`` configura la corrección de OCR y la segmentación del texto por oraciones.

.. code-block:: cfg

   [PreprocessingController]
   APPLY: True
   STEP: 1
   OPENNLP_BIN: ../apache-opennlp-1.5.3/bin/opennlp
   OPENNLP_MOD: ./txtmining/resources/models/es-iula.bin
   IN_DIR : ./txtmining/tests/data/fromOCR
   OUT_DIR : ./txtmining/tests/data/segmented


La opción ``STEP: 1`` en la sección ``[PreprocessingController]`` indica que se debe aplicar este proceso a los archivos resultantes de la etapa anterior.

También se indica la ruta del ejecutable de opennlp y del modelo de segmentación que debe utilizarse.

Las secciones ``[ReIndexController]`` y ``[GnIndexController]`` controlan los parámetros para extraer nombres de especies.
El primero a partir de un archivo de expresión regular y el segundo a partir de un servicio Web.

.. code-block:: cfg

   [ReIndexController]
   APPLY: True
   STEP: 2
   REGEXP_FILE: ./txtmining/resources/regexp/regexp_NombreCientifico.re
   OUTPUT_EXTENSION : re.json
   IN_DIR : ./txtmining/tests/data/segmented


   [GnIndexController]
   APPLY: True
   STEP: 2
   OUTPUT_EXTENSION = gn.json
   IN_DIR : ./txtmining/tests/data/segmented



La opción ``REGEXP_FILE`` de la sección ``[ReIndexController]`` determina la ruta del archivo que se usará en este proceso.

Note que es posible usar ``STEP: 2`` en ambos procesos, lo que causará que se busquen nombres científicos mediante ambos métodos.


La sección ``[FragmentController]`` configura los parámetros para extraer información de usos tradicionales.

.. code-block:: cfg

   [FragmentController]
   REGEX_DIR: ./txtmining/resources/regexp/
   NLINES_BEFORE : 4
   NLINES_AFTER : 2
   OUTPUT_EXTENSION : fr.json
   IN_DIR : ./txtmining/tests/data/segmented
   OUT_DIR : ./txtmining/tests/data/fragments


Los parámetros ``NLINES_BEFORE`` y ``NLINES_AFTER`` indican las líneas de contexto que deben considerarse tomando como referencia la línea que contiene la información de uso.
