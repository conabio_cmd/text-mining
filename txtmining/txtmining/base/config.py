'''
Created on Jun 15, 2015

@author: Daniel
'''
import ConfigParser

from txtmining.controllers.controllers import get_controllers_name


class Config(object):
    '''
    classdocs
    '''

    def __init__(self, config_file):
        '''
        Constructor
        '''
        self.sections = get_controllers_name()

        self.config = ConfigParser.SafeConfigParser()
        with open(config_file, 'r') as cf:
            self.config.readfp(cf)

        self.parameters = dict()
        self.__create_dictionary()

        self.n_jobs = self.config.defaults()['n_jobs']
        self.n_jobs = int(self.n_jobs)

    def __create_dictionary(self):

        for section in self.sections:
            if self.config.getboolean(section, 'APPLY'):
                options = dict()
                config_options = self.config.options(section)
                apply_index = config_options.index('apply')
                config_options.pop(apply_index)
                for option in config_options:
                    options.setdefault(option,
                                       self.config.get(section, option))

                self.parameters.setdefault(section,
                                           options)
