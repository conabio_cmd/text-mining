"""
Created on May 2016.
@author: Alejandro Molina.

Script to create Named Entity Recognition (NER) data from a directory.
"""

import os
import sys
import getopt

from txtmining.controllers.controllers import AliasController


def main(argv):
    (indir, outdir) = args_ok(argv)

    controller = AliasController(arg1_label='Nombre_comun',
                                 arg2_label='Nombre_cientifico',
                                 rel_label='revisar',
                                 taxon_indexer='Taxonfinder',
                                 sciname_type='all')

    for afile in os.listdir(indir):
        if afile.endswith(".txt"):
            basename = afile.rsplit('.', 1)[0]
            # raw text
            txt_file = os.path.join(indir, basename+".txt")
            print 'txt_file', txt_file

            # annotation file
            controller.apply(txt_file, outdir)
            ann_file = os.path.join(outdir, basename+".ann")
            print 'ann_file', ann_file


def args_ok(argv):
    indir = ''
    outdir = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["idir=", "odir="])
    except getopt.GetoptError:
        msg()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            msg()
            sys.exit()
        elif opt in ("-i", "--idir"):
            indir = arg
        elif opt in ("-o", "--odir"):
            outdir = arg

    return (indir, outdir)


def msg():
    print 'findalias.py -i <inputdir> -o <outputdir>'

if __name__ == '__main__':
    main(sys.argv[1:])
