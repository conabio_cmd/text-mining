'''
Created on Apr 21, 2015

@author: Daniel
'''
import subprocess

from txtmining.base.util import DEFAULT_ENCODING, shell_quote
from txtmining.document.metadata_extractor import MetadataExtractor
from txtmining.document.text_extractor import TextExtractor


class Tika(TextExtractor, MetadataExtractor):
    '''
    classdocs
    '''

    def __init__(self,
                 bin_path,
                 output_dir):
        '''
        Constructor
        '''
        super(Tika, self).__init__(bin_path,
                                   output_dir)

    def extract(self, path_file):
        self._set_call()
        self._output_file = self._get_created_file_path(path_file)
        self._call += ' < ' + shell_quote(path_file)
        self._call += ' > ' + self._output_file
        try:
            super(Tika, self).extract()
            return super(Tika, self)._check_output()
        except subprocess.CalledProcessError:
            return None
        except Exception:
            return None

    def _set_call(self):
        self._call = 'java -jar -Djava.awt.headless=true '
        self._call += self._bin
        self._call += ' --text --encoding=' + DEFAULT_ENCODING

    def extract_meta_data(self, path):
        self._call = 'java -jar -Djava.awt.headless=true '
        self._call += self._bin
        self._call += ' -m ' + shell_quote(path)
        return super(Tika, self).get_metadata(path)
