# -*- coding: utf-8 -*-
'''
Created on Apr 20, 2015

@author: Daniel
'''

import os
import re
import codecs
import pickle
import simplejson


DEFAULT_ENCODING = 'UTF-8'


def shell_quote(path):
    return "'" + path.replace("'", "'\\''") + "'"


def verif_path(path):
    # None or empty string
    if not path:
        raise IOError(
            "Path does not exist: \"{}\"".format(path))
    # not str type
    if not isinstance(path, basestring):
        raise IOError(
            "Path must be string or unicode: {} found.".format(type(path)))
    # does not exist
    if not os.path.exists(path):
        raise IOError(
            "Path does not exist.")
    # path to dir
    if os.path.isdir(path):
        raise IOError(
            "Expected file name, directory name given.")


def get_netineti_model():
    # Load the netineti trained model back from a pickle file.
    current_path = os.path.realpath(__file__)
    current_path = os.path.dirname(current_path)
    model_path = os.path.join(current_path,
                              '../resources/models/netineti.bin')
    model_pickle = open(model_path)
    cornichon = pickle.load(model_pickle)
    return cornichon


def get_spaneti_model():
    # Load the netineti trainde model back from a pickle file.
    current_path = os.path.realpath(__file__)
    current_path = os.path.dirname(current_path)
    model_path = os.path.join(current_path,
                              '../resources/models/spaneti.bin')
    model_pickle = open(model_path)
    cornichon = pickle.load(model_pickle)
    return cornichon


def get_eval_file():
    return os.path.join(get_resources_folder(), 'evaluation/'
                                                'Estudio'
                                                '_ecologico'
                                                '_etnobotanico'
                                                '_vegetacion'
                                                '_municipio'
                                                '_San_Pablo_Etla_'
                                                'Oaxaca.txt')


def get_eval_dir():
    return os.path.join(get_resources_folder(), 'evaluation/')


def get_annot_csv():
    return os.path.join(get_resources_folder(),
                        'evaluation/Gold_Standard_Annotations.csv')


def get_resources_folder():
    current_path = os.path.realpath(__file__)
    current_path = os.path.dirname(current_path)
    return os.path.join(current_path, '../resources')


def global_offsets(line, text, local_start, local_end):
    """Transform local offsets into global offsets of unicode text.
    """
    if line == 1:
        # local and global are the same for fisrt line
        abs_start = local_start
        abs_end = local_end
    else:
        # count breaklines and add local offsets
        n = str(line-1)
        pat = r'(.*\n){1,'+(n)+r'}'
        mm = re.match(pat, text, re.M)
        accumulated = mm.end()
        abs_start = accumulated + local_start
        abs_end = accumulated + local_end
    return (abs_start, abs_end)


class ResourceLoader(object):
    """ResourceLoader: load project especific data files.
    """
    @staticmethod
    def load_regexp(regexpfile_path):
        """Read regexpfile_path - return a regular expression object.
        """
        verif_path(regexpfile_path)
        regex_data = codecs.open(regexpfile_path, 'r', DEFAULT_ENCODING)
        regex_contents = regex_data.read()
        regex_data.close()
        pattern = regex_contents.replace('\t', '').replace('\n', '')
        compiled_re = re.compile(pattern)
        return compiled_re

    @staticmethod
    def load_dict(dictfile_path):
        """Read dictfile_path - return a dict object.
        """
        verif_path(dictfile_path)
        json_data = codecs.open(dictfile_path, 'r', DEFAULT_ENCODING)
        data = simplejson.load(json_data, encoding=DEFAULT_ENCODING)
        json_data.close()
        return data

    @staticmethod
    def load_utf8list(listfile_path):
        """Read listfile_path - return a list of UTF-8 objects.
        """
        verif_path(listfile_path)
        list_file = codecs.open(listfile_path, 'r', DEFAULT_ENCODING)
        # list_file = open(listfile_path, 'rb')
        list_data = list_file.readlines()
        list_file.close()
        list_stripe = [l.strip('\n') for l in list_data]
        return list_stripe
