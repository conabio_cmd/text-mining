'''
Created on Apr 20, 2015

@author: Daniel
'''
import os
from shutil import rmtree
from tempfile import mkdtemp
import unittest

from txtmining.document.pdf_box import PDFBox
from txtmining.document.pdf_to_text import PDFtoText
from txtmining.document.tika import Tika
from txtmining.tests.util import get_pdf_folder
from txtmining.base.util import shell_quote


class TestTextExtractor(unittest.TestCase):

    def setUp(self):
        # Create tmp dir for extraction results
        self.__tmp_dir = mkdtemp()

        self.__pdf_dir = get_pdf_folder()

        self.pdf_box_bin_path = os.getenv("PDFBOX_BIN", None)
        self.tika_bin_path = os.getenv("TIKA_BIN", None)
        self.pdf_to_text_path = os.getenv("PDF_TO_TEXT_BIN", None)

        self.assertIsNotNone(self.pdf_box_bin_path,
                             "You must set PDFBOX_BIN env var")
        self.assertIsNotNone(self.tika_bin_path,
                             "You must set TIKA_BIN env var")
        self.assertIsNotNone(self.pdf_to_text_path,
                             "You must set PDF_TO_TEXT_BIN env var")

        self.__pdf_file = os.path.join(self.__pdf_dir,
                                       'Test_PDF.pdf')

        self.__pdf_box = PDFBox(self.pdf_box_bin_path,
                                self.__tmp_dir)
        self.__tika = Tika(self.tika_bin_path,
                           self.__tmp_dir)

        self.__pdf_to_text = PDFtoText(self.pdf_to_text_path,
                                       self.__tmp_dir)

    def tearDown(self):

        # Remove everything that was necessary for the test

        if os.path.isdir(self.__tmp_dir):
            rmtree(self.__tmp_dir)

    def test_tika_set_call(self):
        self.__tika._set_call()
        cmd = ['java',
               '-jar',
               '-Djava.awt.headless=true',
               self.tika_bin_path,
               '--text',
               '--encoding=UTF-8']
        self.assertEqual(' '.join(cmd), self.__tika._call)

    def test_tika_extract_call(self):
        self.__tika.extract(self.__pdf_file)
        cmd = ['java',
               '-jar',
               '-Djava.awt.headless=true',
               self.tika_bin_path,
               '--text',
               '--encoding=UTF-8',
               '<',
               shell_quote(self.__pdf_file),
               '>',
               self.__tika._output_file]
        self.assertEqual(' '.join(cmd), self.__tika._call)

    def test_pdfbox_set_call(self):
        self.__pdf_box._set_call()
        cmd = ['java',
               '-jar',
               '-Djava.awt.headless=true',
               self.pdf_box_bin_path,
               'ExtractText',
               '-force',
               '-debug',
               '-encoding',
               'UTF-8']
        self.assertEqual(' '.join(cmd), self.__pdf_box._call)

    def test_pdfbox_extract_call(self):
        self.__pdf_box.extract(self.__pdf_file)
        cmd = ['java',
               '-jar',
               '-Djava.awt.headless=true',
               self.pdf_box_bin_path,
               'ExtractText',
               '-force',
               '-debug',
               '-encoding',
               'UTF-8',
               shell_quote(self.__pdf_file),
               self.__pdf_box._output_file]
        self.assertEqual(' '.join(cmd), self.__pdf_box._call)

    def test_pdftotext_set_call(self):
        self.__pdf_to_text._set_call()
        self.assertEqual(self.pdf_to_text_path, self.__pdf_to_text._call)

    def test_pdftotext_extract_call(self):
        self.__pdf_to_text.extract(self.__pdf_file)
        cmd = [self.pdf_to_text_path,
               shell_quote(self.__pdf_file),
               self.__pdf_to_text._output_file]
        self.assertEqual(' '.join(cmd), self.__pdf_to_text._call)


if __name__ == '__main__':
    unittest.main()
    pass
