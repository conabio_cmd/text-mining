# -*- coding: utf-8 -*-

"""
Created on Nov. 30, 2015.

@author: Alejandro Molina.
"""


import unittest
from txtmining.document.nlp import NLPpreprocessor


class NLPpreEvalTest(unittest.TestCase):

    def setUp(self):

        # An example of phrase in Spanish describing species
        self.orig_txt = u'La Acacia farnesiana, comúnmente conocida como Espinillo blanco, es así nombrada debido a las numerosas espinas distribuidas a lo largo de sus ramas, es conocido también como Huisache o Huizache o Vinorama.'

        self.NLPpre = NLPpreprocessor('spanish')

        # expected preprocessing transformations

        self.tokens = u'La Acacia farnesiana , comúnmente conocida como Espinillo blanco , es así nombrada debido a las numerosas espinas distribuidas a lo largo de sus ramas , es conocido también como Huisache o Huizache o Vinorama .'

        self.nopunct = u'La Acacia farnesiana comúnmente conocida como Espinillo blanco es así nombrada debido a las numerosas espinas distribuidas a lo largo de sus ramas es conocido también como Huisache o Huizache o Vinorama'

        self.nostopswords = u'Acacia farnesiana , comúnmente conocida Espinillo blanco , así nombrada debido numerosas espinas distribuidas largo ramas , conocido Huisache Huizache Vinorama .'

        self.stems = u'la acaci farnesian , comun conoc com espinill blanc , es asi nombr deb a las numer espin distribu a lo larg de sus ram , es conoc tambien com huisach o huizach o vinoram .'

        self.bow = u'acaci farnesian comun conoc espinill blanc asi nombr deb numer espin distribu larg ram conoc huisach huizach vinoram'

        # allow to compare long strings
        self.maxDiff = None

    # useful to verify using unicode all the time
    def test_tokenization(self):
        tokens_list = self.NLPpre.tokenize(self.orig_txt)
        tokens_str = str.join(' ', tokens_list)
        self.assertEqual(tokens_str, self.tokens)

    def test_offset_tokenization(self):
        reference = self.orig_txt
        tokens = self.NLPpre.offset_tokenize(reference)
        for t in tokens:
            (tok, offset) = t
            self.assertEqual(tok, reference[offset[0]:offset[1]])

    def test_nopunctuation(self):
        nopunct_str = self.NLPpre.remove_punctuation(self.orig_txt)
        self.assertEqual(nopunct_str, self.nopunct)

    def test_nostopwords(self):
        nostop_str = self.NLPpre.remove_stopwords(self.orig_txt)
        self.assertEqual(nostop_str, self.nostopswords)

    def test_nonumbers(self):
        nonum_str = self.NLPpre.remove_numbers(self.orig_txt)
        self.assertEqual(nonum_str, self.tokens)

    def test_stemmize(self):
        stem_str = self.NLPpre.stemmize(self.orig_txt)
        self.assertEqual(stem_str, self.stems)

    def test_bagofwords(self):
        bow_str = self.NLPpre.get_bagofwords(self.orig_txt,
                                             {'remove_punctuation':True,
                                             'remove_numbers':True,
                                             'remove_stopwords':True,
                                             'use_stemmer':True,
                                             'stoplist':None})
        self.assertEqual(bow_str, self.bow)


if __name__ == '__main__':
    unittest.main()
