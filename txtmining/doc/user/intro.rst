Introducción
============

La biblioteca para minería de textos en biodiversidad es un conjunto de
programas para procesar y extraer información relevante de manera automática a
partir de grandes cantidades de documentos digitales en español.

La versión actual es v\ |release| y cuenta con la siguiente funcionalidad:

- extracción de metadatos;
- conversión de pdf a texto plano;
- reconocimiento óptico de caracteres (OCR);
- corrección de errores de OCR;
- detección de fronteras de las oraciones;
- detección de entidades de interés en biodiversidad (géneros, especies,
  localidades);
- uso eficiente de servicios web especializados (Global Names);
- extracción de información de usos tradicionales de especies;
- multiprocesamiento.

Todas estas funciones se puede utilizar desde la línea de comandos o se puede usar el código fuente como base para desarrollar sistemas más complejos.

El presente manual describe el uso de la biblioteca en la versión actual desde
la línea de comandos.
