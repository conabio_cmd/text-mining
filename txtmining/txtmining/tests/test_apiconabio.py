# -*- coding: utf-8 -*-
"""
Created on Feb 9, 2016.

@author: Alejandro Molina.
"""

import os
import codecs
import unittest

from txtmining.tests.util import get_test_data_folder
from txtmining.API.BioAPI import MexBioAPI
from txtmining.API.BioAPI import AliasFinder


class BioAPITest(unittest.TestCase):

    def setUp(self):
        # get text contents
        self.taxon = 'Litsea glaucescens'
        self.bioapi = MexBioAPI(self.taxon)

        # allow to compare long strings
        self.maxDiff = None

    def test_common_names(self):
        api_str = self.bioapi.get()
        API_names = self.bioapi.common_names(api_str)
        self.assertIsInstance(API_names, list)


class AliasFinderTest(unittest.TestCase):

    def setUp(self):
        self.mini_file = os.path.join(get_test_data_folder(), 'mini.txt')
        self.txt_file = os.path.join(get_test_data_folder(), 'brat',
                                     'human.txt')
        self.contents = codecs.open(self.txt_file, 'r', 'UTF-8').read()
        self.unicodestring =  u'''Ubicación del área de estudio: La comunidad de Iliatenco se encuentra ubicado a una 
altitud de 1030 msnm (longitud Oeste 09°84’10’’ y latitud Norte 17°02’40’’) y El Aserradero se 
encuentra a una altitud de 900 msnm (longitud Oeste 09°84’00’’ y latitud Norte 17°00’24’’) 
(INEGI, 2005), la vegetación original esta compuesta por un bosque tropical caducifolio; además 
de una gran variedad de gramíneas asociadas a los cultivos de maíz (Zea mays L.), platanares 
(Mussa spp), cafetales (Coffea arabica L.), y otros frutales. El clima es cálido subhúmedo 
A(c)w2(w)ig con lluvias entre mayo y octubre, el periodo seco se presenta entre los meses de 
noviembre-abril; la precipitación media anual de 1350 mm (Pacheco y Castro, 2003; Pacheco et 
al., 2003; PNUD, 2007). El tipo de suelo predominante es el regosol que tiene una textura gruesa 
y poca salinidad. En menor medida se encuentran los tipos luvisol, cambisol, leptosol, foezem y 
fluvisol (PNUD, 2007), cabe mencionar que se carece de información detallada sobre los tipos de 
suelo en la comunidad, la información vertida sobre la comunidad en relación a los tipos de 
suelos procede de material no consolidado.'''

        self.AliasFinder = AliasFinder()

        # allow to compare long strings
        self.maxDiff = None

    def test_dict(self):
        ents_rels_dict = self.AliasFinder.get_rels(self.mini_file)
        self.assertIsInstance(ents_rels_dict, dict)

    def test_get_rels(self):
        ents_rels_dict = self.AliasFinder.get_rels(self.txt_file)
        # print ents_rels_dict
        for key, val in ents_rels_dict.items():
            # assert consistence
            if key.startswith('R'):
                self.assertTrue(val['Arg1'] in ents_rels_dict and
                                val['Arg2'] in ents_rels_dict)

            if key.startswith('T'):
                start = val['offsets'][0][0]
                end = val['offsets'][0][1]
                # compare entities
                expected = val['texts'][0]
                obtained = self.contents[start:end]
                self.assertEqual(expected, obtained)

    def test_get_rels_from_unicode(self):
        ents_rels_dict = self.AliasFinder.get_rels_unicode(self.unicodestring)
        self.assertIsInstance(ents_rels_dict, dict)


if __name__ == '__main__':
    unittest.main()
