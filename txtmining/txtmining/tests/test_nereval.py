"""
Created on August 4, 2015.

@author: Alejandro Molina.
"""

import unittest
import os
import numpy

from txtmining.base.util import get_eval_dir
from txtmining.mlearning.ner_eval import NEReval


class NERevalTest(unittest.TestCase):
    def setUp(self):
        self.txt = os.path.join(get_eval_dir(), 'h.txt')
        self.reference_ann = os.path.join(get_eval_dir(), 'h.ann')
        self.toeval_ann = os.path.join(get_eval_dir(), 'h.ann')

        self.eval = NEReval(self.txt,
                            self.reference_ann,
                            self.toeval_ann)

    def test_word_classification(self):
        (confu_matrix,
         class_report) = self.eval.word_classification(["WORD",
                                                        "Nombre_cientifico"])
        self.assertIsInstance(confu_matrix, numpy.ndarray)
        self.assertIsInstance(class_report, unicode)

    def test_relation_detection(self):
        (precision,
         recall) = self.eval.relation_detection(self.eval.ref_rels,
                                                self.eval.ref_rels,
                                                "alias")

        self.assertTrue(precision >= 0.0 and precision <= 1.0)
        self.assertTrue(recall >= 0.0 and recall <= 1.0)

if __name__ == '__main__':
    unittest.main()
