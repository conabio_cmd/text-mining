# -*- coding: utf-8 -*-

import codecs
import os
import re
import unittest

from txtmining.document.text_indexer import (RegexIndexBuilder, RegexGroup)
from txtmining.tests.util import (get_test_data_folder, get_regex_folder)


class IndexBuilderTest(unittest.TestCase):

    def setUp(self):

        segmented_file = os.path.join(get_test_data_folder(),
                                      'segmented/'
                                      '_LUSTRE_data_text_LUSTRE_data_'
                                      'text_0019_'
                                      'INTRODUCCION_AL_CONOCIMIENTO_'
                                      'DE_LOS_RECURSOS_TERAPEUTICOS_VEGETALES'
                                      '_DE_LA_MEDICINA.pdf.txt')
        # It is important to use codecs.open() to get unicode list
        # instead of list of str see:
        # https://ecoinformatica.atlassian.net/wiki/pages/viewpage.action?pageId=1867873
        self.lines = codecs.open(segmented_file, 'r', 'UTF-8').readlines()

        # RegexIndexBuilder
        re_path = os.path.join(get_regex_folder(),
                               'regexp_NombreCientifico.re')
        compiled_re = re.compile(
            open(re_path, "r").read().replace("\t", "").replace("\n", ""),
            re.UNICODE | re.IGNORECASE)
        r = RegexIndexBuilder(segmented_file, compiled_re)
        self.r_index = r.get_index()

        # allow to compare long strings
        self.maxDiff = None

    def test_regindexbuilder(self):
        # assert each entry in index, eg. (426, 'Nicotiana glauca', (131, 147))
        if self.r_index:
            for (num, name, (start, end)) in self.r_index:
                self.assertEqual(name, self.lines[num-1][start:end])


class RegexGroupTest(unittest.TestCase):

    def setUp(self):
        self.items = ['Acacia farnesiana', 'Opuntia guatemalensis']
        self.rg = RegexGroup(self.items)
        self.matched_names = []
        self.line = '''La Acacia farnesiana es un árbol (A. farnesiana) y Opuntia es el género
                  de Opuntia guatemalensis (O. sp) debe hacer match pero Y. spp. no debería hacer match.'''
        self.expected = ['Acacia farnesiana',
                         'A. farnesiana',
                         'Opuntia guatemalensis',
                         'O. sp']

    def test_nolist(self):
        self.assertRaises(ValueError, self.rg.__init__, 4324)
        self.assertRaises(ValueError, self.rg.__init__, None)
        self.assertRaises(ValueError, self.rg.__init__, 'a string')

    def test_bad_items(self):
        self.assertRaises(ValueError, self.rg.__init__, ['a b', 23])

    def test_len_items(self):
        self.assertRaises(ValueError, self.rg.__init__, ['a b', 'c'])
        self.assertRaises(ValueError, self.rg.__init__, ['a b', 'c d e'])

    def test_match_str(self):
        # Warning: Do not use re.U (UNICODE) flag in find
        #          because results are incorrect
        for m in self.rg.re.finditer(self.line):
            self.matched_names.append(m.group('scientificNames'))

        self.assertEqual(self.expected, self.matched_names)

    def test_match_file(self):
        list_file = os.path.join(get_test_data_folder(),
                                 'regexp_list.txt')
        rg = RegexGroup(list_names=list_file)
        for m in rg.re.finditer(self.line):
            self.matched_names.append(m.group('scientificNames'))
        self.assertEqual(self.expected, self.matched_names)

if __name__ == '__main__':
    unittest.main()
