# -*- coding: utf-8 -*-
"""
Created on October 14, 2015.

@author: Alejandro Molina.
"""

import os
import codecs
import re
import string
import unicodecsv
from tempfile import mkdtemp
from shutil import rmtree
import warnings
from txtmining.document.nlp import NLPpreprocessor
from txtmining.base.util import global_offsets


class BratConv(object):

    @staticmethod
    def ann_format(infile, index, label='entity'):
        """
        Transform an index into a Brat formated string.

        :param infile: the file to process.

        :param index: an index of that file, e.g.:
                      [[4, 'Dendroctonus sp.', [73, 91]]...].

        :return: :class:`str` Each line in Brat format, e.g.:
                         T43 Nombre_cientifico 83014 83027   Schinus molle
                         ...

        About Brat annotation see:
                http://2011.bionlp-st.org/home/file-formats

        :rtype: str
        """
        try:
            text = codecs.open(infile, 'r', 'UTF-8').read()
        except Exception, e:
            raise e

        # index format e.g.
        # [[4, 'Dendroctonus sp.', [73, 91]],...]
        brated = dict()
        for (i, tup) in enumerate(index):
            (line, name, canonical, offst) = tup
            # local offset
            local_start = offst[0]
            local_end = offst[1]
            if local_start is None or local_end is None: # SKIP inconsistent
                continue
            # get global offsets
            (abs_start, abs_end) = global_offsets(line,
                                                  text,
                                                  local_start,
                                                  local_end)

            # get name from line
            real_name = text[abs_start:abs_end]
            # canonical form if abbreviated
            # canonname = canonical if canonical != real_name else None
            # format
            brated.setdefault('T{0}'.format(i + 1),
                              {'offsets': [(abs_start, abs_end)],
                               'type': 'ENT',
                               'label': label,
                               'texts': [real_name],
                               'canonical': canonical})

        # Brat format e.g.
        # T43 Nombre_cientifico 83014 83027   Schinus molle
        return brated

    @staticmethod
    def dict2ann(brat_dict):
        """
        Transform a brat dictionary into a Brat formated string.

        :param brat_dict: the dictionary to process: see ann_format().

        :return: :class:`str` Each line in Brat format, e.g.:
                         T43 Nombre_cientifico 83014 83027   Schinus molle
                         R1 alias Arg1:T2 Arg2:T1
                         ...

        About Brat annotation see:
                http://2011.bionlp-st.org/home/file-formats

        :rtype: str
        """
        brat_string = ''

        for k, v in brat_dict.items():
            if k.startswith('T'):
                entid = k
                label = v['label']
                start = v['offsets'][0][0]
                end = v['offsets'][0][1]
                ent = v['texts'][0].encode('UTF-8')
                can = v.get('canonical', None)
                if can and can != ent:
                    brat_line = '{}\t{} {} {}\t{} # {}\n'.format(entid,
                                                                 label,
                                                                 start,
                                                                 end,
                                                                 ent,
                                                                 can)
                else:
                    brat_line = '{}\t{} {} {}\t{}\n'.format(entid,
                                                            label,
                                                            start,
                                                            end,
                                                            ent)
            elif k.startswith('R'):
                entid = k
                label = v['label']
                arg1 = v['Arg1']
                arg2 = v['Arg2']
                brat_line = '{}\t{} Arg1:{} Arg2:{}\n'.format(entid,
                                                              label,
                                                              arg1,
                                                              arg2)
            else:
                raise ValueError('Unknown entry in brat dictionary '+k+v)

            brat_string += brat_line

        return brat_string

    @staticmethod
    def ann2dict(ann_file):
        """
        Transform a brat formated string into a Brat dictionary.

        :param ann_file: the path to the .ann file.

        :return: :class:`dict` A dictionary with NER information

        About Brat annotation see:
                http://2011.bionlp-st.org/home/file-formats

        :rtype: dict
        """
        brat_dict = dict()

        (ents, rels) = list_from_ann(ann_file)
        # put entities into dictionary
        for e in ents:
            (entid, label, ent_offset_start, ent_offset_end, name, canon) = e
            brat_dict['T{}'.format(entid)] = {'label': label,
                                              'offsets': [ent_offset_start, ent_offset_end],
                                              'texts': [name],
                                              'canonical': canon}

        # put rels into dictionary
        for r in rels:
            (relid, arg1, label, arg2) = r
            (id_a1, label_a1, offset_start_a1, offset_end_a1, text_a1, comment_a1) = arg1
            (id_a2, label_a2, offset_start_a2, offset_end_a2, text_a2, comment_a2) = arg2
            brat_dict['R{}'.format(relid)] = {'label': label,
                                              'Arg1': 'T{}'.format(id_a1),
                                              'Arg2': 'T{}'.format(id_a2)}
        return brat_dict

    @staticmethod
    def ann2csv(txt_file, ann_file, context, taxlevel=False):
        """
        Transform a brat file (.ann) into a csv formated string.

        :param txt_file: the file to process.

        :param ann_file: a brat annotation file of txt_file.

        :return: :class:`str` Each line containing a row in csv format:
                         archivo    taxón   canónico    común   contexto
                        txt_file.txt   Arachnida   Arachnida   arácnidos   ...  grupos de la clase Arachnida no han sido estudiados formalmente y que deberán atenderse a la brevedad, dado que existen en el Estado otros arácnidos con importancia méd ...
                        txt_file.txt   Scorpiones  Scorpiones  alacranes   ...  especies del orden Scorpiones (alacranes o escorpiones) para ...
                         ...

        About Brat annotation see:
                http://2011.bionlp-st.org/home/file-formats

        :rtype: str
        """
        (ents, rels) = list_from_ann(ann_file)
        bname = basename(txt_file)
        tmp_dir = mkdtemp()
        csvfile = os.path.join(tmp_dir, 'temp.csv')
        with open(csvfile, 'w') as f:
            csv_writer = unicodecsv.writer(f, encoding='utf-8')
            # csv headers
            csv_writer.writerow([u'archivo',
                                 # TODO: u'idconabio',
                                 u'taxón',
                                 u'canónico',
                                 # TODO: u'categoría',
                                 u'común',
                                 u'contexto'])
            # taxon-common name asociations
            for r in rels:
                (relid, comm_name, label, scie_name) = r
                (id_comm, comm_label, comm_offset_start, comm_offset_end, cname, ccanon) = comm_name
                (id_scie, scie_label, scie_offset_start, scie_offset_end, sname, scanon) = scie_name
                offsets = [comm_offset_start, comm_offset_end, scie_offset_start, scie_offset_end]
                s = get_context(txt_file, min(offsets), max(offsets), context)
                csv_writer.writerow([bname,
                                     # TODO: u'idconabio',
                                    sname,
                                    sname,
                                     # TODO: u'categoría',
                                    cname,
                                    s])
            # taxon
            for e in ents:
                (entid, label, ent_offset_start, ent_offset_end, name, canon) = e
                if label == 'Nombre_cientifico':
                    s = get_context(txt_file, ent_offset_start, ent_offset_end, context)
                    csv_writer.writerow([bname,
                                         # TODO: u'idconabio',
                                         name,
                                         canon,
                                         # TODO: u'categoría',
                                         u'',
                                         s])
        csvstr = codecs.open(csvfile, 'r', encoding='UTF-8').read()
        rmtree(tmp_dir)
        return csvstr

    @staticmethod
    def brat2classes(txt_file, ann_file, default_label='WORD'):
        """
        A tagged list of words from txt file given the annotations in ann_file.

        :param txt_file: the file to process.

        :param ann_file: a brat annotation file of txt_file.

        :return: two lists: classes and relations.

        About Brat annotation see:
                http://2011.bionlp-st.org/home/file-formats

        """
        # list entities and relations
        ents, rels_with_class = list_from_ann(ann_file)

        # Get class for every entity
        ents_with_class = []
        tokens = tokens_from_file(txt_file)
        if ents:
            (entid, label, start_ent, end_ent, verbatim, canonical) = ents.pop(0)
        for (tok, offs) in tokens:
            (start_tok, end_tok) = offs
            if start_tok < start_ent:
                ents_with_class.append((tok, default_label))
            elif start_tok >= start_ent and end_tok <= end_ent:
                ents_with_class.append((tok, label))
            elif end_tok > start_ent:
                ents_with_class.append((tok, default_label))
                if ents:
                    (entid, label, start_ent, end_ent, verbatim, canonical) = ents.pop(0)
        return ents_with_class, rels_with_class


def tokens_from_file(text_file):
    with codecs.open(text_file, 'r', encoding='UTF-8') as f:
        contents = f.read()
    NLPpre = NLPpreprocessor('spanish')
    return NLPpre.offset_tokenize(contents)


def list_from_ann(ann_file):
    # read ann and get list of entities:
    with codecs.open(ann_file, 'r', encoding='UTF-8') as f:
        lines = f.readlines()
    # different patterns
    # T9    Nombre_cientifico 1242 1257 Ficus benjamina
    # R20   alias Arg1:T42 Arg2:T12
    ent_pattern = r'T(\d+)\t(\w+)\s(\d+)\s(\d+)\t(.*)'
    rel_pattern = r'R(\d+)\t(\w+)\sArg1:T(\d+)\sArg2:T(\d+)(.*)'

    entities = []
    relations = []
    for l in lines:
        ent_match = re.match(ent_pattern, l)
        rel_match = re.match(rel_pattern, l)
        if ent_match:
            (entid, label, start, end, verbatim) = ent_match.groups()
            s = int(start)
            e = int(end)
            if (s < e) and verbatim != '':
                # support for abbreviated names using comment symbol
                # to indicate the canonical name e.g.:
                # T9    Nombre_cientifico 1242 1257 F. benjamina#Ficus benjamina
                found, canonical = verbatim.split('#') if '#' in verbatim else [verbatim, verbatim]
                entities.append((int(entid), label, s, e, found, canonical))
            else:
                w = 'Inconsistent entity value: {} {} {} {} {}'.format(entid, label, start, end, verbatim)
                warnings.warn(w, Warning)
        elif rel_match:
            (entid, label, arg1, arg2, verbatim) = rel_match.groups()
            relations.append((int(entid), label, int(arg1), int(arg2)))
    # sort by offset start
    sorted_entities = sorted(entities, key=lambda x: x[2])
    # additional info in rels
    rels_info = []
    for (relid, rel_name, arg1, arg2) in relations:
        (info_arg1, info_arg2) = rel_info(entities, arg1, arg2)
        rels_info.append((relid, info_arg1, rel_name, info_arg2))

    return sorted_entities, rels_info


def idx_from_id(entities, entid):
    ent_idx = [y[0] for y in entities].index(entid)
    return ent_idx


def entname(entities, entid):
    ent_idx = idx_from_id(entities, entid)
    return entities[ent_idx][4]


def rel_info(entities, entid1, entid2):
    ent_idx1 = idx_from_id(entities, entid1)
    ent_idx2 = idx_from_id(entities, entid2)
    return ((entities[ent_idx1], entities[ent_idx2]))


def aux_ents_array(entities, final, default_label):
    q = []
    n = len(entities)
    for i in range(n):
        (entid, label, s, e, verbatim) = entities[i]
        if i == 0:
            q+=[(0, s, default_label), (s, e, label)]
        elif i == n-1:
            q+=[(s, e, label), (e, final, default_label)]
        else:
            before = entities[i-1][3]
            after = entities[i+1][2]
            q+=[(before, s, default_label), (s, e, label), (e+1, after, default_label)]
    return q


def isAscii(s):
    if s == '.':
        return True
    for c in s:
        if c not in string.ascii_letters:
            return False
    return True


def apply_rule(classes_list, default_label="WORD"):
    retlist = []
    for (w, k) in classes_list:
        t = k if isAscii(w) else default_label
        retlist.append((w, t))
    return retlist


def get_context(txt_file, start, end, context=100):
    try:
        with codecs.open(txt_file, 'r', encoding='UTF-8') as f:
            contents = f.read()
            s = u"... {} ...".format(contents[start-context:end+context])
    except:
        raise
    return s.replace('\n', ' ').replace('\r', '')


def basename(path):
    try:
        bname = path.split('/')[-1]
    except:
        raise
    return bname
