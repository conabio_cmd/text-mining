# -*- coding: utf-8 -*-

import os
import unittest

from txtmining.document.fragment_extractor import (FragmentExtractor,
                                                   PlantProjectReLoader)
from txtmining.tests.util import get_test_data_folder, get_regex_folder


class FragmentExtractorTest(unittest.TestCase):

    def setUp(self):
        regex = get_regex_folder()
        (nombre_o_parte, verbo, uso) = PlantProjectReLoader.load(regex)
        self.frag_extractor = FragmentExtractor(nombre_o_parte, verbo, uso)
        # this will be extracted
        self.testf = self.txtfile = os.path.join(get_test_data_folder(),
                                                 'vaquero.txt')

        # view long diff
        self.maxDiff = None

    def test_exception_constructor(self):
        self.assertRaises(Exception, FragmentExtractor, 1, 2, 3)

    def test_fragment_context_exact(self):
        frags = self.frag_extractor.extract(file_in=self.testf,
                                            context_lines_before=2,
                                            context_lines_after=2)

        first_fragment = frags[0]
        self.assertEqual(len(first_fragment['contextBefore'].splitlines()),
                         2)
        self.assertEqual(len(first_fragment['contextAfter'].splitlines()),
                         2)

    def test_fragment_context_small(self):
        frags = self.frag_extractor.extract(file_in=self.testf,
                                            context_lines_before=10,
                                            context_lines_after=20)
        first_fragment = frags[0]
        self.assertNotEqual(len(first_fragment['contextBefore'].splitlines()),
                            10)
        self.assertNotEqual(len(first_fragment['contextAfter'].splitlines()),
                            20)
        self.assertLessEqual(len(first_fragment['contextBefore'].splitlines()),
                             10)
        self.assertLessEqual(len(first_fragment['contextAfter'].splitlines()),
                             20)

    # All that is matched is found in the file using the offset
    def test_fragmentextractor(self):
        frags = self.frag_extractor.extract(file_in=self.testf,
                                            context_lines_before=1,
                                            context_lines_after=1)

        for f in frags:
            text = u'{contextBefore}{documentFragment}{contextAfter}'.format(**f)
            matchInfo = f['matchInfo']
            for k, v in matchInfo.items():
                for match_dict in v:
                    instance_match = match_dict['instance']
                    start = match_dict['offsetStart']
                    end = match_dict['offsetEnd']
                    instance_text = text[start:end]
                    self.assertEqual(instance_match.lower(),
                                     instance_text.lower())

    # expected unicode instances
    def test_unicodematch(self):
        frags = self.frag_extractor.extract(file_in=self.testf,
                                            context_lines_before=0,
                                            context_lines_after=0)

        minfo = frags[0]['matchInfo']
        matched_name = minfo['commonName'][0]['instance'].encode('UTF-8')
        self.assertEqual(matched_name, 'vaquero cabecicafé')

        matched_verb = minfo['verbo_usar'][0]['instance'].encode('UTF-8')
        self.assertEqual(matched_verb, 'hubiese usado')

        matched_use = minfo['artesanal'][0]['instance'].encode('UTF-8')
        self.assertEqual(matched_use, 'jícaras')


if __name__ == '__main__':
    unittest.main()
