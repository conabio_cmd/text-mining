# -*- coding: utf-8 -*-


import os
import unittest
import simplejson
import codecs

from util import (get_test_data_folder)
from txtmining.mlearning.ml_extract import (BratExtract)


class IndexBuilderTest(unittest.TestCase):

    def setUp(self):
        self.txt = os.path.join(get_test_data_folder(), 'brat/coniferas.txt')
        self.ann = os.path.join(get_test_data_folder(), 'brat/coniferas.ann')
        self.json = os.path.join(get_test_data_folder(), 'brat/coniferas.json')
        self.BE = BratExtract()

        self.maxDiff = None

    def test_isList(self):
        returned_list = self.BE.get_sample(self.txt, self.ann, self.json)
        with codecs.open(self.json, 'rb', 'UTF-8') as fp:
            returned_list = simplejson.load(fp)
        self.assertIsInstance(returned_list, list)
        # self.assertListEqual(returned_list, list)

    def test_isNonEmpty(self):
        returned_list = self.BE.get_sample(self.txt, self.ann, self.json)
        with codecs.open(self.json, 'rb', 'UTF-8') as fp:
            returned_list = simplejson.load(fp)
        self.assertIsNot(returned_list, [])

    def test_byitem(self):
        returned_list = self.BE.get_sample(self.txt, self.ann, self.json)
        with codecs.open(self.json, 'rb', 'UTF-8') as fp:
            returned_list = simplejson.load(fp)
        # print returned_list
        for i, (line, content, labels) in enumerate(returned_list):
            for annot in labels:
                if annot["type"] in ("Nombre_comun", "Nombre_cientifico"):
                    s = annot["offset"][0]
                    e = annot["offset"][1]
                    self.assertEqual(content[s:e], annot["text"])

if __name__ == '__main__':
    unittest.main()
