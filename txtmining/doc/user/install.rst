Requerimientos
==============

Tener instalado `Java Runtime Environment ≥ 1.8
<https://www.java.com/es/download/>`_ y python 2.7.
Si su sistema tiene otra versión de python, puede `utilizar un entorno virtual de python
<http://rukbottoland.com/blog/tutorial-de-python-virtualenv/>`_.

Estar familiarizado con la ejecución de programas desde la línea de comandos.

Instalación y configuración
===========================

Descargue o clone el código desde el repositorio público (apróximadamente 135 MB): https://bitbucket.org/conabio_cmd/text-mining

Instale los paquetes necesarios: ::

$ pip install -r txtmining/requirements.txt

Se pueden configurar variables de entorno para hacer más cómodo el uso del
script ``txtm.py`` (ver :ref:`variables-de-entorno`).
