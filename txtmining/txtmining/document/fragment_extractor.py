# -*- coding: utf-8 -*-
"""
Created on Apr 27, 2015.

@author: Alejandro Molina
"""
# UTF-8 read support
# re.sub match text
import os.path
import re
import codecs
from txtmining.base.util import DEFAULT_ENCODING


class FragmentExtractor(object):

    """docstring for FragmentExtractor."""

    def __init__(self, terms, patterns, cuewords):
        try:
            # this for a use patterns mention in the fragment
            self.structured_re = re.compile('.*(' + terms + ')+.*' +
                                            patterns + '+.*(' +
                                            cuewords + ')+.*',
                                            re.UNICODE | re.IGNORECASE)
            # this for fill the fragment dictionary
            self.unestructured_re = re.compile(terms + '|' + patterns + '|' +
                                               cuewords,
                                               re.UNICODE | re.IGNORECASE)
        except Exception, e:
            raise e

    def extract(self, file_in, context_lines_before, context_lines_after):
        fragments = []
        linesarray = codecs.open(file_in, 'r', DEFAULT_ENCODING).readlines()
        # linesarray = open(file_in, 'rb').readlines()
        for i, line in enumerate(linesarray):
            # current line matches regexp
            if self.structured_re.match(line):
                uses_data = dict()
                uses_data[u'documentName'] = unicode(os.path.basename(file_in))
                uses_data[u'lineNumber'] = i
                context_before = ''.join(linesarray[i - context_lines_before:i])
                context_before = context_before
                uses_data[u'contextBefore'] = (context_before)
                uses_data[u'documentFragment'] = (line)
                context_after = linesarray[i + 1:
                                           i + context_lines_after + 1]
                context_after = ''.join(context_after)
                context_after = context_after
                uses_data[u'contextAfter'] = (context_after)
                contextualized = (uses_data[u'contextBefore'] +
                                  uses_data[u'documentFragment'] +
                                  uses_data[u'contextAfter'])

                # creates match dictionary with cue words
                match_info = dict()
                for m in self.unestructured_re.finditer(contextualized):

                    for group, value in m.groupdict().iteritems():
                        if value:
                            if group in match_info.keys():
                                match_info[group].append({u"instance":
                                                          value,
                                                          u"offsetStart":
                                                          m.start(group),
                                                          u"offsetEnd":
                                                          m.end(group)})
                            else:
                                match_info[group] = [{u"instance":
                                                      value,
                                                      u"offsetStart":
                                                      m.start(group),
                                                      u"offsetEnd":
                                                      m.end(group)}]
                # print(u"match_info:{}\n".format(match_info))
                uses_data[u'matchInfo'] = match_info
                fragments.append(uses_data)
        return fragments


class PlantProjectReLoader(object):

    """PlantReLoader: loads plants project especific files."""

    @staticmethod
    def load(regexdir):
        d = dict()
        sciname_commname_parts = []
        lexicon = []
        for re_file in os.listdir(regexdir):
            if not re_file.startswith('.'):
                re_path = os.path.join(regexdir, re_file)
                f = codecs.open(re_path, 'r', DEFAULT_ENCODING)
                contents = f.read()
                f.close()
                strip_contents = contents.replace(u'\t', u'').replace('\n', '')
                d[re_file] = strip_contents
                if re_file == 'regexp_PVFuncionales.re':
                    functional_verb = d[re_file]
                elif re_file in ['regexp_NombreCientifico.re',
                                 'regexp_NombreComun.re',
                                 'regexp_PartesEstructuras.re']:
                    sciname_commname_parts.append(d[re_file])
                else:
                    lexicon.append(d[re_file])
        return ('|'.join(sciname_commname_parts),
                functional_verb,
                '|'.join(lexicon))
    # TODO: use list of RegexGroup using this stub
    # @staticmethod
    # def load(listsdir):
    #     regex_groups = []

    #     for list_file in os.listdir(listsdir):
    #         if not list_file.startswith('.'):
    #             path = os.path.join(listsdir, list_file)
    #             l = ResourceLoader.load_utf8list(path)
    #             grpname = list_file
    #             sep = r'\b|\b'
    #             g = RegexGroup(l, grpname, sep, re.IGNORECASE | re.UNICODE)
    #             regex_groups.append(g)

    #     return regex_groups
