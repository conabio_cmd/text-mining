"""
Created on August 4, 2015.

@author: Alejandro Molina.
"""

import os
import pickle

from txtmining.lib.netineti.neti_neti_trainer import NetiNetiTrainer
from txtmining.lib.netineti.neti_neti import NetiNeti
from txtmining.document.netineti_indexer import SpaNeti
from txtmining.mlearning.ml_eval import ScinamesEvaluator
from txtmining.base.util import (get_eval_file,
                                 get_resources_folder,
                                 get_annot_csv)


esp_traindir = 'data/'
out_modeldir = os.path.join(get_resources_folder(), 'models/netimodels')

# criteria
consider_abbrev = True
scitype = 'all'
beta = 1
simcutoff = 0.8

cspan_vals = range(1, 4)
sci_names_training_vals = range(500000, 1200000, 50000)
negative_trigrams_vals =  range(500, 1000, 50)

eval_file = get_eval_file()
annot_csv = get_annot_csv()
scie = ScinamesEvaluator(annot_csv)

for span in cspan_vals:
    for names in sci_names_training_vals:
        for ngrams in negative_trigrams_vals:
            print '{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(simcutoff, beta, scitype, consider_abbrev, names, ngrams, span)
            spannt = NetiNetiTrainer(positive_training_file=os.path.join(esp_traindir,"names_in_context.txt"),
                                     negative_training_file=os.path.join(esp_traindir,"no_names.txt"),
                                     sci_names_training_num=names,
                                     negative_trigrams_num=ngrams,
                                     context_span=span)
            pickle_name = 'en_NB_{}_{}_{}_neti.bin'.format(names, ngrams, span)
            pickle_path = os.path.join(out_modeldir, pickle_name)
            # print pickle_path
            pickle.dump(spannt, open(pickle_path, 'wb'))

            # load
            model = spannt
            spaneti = SpaNeti(spannt)

            scinames = spaneti.get_indexf(eval_file,
                                          abbrev=consider_abbrev,
                                          sciname_type=scitype)

            evalresults = scie.eval(scinames,
                                    abbrev=consider_abbrev,
                                    sciname_type=scitype,
                                    beta=beta,
                                    simcutoff=simcutoff)
            (precision, recall, fmeasure) = evalresults
            print '{}\t{}\t{}'.format(precision, recall, fmeasure)
