Uso desde la línea de comandos
==============================

El script principal txtmining/scripts/txtm.py puede ser utilizado para realizar diferentes tareas por medio de un archivo de configuración o a través de comandos. Al ejecutar el script principal sin argumentos se desplegará la lista de comandos disponibles: ::

    $ python txtm.py

       Usage: txtm [subcommand] || [configuration_file]

      Available subcommands:
                            MetadataController
                            PDFparserController
                            PreprocessingController
                            ReIndexController
                            GnIndexController
                            FragmentController


En el resto del manual se explica en detalle cada uno de los comandos, sus opciones y el uso de un archivo de configuración.
