# -*- coding: utf-8 -*-
"""
Module responsible to define calls to a Global Names Recognition and Discovery
service.
"""
# added support for abstract classes
# re.sub match text
from __future__ import absolute_import
import codecs
from itertools import islice
import json
import re
import sys

from txtmining.document.globalnames import (app, submit_text, check_status)
from celery.result import ResultSet

app.conf.CELERY_ALWAYS_EAGER = True

# NOTES
# re.LOCALE Make \w, \W, \b, \B, \s and \S dependent on the current locale.
# re.UNICODE Make \w, \W, \b, \B, \d, \D, \s and \S dependent on the Unicode
# character properties database.
# call global names service endpoint


def get_lotsize(stream):
    """
    Define the lot size use by a file.

    :param stream: text file stream

    :return: :class:`int` [50, 100, 150] for small, medium or large files

    :rtype: int
    """
    nlines = sum(1 for _ in stream)
    stream.seek(0)
    # small file
    if nlines <= 50:
        return nlines
    # medium file
    elif nlines > 50 and nlines <= 250:
        return 50
    elif nlines > 250 and nlines <= 700:
        return 100
    # large file
    else:
        return 150


def split_to_lots(stream, lot_size):
    """
    Split a file in lots given a lot size.

    :param stream: text file stream

    :param lot_size: :class:`int` of given lot size

    :return: :class:`list` of text lots

    :rtype: list
    """
    lots = []
    while True:
        next_n_lines = list(islice(stream, lot_size))
        if not next_n_lines:
            break
        lots.append(next_n_lines)

    return lots


def gn_caller(text):
    return (submit_text.s() | check_status.s())(text)


class GlobalNamesCelery():
    """
    Class responsible to process a file using the Global Names Recognition and
    Discovery service.

    :param file_in: path to file to be processed
    :param celery_conf: configuration object to use with celery (see
                        http://celery.readthedocs.org/en/latest/userguide/application.html#config-from-object)
    """
    def __init__(self, file_in, celery_conf=None):
        if celery_conf:
            app.config_from_object(celery_conf)
        self.HTTP_OK = 200
        self.GN_URL = "http://gnrd.globalnames.org/name_finder.json"
        self.GN_NOT_READY = 303
        # self.FILE = open(file_in, "rUb")
        self.FILE = codecs.open(file_in, 'r', 'UTF-8')
        self.lines_by_lot = get_lotsize(self.FILE)
        self.lots = split_to_lots(self.FILE, self.lines_by_lot)
        self.results = self.submit_tasks()
        # """:class:`ResultSet` where each item is a :class:`AsyncResult` for
        # the text lots."""
        self.gn_answerjson = None

    def submit_tasks(self):
        """
        Construct a :class:`ResultSet` where each item is a :class:`AsyncResult`
        for a given text lot.

        :return: :class:`ResultSet`

        :rtype: ResultSet
        """
        results = ResultSet([])
        for text in self.lots:
            res = gn_caller(text)
            results.add(res)
        return results

    def request_answers(self):
        self.gn_answerjson = self.results.join(propagate=False)

    def get_index(self):
        # call GN if not already called
        if self.gn_answerjson is None:
            self.request_answers()
        index = []
        for (lot_num, answer_str) in enumerate(self.gn_answerjson):
            # useful for debug
            # print 'lot', lot_num, 'from', (lot_num*self.lines_by_lot)+1, 'to'
            # (lot_num*self.lines_by_lot)+self.lines_by_lot+1
            if answer_str:
                try:
                    answer = json.loads(answer_str)
                except Exception:
                    print "Unexpected error:", sys.exc_info()[0]
                    continue
                if answer['status'] == self.HTTP_OK:
                    # contents in array and single str
                    lot_contents = ''.join(filter(None, self.lots[lot_num]))
                    arr_contents = lot_contents.split('\n')

                    # get only verified names
                    try:
                        v_names = [n for n in answer["names"]
                                   for resolved in answer['resolved_names']
                                   if (resolved['is_known_name'] and
                                   n['identifiedName'] ==
                                   resolved['supplied_name_string'])]
                        # if not resolved_names
                    except KeyError:
                        return []

                    names_set = [name['identifiedName'] for name in v_names]
                    names_set = set(names_set)

                    # line number and offsets correction
                    try:
                        for name in names_set:
                            scaped_name = re.escape(name)
                            p = re.compile(scaped_name,
                                           re.MULTILINE |
                                           re.DOTALL |
                                           re.UNICODE |
                                           re.IGNORECASE)

                            for m in p.finditer(lot_contents):
                                lineno = lot_contents.count('\n', 0, m.start())

                                for n in p.finditer(arr_contents[lineno]):
                                    # useful debug
                                    # print scaped_name
                                    index.append(((lot_num * self.lines_by_lot)
                                                 + lineno + 1,
                                                 lot_contents[m.start():m.end()],
                                                 n.span()))
                    except:
                        print "Unexpected error:", sys.exc_info()[0]
                        print "name= " + name
                        raise

        return sorted(set(index))
