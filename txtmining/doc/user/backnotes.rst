Acerca del proyecto
===================

Este proyecto es desarrollado por coordinación de ecoinformática de la comisión
nacional para el conocimiento y uso de la biodiversidad (CONABIO) de México.

Para cualquier asunto relacionado, favor de enviar un correo a
ecoinformatica@conabio.gob.mx.

Publicaciones
=============

Molina, A., Barrios, J., Sierra, R., Zenteno, D.: A Text Mining Library for Biodiversity Literature. 14th Mexican International Conference on Artificial Intelligence (MICAI), (in press, 2015).

Referencias
===========

.. [apache] © The Apache Software Foundation.

.. [poppler] © The Poppler Developers.

.. [taxfinder] Leary, P.R., Remsen, D.P., Norton, C.N., Patterson, D.J., Sarkar, I.N.: ubiorss: tracking taxonomic literature using rss. Bioinformatics 23 (2007) 1434–1436.

.. [neti] Akella, L.M., Norton, C.N., Miller, H.: Netineti: discovery of scientific names from text using machine learning methods. BMC bioinformatics 13 (2012) 211.

.. [laweb] Sierra, G.E., Alarcón, R., Molina, A., Aldana, E.: Web explotation for definition extraction. In: IEEE Latin American Web Congress (LA-WEB’09), Merida, Mexico (2009) 217–223.
