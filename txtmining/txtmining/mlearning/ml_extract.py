# -*- coding: utf-8 -*-
"""
Created on November 13, 2015.

@author: Alejandro Molina, Daniel Zenteno.
"""

import codecs
import re
import simplejson


def data_from_annot(annotations, label):
    """
    Extract all of the positive and negative examples from annotations
    given an specific label.

    Format of entries in annotation list is:
    [line_num, text, labels_list], where
    labels_list format depends on type relation or span:

    spans example:
    {
        "text": "Taxodium mucronatum",
        "type": "Nombre_cientifico",
        "offset": [
          125,
          144
        ]
      }

    relation example:
      {
        "arg2_text": "Salix babylonica",
        "arg1_offset": [
          217,
          223
        ],
        "arg2_line": 16,
        "arg2_offset": [
          225,
          241
        ],
        "arg1_text": "sauces",
        "type": "alias",
        "arg1_line": 16
      }

    """
    pos = []
    neg = []

    if any(annots != [] for (line_num, text, annots) in annotations):
        # do this if text has been annotated
        antilabel = 'NEG_'+label
        for (line_num, text, annots) in annotations:
            if any(d['type'] == label for d in annots):
                pos.append((text, label))
            else:
                neg.append((text, antilabel))

    return (pos, neg)


def invert_arbitrary(d, ldict, p=[]):
    for k, v in ldict.items():
        if isinstance(v, dict):
            invert_arbitrary(d, v, p + [k])
        else:
            if v in d:
                d[v] += (p + [k], )
            else:
                d[v] = (p + [k], )


class BratExtract(object):

    def get_sample(self, txtfile, annfile, output):
        """
        Extract annotations in Brat format to a list.

        :param txtfile: the original txt file to process.

        :param annfile: the annotations file.

        :return: :class:`list` Each phrase annotated, e.g.:
                    [(1, “a) Comestibles”, [ ]),
                     (4, “Plantas registradas como comestibles de mayor mención
                     fueron: el laurel (Litsea glaucescens) ...”,
                      [ (“nombre_comun”, (63,72)),
                      (“nombre_cientifico”, (74,92)),
                      (“alias”, (72, 74))...] ),
                    ...
                    ]

        About Brat annotation format see:
                http://2011.bionlp-st.org/home/file-formats

        :rtype: list
        """
        list_char_counts = []
        entities_dict = {}

        with codecs.open(txtfile, 'rb', 'UTF-8') as t:
            tlines = t.readlines()

        accum = 0
        for l in tlines:
            char_count = len(l)
            accum += char_count
            list_char_counts.append(accum)
        with codecs.open(annfile, 'rb', 'UTF-8') as a:
            alines = a.readlines()
        # if a file was not annotated, skip everything
        if len(alines) == 0:
            return None

        # e.g. T1   Nombre_cientifico 1558 1577 Pinaropappus roseus
        # R5    alias Arg1:T22 Arg2:T5
        exp = r'((?P<Tid>T\d+)\t(?P<Tlab>\w+)\s(?P<Tos>\d+)\s(?P<Toe>\d+)\t(?P<Tverb>.*)|(?P<R>(?P<Rid>R\d+)\t(?P<Rlab>\w+)\sArg1:(?P<Ra1>T\d+)\sArg2:(?P<Ra2>T\d+)(?P<Rverb>.*)))'
        compiled = re.compile(exp, re.U)

        weirdcase_exp = r'(?P<Tid>T\d+)\t(?P<Tlab>\w+)\s(?P<offsets>.*)\t(?P<Tverb>.*)'
        wcompiled = re.compile(weirdcase_exp, re.U)

        for annot in alines:
            m = compiled.match(annot)
            wm = wcompiled.match(annot)

            # in T annotation
            if wm:
                if not m:
                    ofs = wm.group('offsets').replace(' ', ';').split(';')
                    sortedofs = sorted(ofs)
                    tid = wm.group('Tid')
                    tlab = wm.group('Tlab')
                    gstart = int(sortedofs[0])
                    gend = int(sortedofs[-1])

                elif m.group('Tid'):
                    tid = m.group('Tid')
                    tlab = m.group('Tlab')
                    gstart = int(m.group('Tos'))
                    gend = int(m.group('Toe'))

                for i, count in enumerate(list_char_counts):
                    if count > gstart:
                        break

                if i == 0:
                    lstart = gstart
                    lend = gend
                else:
                    lstart = gstart - list_char_counts[i-1]
                    lend = gend - list_char_counts[i-1]

                # checar diferencia
                char_needed = lend - lstart
                txt_todict = tlines[i][lstart:lend]
                k = 0
                while (len(txt_todict) < char_needed):
                    diff_char = char_needed - len(txt_todict)
                    txt_todict += tlines[i+k+1][0:diff_char]
                    k += 1

                entities_dict[tid] = {'line': i+1,
                                      'type': tlab,
                                      'offset': (lstart, lend),
                                      'text': txt_todict.encode('UTF-8')}

                # print tid, entities_dict[tid]

            # in R annotation
            elif m and m.group('Rid'):
                a1_line = entities_dict[m.group('Ra1')]['line']
                a1_offset = entities_dict[m.group('Ra1')]['offset']
                a1_text = entities_dict[m.group('Ra1')]['text']
                a2_line = entities_dict[m.group('Ra2')]['line']
                a2_offset = entities_dict[m.group('Ra2')]['offset']
                a2_text = entities_dict[m.group('Ra2')]['text']
                entities_dict[m.group('Rid')] = {'line': a1_line,
                                                 'type': m.group('Rlab'),
                                                 'arg1_offset': a1_offset,
                                                 'arg1_text': a1_text,
                                                 'arg1_line': a1_line,
                                                 'arg2_line': a2_line,
                                                 'arg2_offset': a2_offset,
                                                 'arg2_text': a2_text}

        searchable_dict = dict()
        invert_arbitrary(searchable_dict, entities_dict)
        # we can search by line
        out_format = list()
        for i, line in enumerate(tlines):
            out_format.append([i + 1, line, list()])
            if i + 1 in searchable_dict:
                for ann in searchable_dict[i + 1]:
                    if ann[1] == 'line':
                        whole_ann = entities_dict[ann[0]]
                        del whole_ann['line']
                        out_format[i][2].append(whole_ann)
        # with open(output, 'wb') as f:
        #     simplejson.dump(out_format, f, indent=2)
        with open(output, 'wb') as f:
            simplejson.dump(out_format,
                            f,
                            encoding='utf-8',
                            indent=2)
