'''
Created on Apr 22, 2015

@author: Daniel
'''
import subprocess

from txtmining.base.util import DEFAULT_ENCODING


class MetadataExtractor(object):
    '''
    classdocs
    '''

    def __init__(self, bin_path):
        '''
        Constructor
        '''
        self._bin = bin_path
        self._call = None

    def get_metadata(self, path):

        try:
            call_out = subprocess.check_output(self._call,
                                               shell=True,
                                               stderr=subprocess.STDOUT)
            info = call_out.decode(DEFAULT_ENCODING,
                                   errors='ignore')
            info_list = info.splitlines()
            return self._get_info_as_dict(info_list)

        except Exception:
            return None

    def _get_info_as_dict(self, info_list):
        info_dict = {}
        for info in info_list:
            if ":" in info:
                info_elements = info.split(": ")
                key = info_elements[0].strip().replace("'", "")
                val = info_elements[1].strip().replace("'", "")
                info_dict.setdefault(key, val)

        return info_dict
