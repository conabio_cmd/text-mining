from __future__ import print_function, absolute_import
from txtmining.document.gn_caller import GlobalNamesCelery
import mycelery

SEGMENTED_FILE = 'my_segmented_file.txt'

gn_celery = GlobalNamesCelery(SEGMENTED_FILE, celery_conf=mycelery)

print(gn_celery.results.join(propagate=False))
