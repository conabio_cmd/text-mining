"""
Created on Dec 9, 2015.

@author: Alejandro Molina
"""


import os
import sys
import simplejson
import codecs
from txtmining.mlearning.ml_extract import (BratExtract)


if __name__ == '__main__':

    usage = """
 Usage: extract.py dir/txt/ dir/ann/ dir/annotations

 Files in directories must differ only in extension:

 dir/txt/a.txt, dir/txt/b.txt, ...
 dir/ann/a.ann, dir/ann/b.ann, ...

 """

    if len(sys.argv) < 4:
        print(usage)
        sys.exit(1)

    txtdir = sys.argv[1]
    anndir = sys.argv[2]
    outdir = sys.argv[3]
    brat_extractor = BratExtract()

    if not (os.path.isdir(txtdir) and
            os.path.isdir(anndir)):
        print("Invalid directory in arguments.")
        sys.exit(0)

    returned_list = []
    for afile in os.listdir(txtdir):
        if afile.endswith(".txt"):

            basename = afile.rsplit('.', 1)[0]

            # raw text
            txt_file = os.path.join(txtdir, basename+".txt")
            print 'txt_file', txt_file
            # annotation file
            ann_file = os.path.join(anndir, basename+".ann")
            print 'ann_file', ann_file
            # out
            out_file = os.path.join(outdir, basename+".JSON")
            print 'out_file', out_file

            if not (os.path.exists(ann_file)):
                print("Annotation file not found:", ann_file)
                sys.exit(0)

            try:
                returned_list = brat_extractor.get_sample(txt_file,
                                                          ann_file,
                                                          out_file)
            except Exception, e:
                print e
                continue
