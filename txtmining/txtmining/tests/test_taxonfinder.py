'''
Created on Sep 29, 2015

@author: Daniel
'''
import unittest
from txtmining.document.taxonfinder import Taxonfinder
from txtmining.tests.util import get_test_data_folder
import os
import codecs
from txtmining.base.util import DEFAULT_ENCODING


class Test(unittest.TestCase):

    def setUp(self):
        self.test_file = get_test_data_folder()
        self.test_file = os.path.join(self.test_file,
                                      'mini.txt')
        self.taxonfinder = Taxonfinder()

    def tearDown(self):
        pass

    def test_taxon_finder(self):
        taxons_found = self.taxonfinder.get_index(self.test_file)
        self.assertIsInstance(taxons_found, list)
        with codecs.open(self.test_file, 'r',
                         DEFAULT_ENCODING) as f:
            original_content = f.readlines()
        for (line, name_complete, canonical, (start, end)) in taxons_found:
            name = (name_complete.split(" ")[-1].lower().
                    encode(DEFAULT_ENCODING))
            name_text = original_content[line - 1][start:end].lower()
            name_text = name_text.split(" ")
            self.assertTrue(name in name_text, u"{0} not similar to {1}".
                            format(name_complete, ' '.join(name_text)))


if __name__ == "__main__":
    unittest.main()
