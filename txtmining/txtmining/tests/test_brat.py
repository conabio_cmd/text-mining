"""
Created on October 14, 2015.

@author: Alejandro Molina.
"""


import unittest
import os
import codecs
import nltk
import re

from txtmining.converters.brat import BratConv
from txtmining.tests.util import get_test_data_folder
from txtmining.controllers.controllers import TaxonfinderController
from txtmining.document.nlp import NLPpreprocessor


class BratTest(unittest.TestCase):

    def setUp(self):

        # text file
        self.txt_file = os.path.join(get_test_data_folder(), 'brat',
                                     'human.txt')

        # annotations file
        self.ann_file = os.path.join(get_test_data_folder(), 'brat',
                                     'human.ann')

        # get scientific names
        tf_controller = TaxonfinderController()
        self.tf_names = tf_controller.apply(self.txt_file)

        # get brat dictionary
        self.ann = BratConv.ann_format(self.txt_file,
                                       self.tf_names,
                                       label='Nombre_cientifico')

        # convert into brat string
        self.brat_str = BratConv.dict2ann(self.ann)
        self.lines = self.brat_str.splitlines(True)

        # get type of match objects
        self.SRE_MATCH_TYPE = type(re.match("", ""))

        # allow to compare long strings
        self.maxDiff = None
        nltk.download('punkt')

    def test_brat_ann_format(self):
        for i in xrange(len(self.ann)):
            current_ann = self.ann.get('T{0}'.format(i + 1))
            current_tf_names = self.tf_names[i]
            self.assertEqual(current_ann.get('texts')[0],
                             current_tf_names[1])

    def test_brat2classes(self):
        NLPpre = NLPpreprocessor('spanish')
        contents = codecs.open(self.txt_file, 'r', 'UTF-8').read()
        expected_tokens = NLPpre.offset_tokenize(contents)
        toks_with_classes, rels = BratConv.brat2classes(self.txt_file,
                                                        self.ann_file)
        for i in xrange(len(expected_tokens)):
            (t_k, k) = toks_with_classes[i]
            (t, offset) = expected_tokens[i]
            self.assertEqual(t, t_k)

    def test_dict2ann(self):
        # check consistency of length
        num_lines = len(self.lines)
        num_items = len(self.ann)
        self.assertEqual(num_items, num_lines)
        # check consistency of each line to be any of ent or rel
        ent_pattern = r'T(\d+)\t(\w+)\s(\d+)\s(\d+)\t(.*)'
        rel_pattern = r'R(\d+)\t(\w+)\sArg1:T(\d+)\sArg2:T(\d+)(.*)'
        for l in self.lines:
            match = None
            if l.startswith('T'):
                match = re.match(ent_pattern, l)
                self.assertTrue(isinstance(match, self.SRE_MATCH_TYPE))
            elif l.startswith('R'):
                match = re.match(rel_pattern, l)
                self.assertTrue(isinstance(match, self.SRE_MATCH_TYPE))
            else:
                self.assertTrue(isinstance(match, self.SRE_MATCH_TYPE))

    def test_ann2csv(self):
        csvstring = BratConv.ann2csv(self.txt_file,
                                     self.ann_file,
                                     context=50,
                                     taxlevel=False)
        self.assertTrue(isinstance(csvstring, basestring))

    def test_ann2dict(self):
        brat_dict = BratConv.ann2dict(self.ann_file)
        self.assertTrue(isinstance(brat_dict, dict))


if __name__ == '__main__':
    unittest.main()
