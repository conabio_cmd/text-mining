(?P<medicina_tradicional_aparato_circulatorio>
\bAdelgazamiento de la sangre\b|\b
Promover la circulación\b|\b
Engrosar la sangre\b|\b
Aumentar la sangre\b|\b
Ataque al corazón\b|\b
Presión baja\b|\b
Presión alta\b|\b
Hemorroides\b|\b
Hemorragias\b|\b
Várices\b|\b
Gangrena\b|\b
Corazón\b|\b
Infarto\b|\b
Embolia\b|\b
Sangre\b|\b
Anemia\b
)|
(?P<medicina_tradicional_aparato_digestivo>
\bHinchazón de arriba de la ingle\b|\b
Tapiados de la guía de atrás\b|\b
Pulsaciones en el estómago\b|\b
Dolor en boca del estómago\b|\b
Cólicos del recién nacido\b|\b
Cólico de tripa retorcida\b|\b
Constipación de vientre\b|\b
Acabamiento de estómago\b|\b
Piedras en la vesícula\b|\b
Resfrío del estómago\b|\b
Fortalecer los dientes\b|\b
Enfermedad de la costa\b|\b
Disentería con sangre\b|\b
Deposiciones de sangre\b|\b
Cólico del intestino\b|\b
Cólico con calentura\b|\b
Brinco del histérico\b|\b
Aflojada de dentadura\b|\b
Disentería colorada\b|\b
Disentería caliente\b|\b
Dientes destemplados\b|\b
Cesido del estómago\b|\b
Sucio del estómago\b|\b
Subida de estómago\b|\b
Intestino recargado\b|\b
Flojedad de dientes\b|\b
Curtir el estómago\b|\b
Chorrillo de sangre\b|\b
Tapiados del caño\b|\b
Tapiados de atrás\b|\b
Encías inflamadas\b|\b
Dolor de estómago\b|\b
Dolor de apéndice\b|\b
Diarrea con sangre\b|\b
Cámaras de sangre\b|\b
Brinco del catrín\b|\b
dientes apretados\b|\b
Dolor enfilítico\b|\b
Pujos con sangre\b|\b
Gusano de muelas\b|\b
Dolor de barriga\b|\b
Disentería roja\b|\b
Dientes podridos\b|\b
Derrame de bilis\b|\b
Cursos colorados\b|\b
Amacizar dientes\b|\b
dentadura floja\b|\b
Tripa retorcida\b|\b
Regada de bilis\b|\b
Pujos colorados\b|\b
Muelas cariadas\b|\b
Dolor de muelas\b|\b
Bilis derramada\b|\b
Ataque de bilis\b|\b
Apretar dientes\b|\b
Flux de sangre\b|\b
Estreñimiento\b|\b
Dolor de ijada\b|\b
Dientes flojos\b|\b
Pasmo del ano\b|\b
Mal de tapón\b|\b
Dolor de ijar\b|\b
Pulso regado\b|\b
Indigestión\b|\b
Embaramiento\b|\b
Deposiciones\b|\b
Sentimiento\b|\b
Mal de pujo\b|\b
Mal de ijar\b|\b
Estomatitis\b|\b
Entapiadura\b|\b
Disentería\b|\b
Corrimiento\b|\b
Congestión\b|\b
Apendicitis\b|\b
Pulsación\b|\b
Gingivitis\b|\b
Dentición\b|\b
Acihuatado\b|\b
Vesícula\b|\b
Tapiadura\b|\b
Opiliados\b|\b
Gastritis\b|\b
Estérico\b|\b
Ciguatera\b|\b
Chorrillo\b|\b
Boquillas\b|\b
Atrancado\b|\b
Amibiasis\b|\b
Úlceras\b|\b
Traspaso\b|\b
Salidera\b|\b
Opilados\b|\b
Ofiadura\b|\b
Miserere\b|\b
Culería\b|\b
Ardencia\b|\b
Agrieras\b|\b
cólico\b|\b
Tumores\b|\b
Torzón\b|\b
Hígado\b|\b
Empacho\b|\b
Dientes\b|\b
Diarrea\b|\b
Cólico\b|\b
Cursera\b|\b
Ciguato\b|\b
Chimeca\b|\b
Boquera\b|\b
Atorado\b|\b
Agruras\b|\b
Acedias\b|\b
Tapado\b|\b
Muelas\b|\b
Latido\b|\b
Cursus\b|\b
Chorro\b|\b
Caries\b|\b
Brinco\b|\b
Amibas\b|\b
Acidez\b|\b
Curso\b|\b
Cirro\b|\b
Bilis\b|\b
Pujo\b
)|
(?P<medicina_tradicional_aparato_reproductor>
\bMujeres que no se pueden embarazar\b|\b
Mujer que no se pueden embarazar\b|\b
Impotencia sexual masculina\b|\b
Sofocación del embarazo\b|\b
Recuperación post-parto\b|\b
Desviación de la matriz\b|\b
Frialdad de los ovarios\b|\b
Tumores en los pechos\b|\b
Problemas en el parto\b|\b
Frialdad de la matriz\b|\b
Recaída de la mujer\b|\b
Secreción de leche\b|\b
Bolas en los pechos\b|\b
Mujer desarreglada\b|\b
Apresurar el parto\b|\b
Impotencia sexual\b|\b
Bajar testículos\b|\b
Bajada de ovarios\b|\b
Acelerar el parto\b|\b
Descarrilamiento\b|\b
Debilidad sexual\b|\b
Caída de matriz\b|\b
Óvulos escasos\b|\b
Ovarios caídos\b|\b
Matriz volteada\b|\b
Pasmo de mujer\b|\b
Mal de matriz\b|\b
Contrariedad\b|\b
Esterilidad\b|\b
Impotencia\b|\b
Cama chica\b|\b
Trastorno\b|\b
Mala cama\b|\b
Mal parto\b|\b
Entuertos\b|\b
Orquitis\b|\b
Tumores\b|\b
Aborto\b|\b
Brote\b
)|
(?P<medicina_tradicional_aparato_respiratorio>
\bInflamación de las anginas\b|\b
Tos con hervidero de pecho\b|\b
Apretamiento de la pechera\b|\b
Tos que rasga la garganta\b|\b
Debilidad de los pulmones\b|\b
Delgado de los pulmones\b|\b
Tos de recién nacido\b|\b
Tos con alcanzamiento\b|\b
Enfriamiento de pecho\b|\b
Sofocación de pecho\b|\b
Cansancio de aliento\b|\b
Resfriado del pecho\b|\b
Madurar el catarro\b|\b
Catarro constipado\b|\b
Cansancio de pecho\b|\b
Tapazón de pecho\b|\b
Picado de pulmón\b|\b
Picado de espalda\b|\b
Dolor de garganta\b|\b
Catarro coagulado\b|\b
Tos de cansancio\b|\b
Pasmo de pulmón\b|\b
Dolor de costado\b|\b
Catarro detenido\b|\b
Catarro al pecho\b|\b
Picado de pecho\b|\b
Pasmo de la tos\b|\b
Hervor de pecho\b|\b
Falta de viento\b|\b
Catarro cuajado\b|\b
Basca de sangre\b|\b
Hilar de pecho\b|\b
Dolor de pecho\b|\b
Tos de niños\b|\b
Tos ahogadora\b|\b
Mal delgadito\b|\b
Tuberculosis\b|\b
Tos de ahogo\b|\b
Mal de pecho\b|\b
Mal de mina\b|\b
Encanijados\b|\b
Consunción\b|\b
Tos fuerte\b|\b
Pechuguera\b|\b
Bronquitis\b|\b
Tosferina\b|\b
Pulmonía\b|\b
Ogurridos\b|\b
Engasados\b|\b
Delgadito\b|\b
Bronquios\b|\b
Tosiento\b|\b
Tos seca\b|\b
Ronquera\b|\b
Resuello\b|\b
Resfrío\b|\b
Cascarse\b|\b
Cascados\b|\b
Ahoguío\b|\b
Tísico\b|\b
Tumores\b|\b
Tiricia\b|\b
Pulmón\b|\b
Oguillo\b|\b
Hoguío\b|\b
Guillos\b|\b
Catarro\b|\b
Boquear\b|\b
Anginas\b|\b
Acecido\b|\b
Fatiga\b|\b
Tisis\b|\b
Oguio\b|\b
Jique\b|\b
Gripa\b|\b
Ahogo\b|\b
Uña\b|\b
Ogio\b|\b
Asma\b|\b
Tos\b|\b
Tis\b
)|
(?P<medicina_tradicional_aparato_tegumentario>
\bEscoriaciones de la boca\b|\b
Enfermedad de la sangre\b|\b
Crecimiento del cabello\b|\b
Irritación de la piel\b|\b
Úlceras de la boca\b|\b
Mal de San Lázaro\b|\b
Mal de San Antonio\b|\b
Lunares abultados\b|\b
Granos enterrados\b|\b
Granos de la cara\b|\b
Hervor de sangre\b|\b
Manchas blancas\b|\b
Fiebre escamosa\b|\b
Exceso de grasa\b|\b
Vejigas orales\b|\b
Lepra mexicana\b|\b
Caída de pelo\b|\b
Mal encerrado\b|\b
Mal del pinto\b|\b
Inflamación\b|\b
Gusto cubano\b|\b
Bubón magil\b|\b
Melancolía\b|\b
Mala angina\b|\b
Magulladura\b|\b
Hidropesía\b|\b
Fiebre roja\b|\b
Estomatitis\b|\b
Escarlatina\b|\b
Escaldadura\b|\b
Alfombrilla\b|\b
Salpullido\b|\b
Pocholacas\b|\b
Jiricuento\b|\b
Hinchazón\b|\b
Espinillas\b|\b
Erupciones\b|\b
Doble pelo\b|\b
Culebrilla\b|\b
Urticaria\b|\b
Tina poti\b|\b
Tacotillo\b|\b
Sabañón\b|\b
Rozaduras\b|\b
Rasquiña\b|\b
Mezquinos\b|\b
Manchados\b|\b
Mal grano\b|\b
Erisipela\b|\b
Enguixado\b|\b
Clavillos\b|\b
Úlceras\b|\b
Verrugas\b|\b
Sabayón\b|\b
Regocijo\b|\b
Pitiflor\b|\b
Ortigada\b|\b
Mimiques\b|\b
Incordio\b|\b
Estrías\b|\b
Escupida\b|\b
Empeines\b|\b
Disipela\b|\b
Chincual\b|\b
Carbunco\b|\b
Calvicie\b|\b
Alopecia\b|\b
Aljorran\b|\b
Tlacote\b|\b
Ronchas\b|\b
Pelagra\b|\b
Orzuela\b|\b
Naguana\b|\b
Nacidos\b|\b
Manchas\b|\b
Jiricua\b|\b
Heridas\b|\b
Gabarro\b|\b
Escamas\b|\b
Divieso\b|\b
Cáncer\b|\b
Cubemba\b|\b
Aljorra\b|\b
Alforra\b|\b
Absceso\b|\b
Uñero\b|\b
Piojos\b|\b
Pavón\b|\b
Memios\b|\b
Llagas\b|\b
Lamida\b|\b
Jiotes\b|\b
Granos\b|\b
Golpes\b|\b
Bubón\b|\b
Barros\b|\b
Tiña\b|\b
Secas\b|\b
Sarna\b|\b
Roña\b|\b
Potro\b|\b
Pinto\b|\b
Paño\b|\b
Lepra\b|\b
Caspa\b|\b
Carey\b|\b
Aftas\b
)|
(?P<medicina_tradicional_envenenamiento_o_intoxicacion>
\bPicadura de araña de reloj de arena\b|\b
Mordedura de lagarto de cuentas\b|\b
Mordedura de monstruo de Gila\b|\b
Envenenamiento por fármacos\b|\b
Envenenamiento por alimentos\b|\b
Picadura de araña capulina\b|\b
Intoxicación por fármacos\b|\b
Intoxicación por alimentos\b|\b
Envenenamiento por plantas\b|\b
Intoxicación por plantas\b|\b
Envenenamiento por hongos\b|\b
Picadura de hormiga roja\b|\b
Intoxicación por hongos\b|\b
Picadura de viuda negra\b|\b
Mordedura de escorpión\b|\b
Picadura de tarántula\b|\b
Mordedura de serpiente\b|\b
Picadura de ciempiés\b|\b
Picadura de alacrán\b|\b
Mordedura de hormiga\b|\b
Picadura de víbora\b|\b
Mordedura de araña\b|\b
Picadura de avispa\b|\b
Picadura de mosco\b|\b
Picadura de abeja\b|\b
Picadura de raya\b|\b
Envenenamiento\b|\b
Intoxicación\b|\b
Mordedura\b|\b
Picadura\b
)|
(?P<medicina_tradicional_infecciones_y_parasitos>
\bInfección de vías respiratorias\b|\b
Escoriaciones de la boca\b|\b
Enfermedad de la sangre\b|\b
Delgado de los pulmones\b|\b
Infección del riñón\b|\b
Enfermedad de la costa\b|\b
Disentería con sangre\b|\b
Deposiciones de sangre\b|\b
Animales en las tripas\b|\b
Infección intestinal\b|\b
Infección en la piel\b|\b
Picado de la espalda\b|\b
Infección de oídos\b|\b
Infección de matriz\b|\b
Disentería colorada\b|\b
Disentería caliente\b|\b
Úlceras de la boca\b|\b
Mal de algodoncillo\b|\b
Chorrillo de sangre\b|\b
Mal de San Lázaro\b|\b
Mal de San Antonio\b|\b
Infección vaginal\b|\b
Diarrea con sangre\b|\b
Cámaras de sangre\b|\b
Picado de pulmón\b|\b
Pujos con sangre\b|\b
Pasmo de pulmón\b|\b
Hervor de sangre\b|\b
Dolor de costado\b|\b
Disentería roja\b|\b
Cursos colorados\b|\b
Catarro al pecho\b|\b
Pujos colorados\b|\b
Picado de pecho\b|\b
Fiebre escamosa\b|\b
Basca de sangre\b|\b
Vejigas orales\b|\b
Lepra mexicana\b|\b
Flux de sangre\b|\b
Flujos blancos\b|\b
Flujo de frío\b|\b
Tos ahogadora\b|\b
Pasmo del ano\b|\b
Moco de frío\b|\b
Mal encerrado\b|\b
Mal delgadito\b|\b
Mal del pinto\b|\b
Mal de conejo\b|\b
Mal de brinco\b|\b
Ardor de pies\b|\b
Tuberculosis\b|\b
Mal de pecho\b|\b
Gusto cubano\b|\b
Algodoncillo\b|\b
Tlalzaguate\b|\b
Mala angina\b|\b
Mal de pujo\b|\b
Mal de mina\b|\b
Mal de arco\b|\b
Infecciones\b|\b
Fiebre roja\b|\b
Estomatitis\b|\b
Escarlatina\b|\b
Encanijados\b|\b
Disentería\b|\b
Desarreglos\b|\b
Consunción\b|\b
Chinguiñas\b|\b
Alfombrilla\b|\b
Sarampión\b|\b
Pasmo seco\b|\b
Parásitos\b|\b
Jiricuento\b|\b
Infección\b|\b
Garrapatas\b|\b
Tosferina\b|\b
Tina poti\b|\b
Solitaria\b|\b
Rasquiña\b|\b
Pulmonía\b|\b
Pinolillo\b|\b
Paludismo\b|\b
Ogurridos\b|\b
Mocezuelo\b|\b
Mezquinos\b|\b
Manchados\b|\b
Lombrices\b|\b
Hermosura\b|\b
Hepatitis\b|\b
Escorbuto\b|\b
Erisipela\b|\b
Engasados\b|\b
Delgadito\b|\b
Boquillas\b|\b
Varicela\b|\b
Tétanos\b|\b
Tifoidea\b|\b
Sífilis\b|\b
Resfrío\b|\b
Pitiflor\b|\b
Paperón\b|\b
Orejones\b|\b
Mimiques\b|\b
Legañas\b|\b
Lagañas\b|\b
Ladillas\b|\b
Gonorrea\b|\b
Coquetas\b|\b
Chinches\b|\b
Cascarse\b|\b
Cascados\b|\b
Carbunco\b|\b
Ácaros\b|\b
Viruela\b|\b
Tísico\b|\b
Tiricia\b|\b
Paperas\b|\b
Naguana\b|\b
Jiricua\b|\b
Gusanos\b|\b
Guillos\b|\b
Gabarro\b|\b
Cólera\b|\b
Chimeca\b|\b
Catarro\b|\b
Boquera\b|\b
Aljorra\b|\b
Absceso\b|\b
Piojos\b|\b
Pavón\b|\b
Memios\b|\b
Herpes\b|\b
Fuegos\b|\b
Chanza\b|\b
Bichos\b|\b
Amibas\b|\b
Tiña\b|\b
Tisis\b|\b
Sarna\b|\b
Roña\b|\b
Rabia\b|\b
Pinto\b|\b
Nigua\b|\b
Lepra\b|\b
Jique\b|\b
Gripa\b|\b
Carey\b|\b
Aftas\b|\b
Uña\b|\b
Tifo\b|\b
Sida\b|\b
Tis\b)|
(?P<medicina_tradicional_signo_y_sintoma>
\bDolor de estómago de bebés\b|\b
Inflamación de las anginas\b|\b
Tos con hervidero de pecho\b|\b
Sofocación del estómago\b|\b
Enfermedad del estómago\b|\b
Inflamación de la cara\b|\b
Descoyuntado del cuerpo\b|\b
Tos con alcanzamiento\b|\b
Sofocación del pecho\b|\b
Enfriamiento de pecho\b|\b
Calor en el estómago\b|\b
Recaída de señoras\b|\b
Recaída de la mujer\b|\b
Alferecía de niños\b|\b
Abierto de la cabeza\b|\b
Sucio del estómago\b|\b
Retención de orina\b|\b
Resfriado del pecho\b|\b
Punzada en el oído\b|\b
Pesadez del cerebro\b|\b
Falseada de cintura\b|\b
Debilidad de niños\b|\b
Aflojado de cintura\b|\b
Recaída de niños\b|\b
Dolor de estómago\b|\b
Cólicos de niños\b|\b
Tapazón de pecho\b|\b
Retraso menstrual\b|\b
Dolor de pulmones\b|\b
Dolor de garganta\b|\b
Dolor de corazón\b|\b
Cerrada de orines\b|\b
Ardores de cuerpo\b|\b
Ardor de garganta\b|\b
Tos de cansancio\b|\b
Hemorragia nasal\b|\b
Falta de apetito\b|\b
Dolor reumático\b|\b
Dolor que sofoca\b|\b
Dolor de riñón\b|\b
Dolor de ovarios\b|\b
Dolor de espalda\b|\b
Dolor de encías\b|\b
Dolor de costado\b|\b
Dolor de cintura\b|\b
Dolor de caballo\b|\b
Disminuir saliva\b|\b
Catarro al pecho\b|\b
Aire de arcoiris\b|\b
Tos tuberculosa\b|\b
Tapado de orín\b|\b
Rodillas flojas\b|\b
Pasmo de la tos\b|\b
Manchas blancas\b|\b
Hervor de pecho\b|\b
Dolores de aire\b|\b
Dolor por frío\b|\b
Dolor de vejiga\b|\b
Dolor de oídos\b|\b
Dolor de muelas\b|\b
Dolor de matriz\b|\b
Dolor de huesos\b|\b
Dolor de diente\b|\b
Dolor de cuerpo\b|\b
Dolor de cabeza\b|\b
Dolor articular\b|\b
Dolor al orinar\b|\b
Calor de niños\b|\b
Vista venteada\b|\b
Un sentimiento\b|\b
Hilar de pecho\b|\b
Estreñimiento\b|\b
Dolor muscular\b|\b
Dolor de pecho\b|\b
Dolor de parto\b|\b
Dolor de brazo\b|\b
Cuerpo cortado\b|\b
Cadera abierta\b|\b
Boca desabrida\b|\b
Aire de basura\b|\b
Adormecimiento\b|\b
Vista nublada\b|\b
Tos ahogadora\b|\b
Mal de oriño\b|\b
Dolor de piel\b|\b
Dolor de ojos\b|\b
Dolor de ijar\b|\b
Dolor de aire\b|\b
Aire de noche\b|\b
Aire de cueva\b|\b
Mal de orín\b|\b
Mal de orina\b|\b
Inflamación\b|\b
Flatulencias\b|\b
Envaramiento\b|\b
Empacho seco\b|\b
Deposiciones\b|\b
Calor subido\b|\b
Cadera floja\b|\b
Aire maligno\b|\b
Tos pasmada\b|\b
Sudoración\b|\b
Salivación\b|\b
Mal de boca\b|\b
Mal aliento\b|\b
Hidropesía\b|\b
Entapiadura\b|\b
Descaderado\b|\b
Corrimiento\b|\b
Congestión\b|\b
Chinguiñas\b|\b
Vergüenza\b|\b
Pechuguera\b|\b
Mal viento\b|\b
Hinchazón\b|\b
Hemorragia\b|\b
Erupciones\b|\b
Depresión\b|\b
Bronquitis\b|\b
Alferecía\b|\b
Traspurga\b|\b
Temblores\b|\b
Tapiadura\b|\b
Moretones\b|\b
Entuertos\b|\b
Enculados\b|\b
Debilidad\b|\b
Cansancio\b|\b
Calentura\b|\b
Aire malo\b|\b
mal aire\b|\b
Vértigo\b|\b
Venteada\b|\b
Tristeza\b|\b
Tosiento\b|\b
Tos seca\b|\b
Tapeados\b|\b
Salidera\b|\b
Ronquera\b|\b
Resuello\b|\b
Recaída\b|\b
Picazón\b|\b
Ofiadura\b|\b
Naúseas\b|\b
Mal aire\b|\b
Legañas\b|\b
Lagañas\b|\b
Insomnio\b|\b
Frialdad\b|\b
Cólicos\b|\b
Culería\b|\b
Angurria\b|\b
Aguajque\b|\b
Vómito\b|\b
Riñón\b|\b
Pulmón\b|\b
Oguillo\b|\b
Diarrea\b|\b
Desmayo\b|\b
Cursera\b|\b
Boquear\b|\b
Alergia\b|\b
Aireada\b|\b
Absceso\b|\b
Viento\b|\b
Vahido\b|\b
Sueño\b|\b
Jiotes\b|\b
Hoguio\b|\b
Fiebre\b|\b
Cursus\b|\b
Chorro\b|\b
Acidez\b|\b
Pujos\b|\b
Pasmo\b|\b
Oguio\b|\b
Mareo\b|\b
Dolor\b|\b
Curso\b|\b
Cruda\b|\b
Calor\b|\b
Aigre\b|\b
Ahogo\b|\b
Yane\b|\b
Ogio\b|\b
Asco\b|\b
Aire\b|\b
Tos\b)|
(?P<medicina_tradicional_sindrome_de_filiacion_cultural>
\bRemolino de los muertos\b|\b
Caída de la campanilla\b|\b
Calentamiento de cabeza\b|\b
Pérdida del espíritu\b|\b
Quebrada de espíritu\b|\b
Pérdida de la sombra\b|\b
Chupetones de vampiro\b|\b
Separación del alma\b|\b
Quebrada en el niño\b|\b
Pérdida del tonalli\b|\b
Alferecía de niños\b|\b
Espanto de chaneque\b|\b
Enfermedad regalada\b|\b
Susto sobre pecado\b|\b
Males de brujería\b|\b
Espanto por pecado\b|\b
Espanto de culebra\b|\b
Caída de varillas\b|\b
Sentado de muerto\b|\b
Pérdida del alma\b|\b
Piquete de piedra\b|\b
Espíritu quedado\b|\b
Espanto de sueño\b|\b
Espanto de muerto\b|\b
Espanto de caída\b|\b
Cáncer de muerto\b|\b
Corazón apretado\b|\b
Caída de mollera\b|\b
Aires mal tirados\b|\b
Vista penetrante\b|\b
Ojo de cristiano\b|\b
Malas corrientes\b|\b
Hético de susto\b|\b
Espanto de muina\b|\b
Espanto de fuego\b|\b
Espanto de cueva\b|\b
Corazón cerrado\b|\b
Chupada de bruja\b|\b
Carácter fuerte\b|\b
Cargar el muerto\b|\b
Aire de arcoiris\b|\b
Agitar la sangre\b|\b
Ojo de borracho\b|\b
Espanto de río\b|\b
Espanto de agua\b|\b
Enfermedad mala\b|\b
Eclipse de luna\b|\b
Aire de difunto\b|\b
Vista candente\b|\b
Vista caliente\b|\b
Paladar caído\b|\b
Ojo de envidia\b|\b
Mollera caída\b|\b
Eclipse de sol\b|\b
Desbarbillarse\b|\b
Aire de muerto\b|\b
Aire de basura\b|\b
Susto de aire\b|\b
Quitar sueño\b|\b
Mirada fuerte\b|\b
Mal de piedra\b|\b
Herida de ojo\b|\b
Encantamiento\b|\b
Desvarillados\b|\b
Aire de noche\b|\b
Aire de cueva\b|\b
Trabajo malo\b|\b
Ojos fuertes\b|\b
Mal de gente\b|\b
Espanto seco\b|\b
Entoloachado\b|\b
Buena suerte\b|\b
Aire maligno\b|\b
Viento loco\b|\b
Sentimiento\b|\b
Pega triste\b|\b
Padre-madre\b|\b
Hechicería\b|\b
Casihuistle\b|\b
Aire fuerte\b|\b
Vergüenza\b|\b
Pocholacas\b|\b
Mal viento\b|\b
Mal puesto\b|\b
Mal echado\b|\b
Mal de ojo\b|\b
Alferecía\b|\b
Protector\b|\b
Maleficio\b|\b
Mal humor\b|\b
Engentado\b|\b
Enculados\b|\b
Chipiloso\b|\b
Brujería\b|\b
Aire malo\b|\b
Tristeza\b|\b
Ojeadura\b|\b
Mala fé\b|\b
Mal aire\b|\b
Frialdad\b|\b
Escupida\b|\b
Atontado\b|\b
Aljorran\b|\b
Aireadas\b|\b
Tiricia\b|\b
Quemada\b|\b
Lujuria\b|\b
Hechizo\b|\b
Espanto\b|\b
Envidia\b|\b
Empacho\b|\b
Embrujo\b|\b
Eclipse\b|\b
Corajes\b|\b
Chípil\b|\b
Aljorra\b|\b
Alforra\b|\b
Viento\b|\b
Trueno\b|\b
Ojeada\b|\b
Maldad\b|\b
Locura\b|\b
Enfado\b|\b
Eclís\b|\b
Chizos\b|\b
Antojo\b|\b
Susto\b|\b
Muina\b|\b
Enojo\b|\b
Daño\b|\b
Ojeo\b|\b
Aire\b|\b
Ira\b
)|
(?P<medicina_tradicional_sistema_endocrino_nutricion_y_metabolismo>
\bIrritación de la sangre\b|\b
Azúcar en la sangre\b|\b
Secreción de leche\b|\b
Azúcar en la orina\b|\b
Güegüecho\b|\b
Orejones\b|\b
Ofiadura\b|\b
Diabetes\b|\b
Tumores\b|\b
Tiricia\b|\b
Paperas\b|\b
Anemia\b|\b
Buche\b|\b
Bocio\b|\b
Betis\b|\b
Gota\b)|
(?P<medicina_tradicional_sistema_genitourinario>
\bRegular la menstruación\b|\b
Michicaues de la mujer\b|\b
Menstruación atrasada\b|\b
Retención de orina\b|\b
Próstata inflamada\b|\b
Testículo montado\b|\b
Cerrada de orines\b|\b
Tapado de orín\b|\b
Huevos caídos\b|\b
Flujos blancos\b|\b
Mal de piedra\b|\b
Mal de orín\b|\b
Entapiadura\b|\b
Dismenorrea\b|\b
Menorragia\b|\b
Tapiadura\b|\b
Tapeadura\b|\b
Riñones\b|\b
Relajado\b|\b
Angurria\b|\b
Tumores\b|\b
Tapeado\b|\b
Quistes\b)|
(?P<medicina_tradicional_sistema_musculo_esqueletico>
\bEnfriamiento de los huesos\b|\b
Descoyuntado del cuerpo\b|\b
Caída de la campanilla\b|\b
Abierto de la cadera\b|\b
Falseada de cintura\b|\b
Aflojado de cintura\b|\b
Caída de varillas\b|\b
Caminen los niños\b|\b
Caída de mollera\b|\b
Rodillas flojas\b|\b
Ombligo salido\b|\b
Mollera caída\b|\b
Frío muscular\b|\b
Entumecimiento\b|\b
Dolor muscular\b|\b
Desbarbillarse\b|\b
Cadera abierta\b|\b
Cabeza deforme\b|\b
Desvarillados\b|\b
Envaramiento\b|\b
Entoloachado\b|\b
Cadera floja\b|\b
Boca de lado\b|\b
Tortícolis\b|\b
Descaderado\b|\b
Reumatismo\b|\b
Encuerdado\b|\b
Enguilado\b|\b
Encordado\b|\b
Cansancio\b|\b
Calambres\b|\b
Venteado\b|\b
Venteada\b|\b
Esguince\b|\b
Ciática\b|\b
Artritis\b|\b
Tumores\b|\b
Tullido\b|\b
Postema\b|\b
Riumas\b|\b
Reumas\b|\b
Hernia\b|\b
Gota\b|\b
Gafo\b)|
(?P<medicina_tradicional_sistema_nervioso_y_organos_de_los_sentidos>
\bCeguera de Montecristo\b|\b
Distraído de un ojo\b|\b
Chillido en el oído\b|\b
Ataques epilépticos\b|\b
Irritación de ojos\b|\b
Fortalecer la vista\b|\b
Carnosidad del ojo\b|\b
Antojo de camarón\b|\b
Zumbido de oído\b|\b
Zumbido de oreja\b|\b
Quitar el sueño\b|\b
Punzada de oído\b|\b
Ceguera del río\b|\b
Aire en los ojos\b|\b
Aire en el oído\b|\b
Manchas de ojos\b|\b
Chichi de perro\b|\b
Mal de bolitas\b|\b
Mal del habla\b|\b
Mal de conejo\b|\b
Mal de brinco\b|\b
El histérico\b|\b
Entoloachado\b|\b
Nube de ojo\b|\b
Nerviosismo\b|\b
Nerviosidad\b|\b
Mal de luna\b|\b
Mal de arco\b|\b
Chinguiñas\b|\b
Pasmo seco\b|\b
Mal morado\b|\b
Estrabismo\b|\b
Encuerdado\b|\b
Mocezuelo\b|\b
Lunático\b|\b
Epilepsia\b|\b
Cataratas\b|\b
Vértigo\b|\b
Tétanos\b|\b
Perrilla\b|\b
Ojo loco\b|\b
Legañas\b|\b
Lagañas\b|\b
Ciática\b|\b
Bizquera\b|\b
Bizcocho\b|\b
Ansiedad\b|\b
Viriolo\b|\b
Sordera\b|\b
Nervios\b|\b
Insulto\b|\b
Ataques\b|\b
Vahido\b|\b
Turnio\b|\b
Virul\b|\b
Nubes\b|\b
Mareo\b|\b
Mudo\b)|
(?P<medicina_tradicional_traumatismo>
\bTorcedura de cuello\b|\b
Hueso dislocado\b|\b
Sacar astillas\b|\b
Sacar espinas\b|\b
Desconchabada\b|\b
Descompostura\b|\b
Lastimaduras\b|\b
Dislocación\b|\b
Descoyontura\b|\b
Traumatismo\b|\b
Volteadura\b|\b
Quebradura\b|\b
Hueso roto\b|\b
Torcedura\b|\b
Mordedura\b|\b
Mallugón\b|\b
Zafadura\b|\b
Rasguño\b|\b
Quemadas\b|\b
Picadura\b|\b
Fractura\b|\b
Falseada\b|\b
Tumores\b|\b
Raspada\b|\b
Cortada\b|\b
Herida\b|\b
Golpe\b)
