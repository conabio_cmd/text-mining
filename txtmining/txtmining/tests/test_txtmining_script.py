"""
Created on Jun 10, 2015.

@author: Daniel
"""
import ConfigParser
import os
from shutil import rmtree, copy
import subprocess
from tempfile import mkdtemp
import unittest

from txtmining.tests.util import get_script_path, \
    get_regex_folder, get_test_data_folder, is_json, \
    get_opennlp_model


class ScriptTest(unittest.TestCase):

    def setUp(self):
        self.script = get_script_path()

    def tearDown(self):
        pass

    def test_FragmentController(self):
        n_lines_before = 4
        n_lines_after = 2
        regex_dir = get_regex_folder()
        txt_file = get_test_data_folder()
        txt_file = os.path.join(txt_file,
                                'segmented/003186.txt')

        call = ['python',
                self.script,
                'FragmentController',
                '--nlines_after',
                str(n_lines_after),
                '--nlines_before',
                str(n_lines_before),
                '--regex_dir',
                regex_dir,
                '--infile',
                txt_file]

        json_output = subprocess.check_output(call)

        self.assertTrue(is_json(json_output))

    def test_ReIndexController(self):

        regexp_file = get_regex_folder()
        regexp_file = os.path.join(regexp_file, 'regexp_NombreCientifico.re')

        txt_file = get_test_data_folder()
        txt_file = os.path.join(txt_file, 'fromOCR/docs__subdirTraditional'
                                '_ecological_knowledge_Solferino_Quintana_Roo'
                                '.pdf.txt')

        call = ['python',
                self.script,
                'ReIndexController',
                '--regexp_file',
                regexp_file,
                '--infile',
                txt_file]

        json_output = subprocess.check_output(call)

        self.assertTrue(is_json(json_output))

    def test_PreprocessingController(self):

        opennlp_bin = os.getenv('OPENNLP_BIN', None)
        self.assertIsNotNone(opennlp_bin, 'You must set OPENNLP_BIN env var')

        opennlp_model = get_opennlp_model()

        txt_file = get_test_data_folder()
        txt_file = os.path.join(txt_file, 'fromOCR/docs__subdirTraditional'
                                '_ecological_knowledge_Solferino_Quintana_Roo'
                                '.pdf.txt')

        call = ['python',
                self.script,
                'PreprocessingController',
                '--opennlp_bin',
                opennlp_bin,
                '--opennlp_mod',
                opennlp_model,
                '--infile',
                txt_file,
                '--correction_type',
                'all']

        str_output = subprocess.check_output(call)

        self.assertIsInstance(str_output,
                              basestring)

    def test_PDFparserController(self):

        tika_bin = os.getenv('TIKA_BIN', None)
        pdfbox_bin = os.getenv('PDFBOX_BIN', None)
        pdftotext_bin = os.getenv('PDF_TO_TEXT_BIN', None)
        self.assertIsNotNone(tika_bin, 'You must set TIKA_BIN env var')
        self.assertIsNotNone(pdfbox_bin, 'You must set PDFBOX_BIN env var')
        self.assertIsNotNone(pdftotext_bin, 'You must set PDF_TO_TEXT_BIN'
                             ' env var')

        output_dir = mkdtemp()

        txt_file = get_test_data_folder()
        txt_file = os.path.join(txt_file,
                                'pdf/Flora_urbana.pdf')

        call = ['python',
                self.script,
                'PDFparserController',
                '--out_dir',
                output_dir,
                '--tika_bin',
                tika_bin,
                '--pdfbox_bin',
                pdfbox_bin,
                '--pdftotext_bin',
                pdftotext_bin,
                '--infile',
                txt_file,
                '--parser_type',
                'all']

        json_output = subprocess.check_output(call)

        self.assertTrue(is_json(json_output))

        rmtree(output_dir)

    def test_MetadataController(self):

        tika_bin = os.getenv('TIKA_BIN', None)
        pdf_info_bin = os.getenv('PDF_INFO_BIN', None)

        self.assertIsNotNone(tika_bin, 'You must set TIKA_BIN env var')
        self.assertIsNotNone(pdf_info_bin, 'You must set PDF_INFO_BIN env var')

        txt_file = get_test_data_folder()
        txt_file = os.path.join(txt_file,
                                'pdf/Flora_urbana.pdf')

        call = ['python',
                self.script,
                'MetadataController',
                '--tika_bin',
                tika_bin,
                '--pdfinfo_bin',
                pdf_info_bin,
                '--infile',
                txt_file,
                '--metadata_type',
                'all']

        json_output = subprocess.check_output(call)

        self.assertTrue(is_json(json_output))

    def test_TaxonfinderController(self):
        txt_file = get_test_data_folder()
        txt_file = os.path.join(txt_file,
                                'mini.txt')

        call = ['python',
                self.script,
                'TaxonfinderController',
                '--infile',
                txt_file]

        json_output = subprocess.check_output(call)

        self.assertTrue(is_json(json_output))

    def test_NetinetiController(self):
        txt_file = get_test_data_folder()
        txt_file = os.path.join(txt_file,
                                'fromOCR',
                                'docs_Anexos_biodiversidad_Michoacan.pdf.txt')
        call = ['python',
                self.script,
                'NetinetiController',
                '--language',
                'es',
                '--infile',
                txt_file,
                '--abbrev',
                'true',
                '--sciname_type',
                'all']
        json_output = subprocess.check_output(call)
        self.assertTrue(is_json(json_output))

    def test_pipeline(self):
        # read configuration file
        configuration_file = get_test_data_folder()
        configuration_file = os.path.join(configuration_file, 'test.conf')
        config = ConfigParser.SafeConfigParser()
        with open(configuration_file, 'r') as cf:
            config.readfp(cf)

        # copy two files
        in_dir = mkdtemp()
        file1 = get_test_data_folder()
        file1 = os.path.join(file1, 'pdf/anothersubdir/0647192_A7.pdf')
        copy(file1, in_dir)

        file2 = get_test_data_folder()
        file2 = os.path.join(file2, 'pdf/Test_PDF.pdf')
        copy(file2, in_dir)

        # create tmp output dir
        output_dir = mkdtemp()
        config.set('DEFAULT', 'OUT_DIR', output_dir)
        tika_bin = os.getenv('TIKA_BIN', None)
        config.set('DEFAULT', 'TIKA_BIN', tika_bin)
        pdf_info_bin = os.getenv('PDF_INFO_BIN', None)
        config.set('DEFAULT', 'PDFINFO_BIN', pdf_info_bin)
        pdfbox_bin = os.getenv('PDFBOX_BIN', None)
        config.set('DEFAULT', 'PDFBOX_BIN', pdfbox_bin)
        pdftotext_bin = os.getenv('PDF_TO_TEXT_BIN', None)
        config.set('DEFAULT', 'PDFTOTEXT_BIN', pdftotext_bin)

        config.set('DEFAULT', 'IN_DIR', in_dir)

        output_meta = os.path.join(output_dir, 'meta')
        config.set('MetadataController', 'OUT_DIR', output_meta)

        output_parser = os.path.join(output_dir, 'parser')
        config.set('PDFparserController', 'OUT_DIR', output_parser)

        input_pre = output_parser
        output_pre = os.path.join(output_dir, 'corrected')

        opennlp_bin = os.getenv('OPENNLP_BIN')
        opennlp_model = get_opennlp_model()

        config.set('PreprocessingController', 'IN_DIR', input_pre)
        config.set('PreprocessingController', 'OUT_DIR', output_pre)
        config.set('PreprocessingController', 'OPENNLP_BIN', opennlp_bin)
        config.set('PreprocessingController', 'OPENNLP_MOD', opennlp_model)

        input_ind = output_pre
        output_ind = os.path.join(output_dir, 'indexes')
        config.set('ReIndexController', 'IN_DIR', input_ind)
        config.set('ReIndexController', 'OUT_DIR', output_ind)
        regexp_file = get_regex_folder()
        regexp_file = os.path.join(regexp_file, 'regexp_NombreCientifico.re')
        config.set('ReIndexController', 'REGEXP_FILE', regexp_file)

        config.set('FragmentController', 'IN_DIR', input_ind)
        config.set('FragmentController', 'OUT_DIR', output_ind)
        regex_dir = get_regex_folder()
        config.set('FragmentController', 'REGEX_DIR', regex_dir)

        config.set('TaxonfinderController', 'IN_DIR', input_ind)
        config.set('TaxonfinderController', 'OUT_DIR', output_ind)

        config.set('NetinetiController', 'IN_DIR', input_ind)
        config.set('NetinetiController', 'OUT_DIR', output_ind)

        new_configuration_file = os.path.join(output_dir, 'test.conf')
        with open(new_configuration_file, 'w') as c:
            config.write(c)

        call = ['python',
                self.script,
                new_configuration_file]

        self.assertEqual(subprocess.check_call(call), 0)

        # check all directories were created
        self.assertTrue(os.path.isdir(output_meta))
        self.assertTrue(os.path.isdir(output_parser))
        self.assertTrue(os.path.isdir(output_pre))
        self.assertTrue(os.path.isdir(output_ind))

        rmtree(output_dir)
        rmtree(in_dir)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
