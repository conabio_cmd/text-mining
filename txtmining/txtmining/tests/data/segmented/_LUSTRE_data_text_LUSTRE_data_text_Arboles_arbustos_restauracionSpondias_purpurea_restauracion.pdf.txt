Spondias purpurea  28  Spondias purpurea L. (1762).
— ANACARDIACEAE  — Publicado en: Species Plantarum, Editio 2: 613.
1762.
Nombres comunes en México.
Ciruelo, Ciruela, Ciruela colorada, Abal (Rep.
Mex.); Chatsutsocoscatan, Tsusocostata, Smucuco-scatan (l. totonaca, Ver.); Ciruela Campechana (Ver.,
Chis.,
Yuc.); Huitzó (l. zoque, Chis.);
Jocote (Oax.,
Tab.,
Chis.); Jondura, Poon (Chis.);
Biaxhi, Biagi, Biadxi, Yaga-piache (l. zapoteca); Atoyaxócotl, Mazaxocotl (l. náhuatl antigua); Chak-abal, Ix-houen, Chi-abal, Kosumil muluch-abal (l. maya, Yuc.); Ciruelo de San Juan (Yuc.);
Cuaripá (l. huichol, Jal.); Shuiutipi´chic (l. popoluca, Ver.); Mauí (l. chinanteca, Oax.); Schizá, El Shimalo-schindzá (l. chonatl, Oax.); Cundaria, Tuñ (Oax.);
Tuxpana (Tab.);
Cupú (l. tarasca); Ciruelo cimarrón (Ver.);
Ten (l. huasteca, S.L.P.).
Sinonimia.
Spondias cirouella Tussac ; Spondias cytherea Sonn.
; Spondias macrocarpa Engl.
; Spondias mombin L. ; Spondias purpurea fo. lutea (Macfadyen) Fawcett & Rendle ; Warmingia macrocarpa Engl.
DESCRIPCION  Forma.
Arbol o arbusto caducifolio, de 3 a 8 m (hasta 15 m) de altura, con un diámetro a la altura del pecho de hasta 80 cm.
Copa / Hojas.
Copa muy extendida.
Hojas alternas, pinnadas, de color verde amarillento, de 10 a 20 cm de largo con 9 a 25 folíolos elípticos de 1.9 a 4 cm de largo, con borde ligeramente ondulado.
Tronco / Ramas.
Tronco corto, se ramifica desde 1 m de altura.
Ramas gruesas, retorcidas y frágiles o quebradizas (vidriosas o volubles).
Corteza.
Externa rugosa, muy ornamentada y con aspecto muy variable, de color gris plomo a moreno verdoso, a veces con fisuras irregulares y protuberancias con textura de corcho pequeñas o en ocasiones muy grandes, que incluso pueden confundirse con espinas o costillas.
Flor(es).
Panículas finamente vellosas con pocas flores.
Las flores son pequeñas y de color rojo o rosado, de 0.63
cm de diámetro; cáliz diminuto con 5 lóbulos y 5 pétalos.
Fruto(s).
Drupa, de color rojo purpúreo o amarillo, ovoide, de 3 cm de largo por 1.5 de ancho, pulpa de color amarillo, jugosa y agridulce, con un hueso de 0.50
a 0.75
cm de largo, grande, fibroso por fuera; contiene de 1 a 5 semillas.
Semilla(s).
Semillas aplanadas, de 12 mm de largo.
Raíz.
No disponible.
Sexualidad.
Hermafrodita, polígama.
DISTRIBUCION  Especie de amplia distribución, principalmente en la  vertiente del Pacífico y la mitad sur de la República Mexicana, desde Sonora hasta Oaxaca, Chiapas y Yucatán.
Altitud: 0 a 1,000 m.
Estados.
CHIS.
GRO.
JAL.
MEX.
MICH.
MOR.
NAY.
OAX.
PUE.
QROO.
S.L.P.
SON.
TAB.
VER.
YUC.
ORIGEN / EXTENSION  Originaria de Mesoamérica.
Se extiende desde México hasta Brasil y Perú, se le ha introducido a los trópicos del Viejo Mundo.
Se le cultiva en el sur de Florida (Estados Unidos).
ESTATUS  Nativa.
Cultivada.
Silvestre.
Nativo de los trópicos del continente.
Se le cultiva en toda la tierra caliente.
HABITAT  Se le puede encontrar en potreros, acahuales, huertos familiares, pastizales.
Suelos: pedregoso, somero, aluvial, amarillo arcilloso, roca caliza.
IMPORTANCIA ECOLOGICA  Especie Secundaria.
VEGETACION / ZONA ECOLOGICA  Tipos de Vegetación.
· Bosque de pino.
· Bosque de encino.
· Bosque de galería.
· Bosque tropical perennifolio.
· Bosque tropical caducifolio.
· Bosque tropical subcaducifolio.
Vegetación asociada.
Acacia sp.,
Agave sp.,
Swietenia sp.,
Manilkara sp.,
Jacaratia sp.,
Talisia sp.
Zona(s) ecológica(s).
Arida y semiárida.
Trópico húmedo.
Trópico subhúmedo.
FENOLOGIA  Follaje.
Caducifolio.
Floración.
Florece de febrero a mayo.
Fructificación.
Fructifica de mayo a julio.
En Yucatán los frutos maduran de marzo a mayo.
Polinización.
No disponible.
Spondias purpurea  29  ASPECTOS FISIOLOGICOS  Adaptación.
No disponible.
Competencia.
No disponible.
Crecimiento.
No disponible.
Descomposición.
No disponible.
Establecimiento.
No disponible.
Interferencia.
No disponible.
Producción de hojas, frutos, madera y/o semillas.
No disponible.
Regeneración.
No disponible.
SEMILLA  Almacenamiento / Conservación.
No disponible.
Dispersión.
Zoócora.
Entre los dispersores se han registrado a los siguientes animales: coyote, coatí, zorra gris, venado, chachalaca, iguana.
Los frutos dispersados por iguanas son menos exitosos que los dispersados por venados.
Germinación.
No disponible.
Porcentaje de germinación.
No disponible.
Número de semillas por kilogramo.
No disponible.
Recolección / Extracción.
No disponible.
Tratamiento pregerminativo.
No disponible.
Viabilidad / Latencia / Longevidad.
No disponible.
EXPERIENCIAS CON LA PLANTA  Plantación Comercial / Productiva / Experimental.
No disponible.
Reforestación / Restauración.
Especie con potencial para reforestación productiva en zonas degradadas de selva.
Sistema agroforestal.
Arboles en linderos formando una cerca viva.
Frecuentemente encontrada en huertos familiares en Puebla y Yucatán.
Uso principal en el huerto: fruta.
CULTIVO  Aspectos del cultivo.
Se propaga muy fácilmente y se pueden cultivar muchas formas agronómicas.
Las semillas germinan bien cuando son cubiertas con humus.
Tolera bien el corte o poda.
PROPAGACION  Reproducción asexual.
1. Estacas o esquejes.
Cortes de tallo.
2.
Acodos.
Reproducción sexual.
1. Semilla (plántulas).
Raramente puede propagarse por semillas.
EFECTO RESTAURADOR / SERVICIO AL AMBIENTE  Efecto(s)  restaurador(es).
1. Recuperación de terrenos degradados.
Esta planta se ha empleado para rehabilitar sitios donde hubo explotación minera.
Servicio(s).
1. Cerca viva en los agrohábitats.
2.
Sombra / Refugio.
Algunas especies de mamíferos, aves y hormigas se alimentan de los frutos.
TOLERANCIAS  Demandante de.
1. Luz Resistente a. 1. Fuego.
2.
Sequía Tolerante a. 1. Inundación temporal.
2.
Exposición constante al viento.
3. Suelos compactados y pedregosos.
4. Suelos someros.
DESVENTAJAS  Intolerante a. 1. No disponible.
Sensible / Susceptible a. 1. Daño por el viento.
2.
Suelos salinos.
Exhibe una pobre tolerancia a suelos salinos, por lo que no se recomienda su plantación en zonas costeras tropicales.
3. Suelos ácidos.
4. Daño por insectos (semilla, fruto).
USOS  Adhesivo [exudado (resina)].
La resina se utiliza en América Central para elaborar pegamentos y gomas.
Comestible (fruta, bebidas) [fruto, brote].
Los frutos frescos se comen maduros o crudos, deshidratados, curtidos en alcohol o salmuera.
Con los frutos se elaboran bebidas refrescantes y vinagre.
Se usan también para hacer gelatina y jalea.
Los frutos se venden en el mercado local y regional.
Los frutos inmaduros se adicionan a los frijoles y se hace atole, tartas y salsa.
En Sinaloa se industrializa en diferentes formas, ciruela pasa con y sin sal, ciruela negra dulce, ciruela cristalina dulce.
Los brotes jóvenes y hojas se consumen crudos o cocinados como verduras.
Estimulante [fruto].
Con los frutos se confeccionan vinos y  otras bebidas alcohólicas (chicha).
Forrajero [hoja, brote].
Se usa para alimentar ganado y cebar cerdos.
La mayor proporción de materia seca comestible se produce los primeros 90 días después de la poda inicial debido a lo tierno de los rebrotes en ese momento.
Después de 150 días de la poda inicial, la proporción de materia seca  comestible disminuye  por la lignificación de la biomasa, ocasionando una baja digestibilidad.
Industrializable [madera].
La madera es ligera y blanda y en Brasil se ha encontrado que es adecuada para la fabricación de papel.
Medicinal [corteza, hoja, fruto, exudado (resina), raíz].
El extracto de las hojas y corteza es utilizado como febrífugo.
Al sureste de Nigeria se emplea una    Spondias purpurea  30  infusión de hojas para lavar heridas, inflamaciones y quemaduras.
El extracto de la corteza cocida es un remedio para la roña, disentería y para la flatulencia infantil.
En Filipinas la savia de la corteza se usa para tratar estomatitis en infantes.
Fruto (extracto, jarabe): el extracto se utiliza para sanar inflamaciones, el jarabe se usa para curar diarrea crónica.
Exudado (resina): la resina se mezcla con jugo de piña o guanábana para tratar la ictericia.
Hoja: infecciones de encía, salpullido, sarampión.
Hoja, corteza: antipirético y antidiarréico.
Raíz: erupciones acompañadas de dolor de cabeza y cuello.
Se ha reportado que además se utiliza para enfermedades del intestino y la vejiga, y como remedio contra la sarna.
Fruto: pus en la orina; diurético, antiespasmódico (machacado con alcohol).
Melífera [flor].
Apicultura.
Saponífera [madera].
Las cenizas de la madera se utilizan en la fabricación de jabón.
Tutor [toda la planta].
Tutor de orquídeas.
BIBLIOGRAFIA  Avitia García, Ediberto.
1996.
Benavides, Jorge Evelio (Compilador y Editor).1994
Bullock, S.H.
1992.
Elbert L., Little Jr.,
F.H.
Wadsworth y J.
Marrero.
1967.
García Nuñez, Rosa María.
1993.
Howard, R.A.1991.
León de Pinto, G., M. Martínez, J.A.
Mendoza, E. Ocando and C.
Rivas.
1995.
Mandujano, S., S. Gallina and H.
Bullock S. 1994.
Marler, Thomas E. and V.
Mickelbart Michael.
1993.
Martínez Bravo, L. y V.G.
Almaguer.
1988.
Martínez, Maximino.
1979.
Mendieta, Rosa María y Silvia del Amo.
1981.
Messenger, A.S.,
J.F.
Di Stéfano and L.A.
Fournier.
1997.
Nolazco Guizar, Enrique y Alejandro Sánchez Vélez.
1991.
Peter G. von Carlowitz, G.V.
Wolf and E.M.
Kemperman.
1991.
Rzedowski, Jerzy y Miguel Equihua.
1987.
Zamora Martínez, Marisela C. y Luis Hernández Pallares.
1985.

