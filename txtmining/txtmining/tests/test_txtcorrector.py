# -*- coding: utf-8 -*-

from StringIO import StringIO
import os
import unittest

from txtmining.document.text_corrector import (OnelineCorrector,
                                               DashCorrector,
                                               SentenceCorrector)


class CorrectorTest(unittest.TestCase):

    def setUp(self):
        self.text = StringIO('Lorem ipsum dolor sit amet, vím ex amét illúm'
                             ' causáe.'
                             ' Hás añcillae apéi-\nriañ te, essént scaevóla '
                             'te mei. '
                             'Lorem clitá affert vix et, clita expe-\ntendá'
                             ' reprímique'
                             ' pri ád. ¡Qúo purto iñañi no, movet aliquid eam'
                             ' in, '
                             'di-\ncta quaereñdum sea te! Te dolorem offendit'
                             ' percipít'
                             ' per, mañdamus ácco-\nmmodare no vix. Qui at'
                             ' iísque '
                             'fácilis coñstitúto, at nec facéte sanctús.')

        self.undashed = ('Lorem ipsum dolor sit amet, vím ex amét illúm '
                         'causáe.'
                         ' Hás añcillae apéiriañ te, essént scaevóla te mei. '
                         'Lorem clitá affert vix et, clita expetendá '
                         'reprímique'
                         ' pri ád. ¡Qúo purto iñañi no, movet aliquid eam in, '
                         'dicta quaereñdum sea te! Te dolorem offendit '
                         'percipít'
                         ' per, mañdamus áccommodare no vix. Qui at iísque '
                         'fácilis coñstitúto, at nec facéte sanctús.')

        self.oneline = ('Lorem ipsum dolor sit amet, vím ex amét illúm causáe.'
                        ' Hás añcillae apéiriañ te, essént scaevóla te mei. '
                        'Lorem clitá affert vix et, clita expetendá reprímique'
                        ' pri ád. ¡Qúo purto iñañi no, movet aliquid eam in, '
                        'dicta quaereñdum sea te! Te dolorem offendit percipít'
                        ' per, mañdamus áccommodare no vix. Qui at iísque '
                        'fácilis coñstitúto, at nec facéte sanctús.')

        self.sentences = ('Lorem ipsum dolor sit amet, vím ex amét illúm '
                          'causáe.\nHás añcillae apéiriañ te, essént scaevóla'
                          ' te mei.\nLorem clitá affert vix et, clita '
                          'expetendá reprímique pri ád.\n¡Qúo purto iñañi no, '
                          'movet aliquid eam in, dicta quaereñdum sea te!\nTe '
                          'dolorem offendit percipít per, mañdamus áccommodare'
                          ' no vix.\nQui at iísque fácilis coñstitúto, at nec'
                          ' facéte sanctús.\n\n')

        # allow to compare long strings
        self.maxDiff = None

    def test_dashcorrector(self):
        self.assertEqual(DashCorrector.correct(self.text).getvalue(),
                         self.undashed)

    def test_onelinecorrector(self):
        self.assertEqual(OnelineCorrector.correct(StringIO(self.undashed)).
                         getvalue(),
                         self.oneline)

    def test_sentencescorrector(self):
        corrector = SentenceCorrector(opennlp_bin=os.getenv("OPENNLP_BIN"),
                                      opennlp_mod=os.getenv("OPENNLP_MODEL"))
        self.assertEqual(corrector.correct(StringIO(self.oneline)).getvalue(),
                         self.sentences)


if __name__ == '__main__':

    unittest.main()
