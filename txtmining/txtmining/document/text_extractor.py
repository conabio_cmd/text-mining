'''
Created on Apr 10, 2015

@author: Daniel
'''
import codecs
import os
import subprocess

from slugify import slugify

from txtmining.base.util import shell_quote


class TextExtractor(object):
    '''
    classdocs
    Abstract class
    '''

    def __init__(self,
                 bin_path,
                 output_dir):
        '''
        Constructor
        '''
        self._call = None
        self._bin = bin_path
        self._output_dir = output_dir
        self._output_file = None

    def extract(self):
        subprocess.check_output(self._call,
                                shell=True,
                                stderr=subprocess.STDOUT)

    def _check_output(self):
        if self._output_file is not None:
            unquoted_created_file_path = self._output_file.replace("\'\\\'",
                                                                   "")
            upath = unquoted_created_file_path[1:-1]
            if os.path.isfile(upath):
                with codecs.open(upath, 'rU') as open_file:
                    content = str(open_file.read())
                    if os.stat(upath).st_size == 0 or str(content).isspace():
                        # File is empty or with just blank spaces
                        os.remove(upath)
                        return None
            else:
                # Cannot read file
                return None

            return upath

        else:
            raise Exception("Output file was not created")

    def _get_created_file_path(self, path_file):
        fn, ext = os.path.splitext(path_file)
        filename = ''.join([slugify(fn), ext])
        created_file_path = os.path.join(self._output_dir,
                                         filename)
        created_file_path += '.txt'
        return shell_quote(created_file_path)

    def _set_call(self):
        raise NotImplementedError("Implement this")
